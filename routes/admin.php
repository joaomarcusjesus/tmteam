<?php

/**
* Admin Routes
*/
Route::prefix('admin')->group(function() {

  /**
  * Auto login
  */
  Route::name('autologin')->middleware('guest.admin')->get('/autologin/{token}', '\Watson\Autologin\AutologinController@autologin');

  /**
  * Public Routes
  */
  Route::middleware('guest.admin')->namespace('Admin\Sessions')->group(function() {
    // Login
    Route::name('admin.session.login')->get('/acessar', 'SessionController@showLoginForm');
    Route::name('admin.session.login')->post('/acessar', 'SessionController@login');

    // Social Login
    Route::name('admin.social.redirect')->get('/social/redirect/{provider}', 'SocialController@redirectToProvider');
    Route::name('admin.social.handle')->get('/social/handle/{provider}', 'SocialController@handleProviderCallback');

    // Reset Password
    Route::name('admin.password.forgot')->get('/esqueci-minha-senha', 'ForgotPasswordController@showLinkRequestForm');
    Route::name('admin.password.processForgot')->post('/esqueci-minha-senha', 'ForgotPasswordController@sendResetLinkEmail');
    Route::name('admin.password.reset')->get('/cadastrar-senha/{token}/{email}', 'ResetPasswordController@showResetForm');
    Route::name('admin.password.processReset')->post('/cadastrar-senha', 'ResetPasswordController@reset');

    // First access
    Route::name('admin.activation.create')->get('/primeiro-acesso/{token}', 'ActivationController@showActivationForm');
    Route::name('admin.activation.processCreate')->post('/primeiro-acesso/{token}', 'ActivationController@store');
  });

  /**
   * Protected Routes
   */
  Route::middleware(['auth.admin'])->group(function() {

    // Logout
    Route::name('admin.session.logout')->get('/sair', 'Admin\Sessions\SessionController@logout');

    // Api
    Route::prefix('api')->namespace('Api')->group(function() {
      Route::name('api.modalities.filter')->get('/filtrar-categorias/{slug?}', 'Modalities\ModalityController@filter');
    });

    // Check first access
    Route::middleware('first.access')->namespace('Admin')->group(function() {

      // Dashboards
      Route::name('admin.dashboard.index')->get('/', 'Dashboard\DashboardController@index');

      // Dashboards
      Route::name('admin.birthdays.index')->get('/aniversariantes', 'Birthdays\BirthdayController@index');

      // Students frequencies
      Route::prefix('frequencias')->group(function() {
        Route::name('admin.students_frequencies.index')->get('/', 'Student\Frequencies\FrequencyController@index');
        Route::name('admin.students_frequencies.show')->get('/visualizar/{id}', 'Student\Frequencies\FrequencyController@show');
        Route::name('admin.students_frequencies.create')->get('/adicionar', 'Student\Frequencies\FrequencyController@create');
        Route::name('admin.students_frequencies.store')->post('/adicionar', 'Student\Frequencies\FrequencyController@store');
        Route::name('admin.students_frequencies.edit')->get('/editar/{id}', 'Student\Frequencies\FrequencyController@edit');
        Route::name('admin.students_frequencies.update')->put('/editar/{id}', 'Student\Frequencies\FrequencyController@update');
        Route::name('admin.students_frequencies.destroy')->delete('/excluir/{id}', 'Student\Frequencies\FrequencyController@destroy');
      });
      ;

       // Students graduations
      Route::prefix('graduacoes')->group(function() {
        Route::name('admin.students_graduations.index')->get('/', 'Student\Graduations\GraduationController@index');
        Route::name('admin.students_graduations.show')->get('/visualizar/{id}', 'Student\Graduations\GraduationController@show');
        Route::name('admin.students_graduations.create')->get('/adicionar', 'Student\Graduations\GraduationController@create');
        Route::name('admin.students_graduations.store')->post('/adicionar', 'Student\Graduations\GraduationController@store');
        Route::name('admin.students_graduations.edit')->get('/editar/{id}', 'Student\Graduations\GraduationController@edit');
        Route::name('admin.students_graduations.update')->put('/editar/{id}', 'Student\Graduations\GraduationController@update');
        Route::name('admin.students_graduations.destroy')->delete('/excluir/{id}', 'Student\Graduations\GraduationController@destroy');
      });

      // Students
      Route::prefix('alunos')->group(function() {
        Route::name('admin.students.index')->get('/', 'Students\StudentController@index');
        Route::name('admin.students.show')->get('/visualizar/{code}', 'Students\StudentController@show');
        Route::name('admin.students.create')->get('/adicionar', 'Students\StudentController@create');
        Route::name('admin.students.store')->post('/adicionar', 'Students\StudentController@store');
        Route::name('admin.students.edit')->get('/editar/{id}', 'Students\StudentController@edit');
        Route::name('admin.students.update')->put('/editar/{id}', 'Students\StudentController@update');
        Route::name('admin.students.destroy')->delete('/excluir/{id}', 'Students\StudentController@destroy');
        Route::name('admin.students.subscription')->post('/nova-matricula/{code}', 'Students\StudentController@subscription');
        Route::name('admin.students.band')->post('/nova-faixa/{code}', 'Students\StudentController@band');
        Route::name('admin.students.fisic')->post('/aptidao-fisica/{code}', 'Students\StudentController@fisic');

        // Students Tuitions
        Route::prefix('{code}/mensalidades')->group(function() {
          Route::name('admin.students_tuitions.index')->get('/', 'Students\StudentTuitionController@index');
          Route::name('admin.students_tuitions.show')->get('/visualizar/{id}', 'Students\StudentTuitionController@show');
          Route::name('admin.students_tuitions.create')->get('/adicionar', 'Students\StudentTuitionController@create');
          Route::name('admin.students_tuitions.store')->post('/adicionar', 'Students\StudentTuitionController@store');
          Route::name('admin.students_tuitions.destroy')->delete('/excluir/{id}', 'Students\StudentTuitionController@destroy');
        });

        // Students Origins
        Route::prefix('origens')->group(function() {
          Route::name('admin.students_origins.index')->get('/', 'Students\StudentOriginController@index');
          Route::name('admin.students_origins.create')->get('/adicionar', 'Students\StudentOriginController@create');
          Route::name('admin.students_origins.store')->post('/adicionar', 'Students\StudentOriginController@store');
          Route::name('admin.students_origins.edit')->get('/editar/{id}', 'Students\StudentOriginController@edit');
          Route::name('admin.students_origins.update')->put('/editar/{id}', 'Students\StudentOriginController@update');
          Route::name('admin.students_origins.destroy')->delete('/excluir/{id}', 'Students\StudentOriginController@destroy');
        });
      });

      // Orders
      Route::prefix('pedidos')->group(function() {
        Route::name('admin.orders.index')->get('/', 'Orders\OrderController@index');
        Route::name('admin.orders.show')->get('/visualizar/{code}', 'Orders\OrderController@show');
      });

      // Classrooms
      Route::prefix('turmas')->group(function() {
        Route::name('admin.classrooms.index')->get('/', 'Classrooms\ClassroomController@index');
        Route::name('admin.classrooms.show')->get('/visualizar/{id}', 'Classrooms\ClassroomController@show');
        Route::name('admin.classrooms.create')->get('/adicionar', 'Classrooms\ClassroomController@create');
        Route::name('admin.classrooms.store')->post('/adicionar', 'Classrooms\ClassroomController@store');
        Route::name('admin.classrooms.edit')->get('/editar/{id}', 'Classrooms\ClassroomController@edit');
        Route::name('admin.classrooms.update')->put('/editar/{id}', 'Classrooms\ClassroomController@update');
        Route::name('admin.classrooms.destroy')->delete('/excluir/{id}', 'Classrooms\ClassroomController@destroy');

        // Classrooms Frequencies
        Route::prefix('{classroom_id}/frequencias')->group(function() {
          Route::name('admin.classrooms_frequencies.index')->get('/', 'Classrooms\ClassroomFrequencyController@index');
          Route::name('admin.classrooms_frequencies.show')->get('/visualizar/{id}', 'Classrooms\ClassroomFrequencyController@show');
          Route::name('admin.classrooms_frequencies.create')->get('/adicionar', 'Classrooms\ClassroomFrequencyController@create');
          Route::name('admin.classrooms_frequencies.store')->post('/adicionar', 'Classrooms\ClassroomFrequencyController@store');
          Route::name('admin.classrooms_frequencies.edit')->get('/editar/{id}', 'Classrooms\ClassroomFrequencyController@edit');
          Route::name('admin.classrooms_frequencies.update')->put('/editar/{id}', 'Classrooms\ClassroomFrequencyController@update');
          Route::name('admin.classrooms_frequencies.destroy')->delete('/excluir/{id}', 'Classrooms\ClassroomFrequencyController@destroy');
        });
      });

      // Bands
      Route::prefix('faixas')->group(function() {
        Route::name('admin.bands.index')->get('/', 'Bands\BandController@index');
        Route::name('admin.bands.create')->get('/adicionar', 'Bands\BandController@create');
        Route::name('admin.bands.store')->post('/adicionar', 'Bands\BandController@store');
        Route::name('admin.bands.edit')->get('/editar/{id}', 'Bands\BandController@edit');
        Route::name('admin.bands.update')->put('/editar/{id}', 'Bands\BandController@update');
        Route::name('admin.bands.destroy')->delete('/excluir/{id}', 'Bands\BandController@destroy');
        Route::name('admin.bands.reorder')->post('/reordenar', 'Bands\BandController@reorder');

        // Bands Categories
        Route::prefix('categorias')->group(function() {
          Route::name('admin.bands_categories.index')->get('/', 'Bands\BandCategoryController@index');
          Route::name('admin.bands_categories.create')->get('/adicionar', 'Bands\BandCategoryController@create');
          Route::name('admin.bands_categories.store')->post('/adicionar', 'Bands\BandCategoryController@store');
          Route::name('admin.bands_categories.edit')->get('/editar/{id}', 'Bands\BandCategoryController@edit');
          Route::name('admin.bands_categories.update')->put('/editar/{id}', 'Bands\BandCategoryController@update');
          Route::name('admin.bands_categories.destroy')->delete('/excluir/{id}', 'Bands\BandCategoryController@destroy');
        });
      });

      // Graduations
      Route::prefix('graduacoes')->group(function() {
        Route::name('admin.graduations.index')->get('/', 'Graduations\GraduationController@index');
        Route::name('admin.graduations.create')->get('/adicionar', 'Graduations\GraduationController@create');
        Route::name('admin.graduations.store')->post('/adicionar', 'Graduations\GraduationController@store');
        Route::name('admin.graduations.edit')->get('/editar/{id}', 'Graduations\GraduationController@edit');
        Route::name('admin.graduations.update')->put('/editar/{id}', 'Graduations\GraduationController@update');
        Route::name('admin.graduations.destroy')->delete('/excluir/{id}', 'Graduations\GraduationController@destroy');

        // Graduations Rules
        Route::prefix('regras')->group(function() {
          Route::name('admin.graduations_rules.index')->get('/', 'Graduations\GraduationRuleController@index');
          Route::name('admin.graduations_rules.create')->get('/adicionar', 'Graduations\GraduationRuleController@create');
          Route::name('admin.graduations_rules.store')->post('/adicionar', 'Graduations\GraduationRuleController@store');
          Route::name('admin.graduations_rules.edit')->get('/editar/{id}', 'Graduations\GraduationRuleController@edit');
          Route::name('admin.graduations_rules.update')->put('/editar/{id}', 'Graduations\GraduationRuleController@update');
          Route::name('admin.graduations_rules.destroy')->delete('/excluir/{id}', 'Graduations\GraduationRuleController@destroy');
        });
      });

      // Bills
      Route::prefix('contas')->group(function() {
        Route::name('admin.bills.index')->get('/', 'Bills\BillController@index');
        Route::name('admin.bills.show')->get('/visualizar/{id}', 'Bills\BillController@show');
        Route::name('admin.bills.create')->get('/adicionar', 'Bills\BillController@create');
        Route::name('admin.bills.store')->post('/adicionar', 'Bills\BillController@store');
        Route::name('admin.bills.edit')->get('/editar/{id}', 'Bills\BillController@edit');
        Route::name('admin.bills.update')->put('/editar/{id}', 'Bills\BillController@update');
        Route::name('admin.bills.destroy')->delete('/excluir/{id}', 'Bills\BillController@destroy');
      });

      // Bills Types
      Route::prefix('contas/tipos')->group(function() {
        Route::name('admin.bills_types.index')->get('/', 'Bills\BillTypeController@index');
        Route::name('admin.bills_types.create')->get('/adicionar', 'Bills\BillTypeController@create');
        Route::name('admin.bills_types.store')->post('/adicionar', 'Bills\BillTypeController@store');
        Route::name('admin.bills_types.edit')->get('/editar/{id}', 'Bills\BillTypeController@edit');
        Route::name('admin.bills_types.update')->put('/editar/{id}', 'Bills\BillTypeController@update');
        Route::name('admin.bills_types.destroy')->delete('/excluir/{id}', 'Bills\BillTypeController@destroy');
      });

      // Bills Items
      Route::prefix('contas/items')->group(function() {
        Route::name('admin.bills_items.index')->get('/', 'Bills\BillItemController@index');
        Route::name('admin.bills_items.create')->get('/adicionar', 'Bills\BillItemController@create');
        Route::name('admin.bills_items.store')->post('/adicionar', 'Bills\BillItemController@store');
        Route::name('admin.bills_items.edit')->get('/editar/{id}', 'Bills\BillItemController@edit');
        Route::name('admin.bills_items.update')->put('/editar/{id}', 'Bills\BillItemController@update');
        Route::name('admin.bills_items.destroy')->delete('/excluir/{id}', 'Bills\BillItemController@destroy');
      });

      // Modalities
      Route::prefix('modalidades')->group(function() {
        Route::name('admin.modalities.index')->get('/', 'Modalities\ModalityController@index');
        Route::name('admin.modalities.create')->get('/adicionar', 'Modalities\ModalityController@create');
        Route::name('admin.modalities.store')->post('/adicionar', 'Modalities\ModalityController@store');
        Route::name('admin.modalities.edit')->get('/editar/{id}', 'Modalities\ModalityController@edit');
        Route::name('admin.modalities.update')->put('/editar/{id}', 'Modalities\ModalityController@update');
        Route::name('admin.modalities.destroy')->delete('/excluir/{id}', 'Modalities\ModalityController@destroy');
      });

      // Teachers
      Route::prefix('professores')->group(function() {
        Route::name('admin.teachers.index')->get('/', 'Teachers\TeacherController@index');
        Route::name('admin.teachers.show')->get('/visualizar/{code}', 'Teachers\TeacherController@show');
        Route::name('admin.teachers.create')->get('/adicionar', 'Teachers\TeacherController@create');
        Route::name('admin.teachers.store')->post('/adicionar', 'Teachers\TeacherController@store');
        Route::name('admin.teachers.edit')->get('/editar/{id}', 'Teachers\TeacherController@edit');
        Route::name('admin.teachers.update')->put('/editar/{id}', 'Teachers\TeacherController@update');
        Route::name('admin.teachers.destroy')->delete('/excluir/{id}', 'Teachers\TeacherController@destroy');
        Route::name('admin.teachers.sendEmail')->get('/reenviar-email/{id}', 'Teachers\TeacherController@sendEmail');
        Route::name('admin.teachers.activate')->get('/ativar/{id}', 'Teachers\TeacherController@activate');
        Route::name('admin.teachers.deactivate')->get('/desativar/{id}', 'Teachers\TeacherController@deactivate');
      });

      // Subscriptions
      Route::prefix('matriculas')->group(function() {
        Route::name('admin.subscriptions.index')->get('/', 'Subscriptions\SubscriptionController@index');
        Route::name('admin.subscriptions.show')->get('/visualizar/{code}', 'Subscriptions\SubscriptionController@show');
        Route::name('admin.subscriptions.create')->get('/adicionar', 'Subscriptions\SubscriptionController@create');
        Route::name('admin.subscriptions.store')->get('/adicionar/{id}', 'Subscriptions\SubscriptionController@store');
        Route::name('admin.subscriptions.edit')->get('/editar/{id}', 'Subscriptions\SubscriptionController@edit');
        Route::name('admin.subscriptions.update')->put('/editar/{id}', 'Subscriptions\SubscriptionController@update');
        Route::name('admin.subscriptions.destroy')->delete('/excluir/{id}', 'Subscriptions\SubscriptionController@destroy');
      });

      // Messages
      Route::prefix('mensagens')->group(function() {
        Route::name('admin.messages.index')->get('/{id?}', 'Messages\MessageController@index');
        Route::name('admin.messages.show')->get('/visualizar/{id}', 'Messages\MessageController@show');
        Route::name('admin.messages.edit')->get('/atualizar/{id}', 'Messages\MessageController@edit');
        Route::name('admin.messages.update')->put('/atualizar/{id}', 'Messages\MessageController@update');
        Route::name('admin.messages.destroy')->delete('/excluir/{id}', 'Messages\MessageController@destroy');
      });

      // Notifications
      Route::prefix('notificacoes')->group(function() {
        Route::name('admin.notifications.index')->get('/{id?}', 'Notifications\NotificationController@index');
        Route::name('admin.notifications.show')->get('/visualizar/{id}', 'Notifications\NotificationController@show');
        Route::name('admin.notifications.edit')->get('/atualizar/{id}', 'Notifications\NotificationController@edit');
        Route::name('admin.notifications.update')->put('/atualizar/{id}', 'Notifications\NotificationController@update');
        Route::name('admin.notifications.destroy')->delete('/excluir/{id}', 'Notifications\NotificationController@destroy');
      });

      // Account
      Route::prefix('perfil')->group(function() {
        Route::name('admin.accounts.show')->get('/', 'Users\AccountController@show');
        Route::name('admin.accounts.edit')->get('/atualizar', 'Users\AccountController@edit');
        Route::name('admin.accounts.update')->put('/atualizar', 'Users\AccountController@update');
      });

      // Permissions
      Route::prefix('permissoes')->group(function() {
        Route::name('admin.permissions.index')->get('/', 'Users\PermissionController@index');
        Route::name('admin.permissions.create')->get('/adicionar', 'Users\PermissionController@create');
        Route::name('admin.permissions.store')->post('/adicionar', 'Users\PermissionController@store');
        Route::name('admin.permissions.edit')->get('/editar/{id}', 'Users\PermissionController@edit');
        Route::name('admin.permissions.update')->put('/editar/{id}', 'Users\PermissionController@update');
        Route::name('admin.permissions.destroy')->delete('/excluir/{id}', 'Users\PermissionController@destroy');
      });

      // Roles
      Route::prefix('grupos')->group(function() {
        Route::name('admin.roles.index')->get('/', 'Users\RoleController@index');
        Route::name('admin.roles.create')->get('/adicionar', 'Users\RoleController@create');
        Route::name('admin.roles.store')->post('/adicionar', 'Users\RoleController@store');
        Route::name('admin.roles.edit')->get('/editar/{id}', 'Users\RoleController@edit');
        Route::name('admin.roles.update')->put('/editar/{id}', 'Users\RoleController@update');
      });

      // Users
      Route::prefix('usuarios')->group(function() {
        Route::name('admin.users.index')->get('/', 'Users\UserController@index');
        Route::name('admin.users.create')->get('/adicionar', 'Users\UserController@create');
        Route::name('admin.users.store')->post('/adicionar', 'Users\UserController@store');
        Route::name('admin.users.edit')->get('/editar/{id}', 'Users\UserController@edit');
        Route::name('admin.users.update')->put('/editar/{id}', 'Users\UserController@update');
        Route::name('admin.users.sendEmail')->get('/reenviar-email/{id}', 'Users\UserController@sendEmail');
        Route::name('admin.users.activate')->get('/ativar/{id}', 'Users\UserController@activate');
        Route::name('admin.users.deactivate')->get('/desativar/{id}', 'Users\UserController@deactivate');
      });

      // Settings
      Route::prefix('configuracoes')->group(function() {
        Route::name('admin.settings.index')->get('/', 'Settings\SettingController@index');
        Route::name('admin.settings.create')->get('/adicionar', 'Settings\SettingController@create');
        Route::name('admin.settings.store')->post('/adicionar', 'Settings\SettingController@store');
        Route::name('admin.settings.edit')->get('/editar/{id}', 'Settings\SettingController@edit');
        Route::name('admin.settings.update')->put('/editar/{id}', 'Settings\SettingController@update');
        Route::name('admin.settings.destroy')->delete('/excluir/{id}', 'Settings\SettingController@destroy');
      });

      // Audit
      Route::prefix('auditoria')->group(function() {
        Route::name('admin.audits.index')->get('/', 'Audits\AuditController@index');
        Route::name('admin.audits.show')->get('/{id}', 'Audits\AuditController@show');
      });
    });
  });
});
