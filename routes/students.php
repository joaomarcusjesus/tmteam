<?php

Route::prefix('area-do-aluno')->middleware(['student.admin'])->namespace('Student')->group(function () {
  Route::name('students.dashboard.index')->get('/', 'Dashboard\DashboardController@index');
  Route::name('students.orders.index')->get('/pagamentos', 'Orders\OrderController@index');
  Route::name('students.orders.show')->get('/pagamentos/visualizar/{code}', 'Orders\OrderController@show');
  Route::name('students.frequencies.index')->get('/frequencias', 'Frequencies\FrequencyController@index');
  Route::name('students.accounts.index')->get('/conta', 'Accounts\AccountController@index');
});

// Api
Route::prefix('api')->namespace('Api')->group(function() {
  Route::name('api.students.fisic')->post('aptidao-fisica/{code}', 'Documents\DocumentController@store');
  Route::name('api.orders.subscription')->get('/nova-matricula/{code}', 'Orders\OrderController@subscription');
});
