<?php

Breadcrumbs::for('dashboard', function ($breadcrumb) {
  $breadcrumb->push('Dashboard', route('admin.dashboard.index'));
});

Breadcrumbs::for('audits.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Auditoria', route('admin.audits.index'));
});

Breadcrumbs::for('accounts.show', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Meu Perfil', route('admin.accounts.show'));
});

Breadcrumbs::for('accounts.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('accounts.show');
  $breadcrumb->push('Editar Perfil', route('admin.accounts.edit', ['id' => $result->id]));
});

Breadcrumbs::for('audits.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('audits.index');
  $breadcrumb->push('Visualizar Log', route('admin.audits.show', ['id' => $result->id]));
});

Breadcrumbs::for('users.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Usuários', route('admin.users.index'));
});

Breadcrumbs::for('users.create', function ($breadcrumb) {
  $breadcrumb->parent('users.index');
  $breadcrumb->push('Novo Usuário', route('admin.users.create'));
});

Breadcrumbs::for('users.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('users.index');
  $breadcrumb->push('Editar Usuário', route('admin.users.edit', ['id' => $result->id]));
});

Breadcrumbs::for('roles.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Grupos', route('admin.roles.index'));
});

Breadcrumbs::for('roles.create', function ($breadcrumb) {
  $breadcrumb->parent('roles.index');
  $breadcrumb->push('Novo Grupo', route('admin.roles.create'));
});

Breadcrumbs::for('roles.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('roles.index');
  $breadcrumb->push('Editar Grupo', route('admin.roles.edit', ['id' => $result->id]));
});

Breadcrumbs::for('permissions.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Permissões', route('admin.permissions.index'));
});

Breadcrumbs::for('permissions.create', function ($breadcrumb) {
  $breadcrumb->parent('permissions.index');
  $breadcrumb->push('Nova Permissão', route('admin.permissions.create'));
});

Breadcrumbs::for('permissions.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('permissions.index');
  $breadcrumb->push('Editar Permissão', route('admin.permissions.edit', ['id' => $result->id]));
});

Breadcrumbs::for('bands.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Faixas', route('admin.bands.index'));
});

Breadcrumbs::for('bands.create', function ($breadcrumb) {
  $breadcrumb->parent('bands.index');
  $breadcrumb->push('Nova Faixa', route('admin.bands.create'));
});

Breadcrumbs::for('bands.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('bands.index');
  $breadcrumb->push('Editar Faixa', route('admin.bands.edit', ['id' => $result->id]));
});

Breadcrumbs::for('bands_categories.index', function ($breadcrumb) {
  $breadcrumb->parent('bands.index');
  $breadcrumb->push('Categorias', route('admin.bands_categories.index'));
});

Breadcrumbs::for('bands_categories.create', function ($breadcrumb) {
  $breadcrumb->parent('bands_categories.index');
  $breadcrumb->push('Nova Categoria', route('admin.bands_categories.create'));
});

Breadcrumbs::for('bands_categories.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('bands_categories.index');
  $breadcrumb->push('Editar Categoria', route('admin.bands_categories.edit', ['id' => $result->id]));
});

Breadcrumbs::for('classrooms.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Turmas', route('admin.classrooms.index'));
});

Breadcrumbs::for('classrooms.create', function ($breadcrumb) {
  $breadcrumb->parent('classrooms.index');
  $breadcrumb->push('Nova Turma', route('admin.classrooms.create'));
});

Breadcrumbs::for('classrooms.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('classrooms.index');
  $breadcrumb->push('Visualizar Turma', route('admin.classrooms.show', ['id' => $result->id]));
});

Breadcrumbs::for('classrooms.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('classrooms.index');
  $breadcrumb->push('Editar Turma', route('admin.classrooms.edit', ['id' => $result->id]));
});

Breadcrumbs::for('classrooms_frequencies.index', function ($breadcrumb, $classroom) {
  $breadcrumb->parent('classrooms.index');
  $breadcrumb->push('Frequências', route('admin.classrooms_frequencies.index', ['classroom_id' => $classroom->id]));
});

Breadcrumbs::for('classrooms_frequencies.show', function ($breadcrumb, $classroom, $result) {
  $breadcrumb->parent('classrooms_frequencies.index', $classroom);
  $breadcrumb->push('Visualizar Frequência', route('admin.classrooms_frequencies.show', ['classroom_id' => $classroom->id, 'id' => $result->id]));
});

Breadcrumbs::for('classrooms_frequencies.create', function ($breadcrumb, $classroom) {
  $breadcrumb->parent('classrooms_frequencies.index', $classroom);
  $breadcrumb->push('Nova Frequência', route('admin.classrooms_frequencies.create', ['classroom_id' => $classroom->id]));
});

Breadcrumbs::for('classrooms_frequencies.edit', function ($breadcrumb, $classroom, $result) {
  $breadcrumb->parent('classrooms_frequencies.index', $classroom);
  $breadcrumb->push('Editar Frequência', route('admin.classrooms_frequencies.edit', ['classroom_id' => $classroom->id, 'id' => $result->id]));
});

Breadcrumbs::for('graduations.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Graduações', route('admin.graduations.index'));
});

Breadcrumbs::for('graduations.create', function ($breadcrumb) {
  $breadcrumb->parent('graduations.index');
  $breadcrumb->push('Nova Graduação', route('admin.graduations.create'));
});

Breadcrumbs::for('graduations.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('graduations.index');
  $breadcrumb->push('Editar Graduação', route('admin.graduations.edit', ['id' => $result->id]));
});

Breadcrumbs::for('graduations_rules.index', function ($breadcrumb) {
  $breadcrumb->parent('graduations.index');
  $breadcrumb->push('Regras', route('admin.graduations_rules.index'));
});

Breadcrumbs::for('graduations_rules.create', function ($breadcrumb) {
  $breadcrumb->parent('graduations_rules.index');
  $breadcrumb->push('Nova Regra', route('admin.graduations_rules.create'));
});

Breadcrumbs::for('graduations_rules.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('graduations_rules.index');
  $breadcrumb->push('Editar Regra', route('admin.graduations_rules.edit', ['id' => $result->id]));
});

Breadcrumbs::for('modalities.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Modalidades', route('admin.modalities.index'));
});

Breadcrumbs::for('modalities.create', function ($breadcrumb) {
  $breadcrumb->parent('modalities.index');
  $breadcrumb->push('Nova Modalidade', route('admin.modalities.create'));
});

Breadcrumbs::for('modalities.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('modalities.index');
  $breadcrumb->push('Editar Modalidade', route('admin.modalities.edit', ['id' => $result->id]));
});

Breadcrumbs::for('students.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Alunos', route('admin.students.index'));
});

Breadcrumbs::for('students.create', function ($breadcrumb) {
  $breadcrumb->parent('students.index');
  $breadcrumb->push('Novo Aluno', route('admin.students.create'));
});

Breadcrumbs::for('students.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('students.index');
  $breadcrumb->push('Visualizar Aluno', route('admin.students.show', ['id' => $result->id]));
});

Breadcrumbs::for('students.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('students.index');
  $breadcrumb->push('Editar Aluno', route('admin.students.edit', ['id' => $result->id]));
});

Breadcrumbs::for('students_origins.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Origens', route('admin.students_origins.index'));
});

Breadcrumbs::for('students_origins.create', function ($breadcrumb) {
  $breadcrumb->parent('students_origins.index');
  $breadcrumb->push('Nova Origem', route('admin.students_origins.create'));
});

Breadcrumbs::for('students_origins.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('students_origins.index');
  $breadcrumb->push('Editar Origem', route('admin.students_origins.edit', ['id' => $result->id]));
});

Breadcrumbs::for('students_tuitions.index', function ($breadcrumb, $result) {
  $breadcrumb->parent('students.index');
  $breadcrumb->push('Mensalidades', route('admin.students_tuitions.index', ['code' => $result->code]));
});

Breadcrumbs::for('students_tuitions.create', function ($breadcrumb, $result) {
  $breadcrumb->parent('students_tuitions.index', $result);
  $breadcrumb->push('Nova Mensalidade', route('admin.students_tuitions.create', ['code' => $result->code]));
});

Breadcrumbs::for('students_tuitions.show', function ($breadcrumb, $student, $result) {
  $breadcrumb->parent('students_tuitions.index', $student);
  $breadcrumb->push('Visualizar Mensalidade', route('admin.students_tuitions.show', ['code' => $student->code, 'id' => $result->id]));
});

Breadcrumbs::for('bills.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Contas', route('admin.bills.index'));
});

Breadcrumbs::for('bills.create', function ($breadcrumb) {
  $breadcrumb->parent('bills.index');
  $breadcrumb->push('Nova Conta', route('admin.bills.create'));
});

Breadcrumbs::for('bills.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('bills.index');
  $breadcrumb->push('Visualizar Conta', route('admin.bills.show', ['id' => $result->id]));
});

Breadcrumbs::for('bills.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('bills.index');
  $breadcrumb->push('Editar Conta', route('admin.bills.edit', ['id' => $result->id]));
});

Breadcrumbs::for('bills_types.index', function ($breadcrumb) {
  $breadcrumb->parent('bills.index');
  $breadcrumb->push('Tipos', route('admin.bills_types.index'));
});

Breadcrumbs::for('bills_types.create', function ($breadcrumb) {
  $breadcrumb->parent('bills_types.index');
  $breadcrumb->push('Novo Tipo', route('admin.bills_types.create'));
});

Breadcrumbs::for('bills_types.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('bills_types.index');
  $breadcrumb->push('Editar Tipo', route('admin.bills_types.edit', ['id' => $result->id]));
});

Breadcrumbs::for('bills_items.index', function ($breadcrumb) {
  $breadcrumb->parent('bills_types.index');
  $breadcrumb->push('Items', route('admin.bills_items.index'));
});

Breadcrumbs::for('bills_items.create', function ($breadcrumb) {
  $breadcrumb->parent('bills_items.index');
  $breadcrumb->push('Novo Item', route('admin.bills_items.create'));
});

Breadcrumbs::for('bills_items.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('bills_items.index');
  $breadcrumb->push('Editar Item', route('admin.bills_items.edit', ['id' => $result->id]));
});

Breadcrumbs::for('units.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Unidades', route('admin.units.index'));
});

Breadcrumbs::for('units.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('units.index');
  $breadcrumb->push('Visualizar Unidade', route('admin.units.show', ['id' => $result->id]));
});

Breadcrumbs::for('units.create', function ($breadcrumb) {
  $breadcrumb->parent('units.index');
  $breadcrumb->push('Nova Unidade', route('admin.units.create'));
});

Breadcrumbs::for('units.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('units.index');
  $breadcrumb->push('Editar Unidade', route('admin.units.edit', ['id' => $result->id]));
});

Breadcrumbs::for('units_responsibles.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Responsáveis', route('admin.units_responsibles.index'));
});

Breadcrumbs::for('units_responsibles.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('units_responsibles.index');
  $breadcrumb->push('Visualizar Responsável', route('admin.units_responsibles.show', ['id' => $result->id]));
});

Breadcrumbs::for('units_responsibles.create', function ($breadcrumb) {
  $breadcrumb->parent('units_responsibles.index');
  $breadcrumb->push('Novo Responsável', route('admin.units_responsibles.create'));
});

Breadcrumbs::for('units_responsibles.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('units_responsibles.index');
  $breadcrumb->push('Editar Responsável', route('admin.units_responsibles.edit', ['id' => $result->id]));
});

Breadcrumbs::for('teachers.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Professores', route('admin.teachers.index'));
});

Breadcrumbs::for('teachers.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('teachers.index');
  $breadcrumb->push('Visualizar Professor', route('admin.teachers.show', ['id' => $result->id]));
});

Breadcrumbs::for('teachers.create', function ($breadcrumb) {
  $breadcrumb->parent('teachers.index');
  $breadcrumb->push('Novo Professor', route('admin.teachers.create'));
});

Breadcrumbs::for('teachers.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('teachers.index');
  $breadcrumb->push('Editar Professor', route('admin.teachers.edit', ['id' => $result->id]));
});

Breadcrumbs::for('subscriptions.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Matrículas', route('admin.subscriptions.index'));
});

Breadcrumbs::for('subscriptions.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('subscriptions.index');
  $breadcrumb->push('Visualizar Matrícula', route('admin.subscriptions.show', ['id' => $result->id]));
});


Breadcrumbs::for('subscriptions.create', function ($breadcrumb) {
  $breadcrumb->parent('subscriptions.index');
  $breadcrumb->push('Nova Matrícula', route('admin.subscriptions.create'));
});

Breadcrumbs::for('subscriptions.edit', function ($breadcrumb, $result) {
  $breadcrumb->parent('subscriptions.index');
  $breadcrumb->push('Editar Matrícula', route('admin.subscriptions.edit', ['id' => $result->id]));
});

Breadcrumbs::for('birthdays.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Aniversariantes', route('admin.birthdays.index'));
});

Breadcrumbs::for('orders.index', function ($breadcrumb) {
  $breadcrumb->parent('dashboard');
  $breadcrumb->push('Vendas', route('admin.orders.index'));
});

Breadcrumbs::for('orders.show', function ($breadcrumb, $result) {
  $breadcrumb->parent('orders.index');
  $breadcrumb->push('Visualizar Venda', route('admin.orders.show', ['code' => $result->code]));
});
