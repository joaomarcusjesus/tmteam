@extends('admin.layouts.students')

@section('content')

<section class="section">
  <div class="section-body">
    <div class="row">
      <div class="col-12 col-md-6 col-lg-3">
        <div class="card">
          <div class="card-header">
            <h4>Frequência via QrCode</h4>
          </div>
          <div class="card-body text-center">
            <img src="{{ asset('/assets/img/qrcode.png') }}" alt="" width="200">
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h4>Aptidão Física</h4>
          </div>
          <div class="card-body">
            @if(auth()->user()->student->fisic)
              <div class="text-center">
                <img src="{{ asset('/assets/img/congratis.png') }}" width="100">
                <br>
                <span>Validado com sucesso!</span>
              </div>
            @else
              {!! Form::open(['novalidate', 'method' => 'post', 'files' => true, 'route' => ['api.students.fisic', 'code' => auth()->user()->student->code]]) !!}
              <div class="row">
                <div class="col-sm-12 col-md-12">
                  {!! Form::label('fisic', 'Faça o upload do arquivo', []) !!}
                  <div class="file-upload-wrapper">
                    <input name="fisic" type="file" class="file-upload-field" value="">
                    @if($errors->has('fisic'))
                      <span class="text-danger">{{ $errors->first('fisic') }}</span>
                    @endif
                  </div>
                </div>
                <div class="col-sm-12 col-md-12 mt-3">
                  <div class="form-group mb-0">
                    <button type="submit" class="btn btn-lg btn-info btn-icon mr-1 btn-block" title="Nova Matrícula"><i class="far fa-file"></i> Enviar</button>
                  </div>
                </div>
              </div>
              {!! Form::close() !!}
            @endif
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-9">
        <div class="card">
          <div class="card-header">
            <h4>Quadro de avisos</h4>
          </div>
          <div class="card-body">
             Aguardando novos avisos ;)
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h4>Progressão de faixas</h4>
          </div>
          <div class="card-body">
            @if($student->band)
            <div class="row">
              <div class="col-12">
                <div class="activities">
                  <div class="activity">
                    <div class="activity-icon bg-info text-white shadow-primary">
                      <i class="fas fa-star"></i>
                    </div>
                    <div class="activity-detail">
                      <div class="mb-2">
                        Faixa {{ $student->band->name }}  - Grau {{ $student->degree }}
                      </div>
                    </div>
                  </div>
                  <div class="activity">
                    <div class="activity-icon bg-info text-white shadow-primary">
                      <i class="fas fa-star"></i>
                    </div>
                    <div class="activity-detail">
                      <div class="mb-2">
                        Próxima: Faixa {{ $student->band->name }}  - Grau {{ $student->degree + 1 }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endif
          </div>

        </div>
        @if($student->frequencies->count() > 0)
        <div class="card">
          <div class="card-header">
            <h4>Treinos da semana</h4>
          </div>
          <div class="card-body -table mb-0">
            <div class="table-responsive">
              <table class="table table-striped overflow mb-0">
                <tbody>
                <tr class="tr-a">
                  <th>Turma</th>
                  <th>Treino</th>
                  <th>Instrutor</th>
                  <th>Status</th>
                </tr>
                  @foreach($student->frequencies as $frequency)
                  <tr class="tr-item td-border">
                    <td class="td-item">{{ $frequency->classroom->name }}</td>
                    <td class="td-item">-</td>
                    <td class="td-item">{{ $frequency->classroom->teacher->full_name }}</td>
                    <td class="td-item">{!! isActive($frequency->released)!!}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</section>

@endsection


