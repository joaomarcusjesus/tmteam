@extends('admin.layouts.students')

@section('content')

  <section class="section">
    <div class="section-body">
      <div class="row">
        <div class="col-md-12 col-lg-12">
        @if($student->frequencies->count() > 0)
          <div class="card ">
            <div class="card-header">
              <h4>Frequências</h4>
            </div>
            <div class="card-body -table mb-0">
              <div class="table-responsive">
                <table class="table table-striped overflow mb-0">
                  <tbody>
                  <tr class="tr-a">
                    <th>Turma</th>
                    <th>Treino</th>
                    <th>Instrutor</th>
                    <th>Revisado?</th>
                  </tr>
                  @foreach($student->frequencies as $frequency)
                    <tr class="tr-item td-border">
                      <td class="td-item">{{ $frequency->classroom->name }}</td>
                      <td class="td-item">-</td>
                      <td class="td-item">{{ $frequency->classroom->teacher->full_name }}</td>
                      <td class="td-item">{!! isReleased($frequency->released)!!}</td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        @endif
        </div>
      </div>
    </div>
  </section>

@endsection
