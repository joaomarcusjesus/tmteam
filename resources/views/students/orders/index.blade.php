@extends('admin.layouts.students')

@section('content')

  <section class="section">
    <div class="section-body">
      <div class="row">
        <div class="col-sm-12 col-lg-4 col-md-4">
          @if($student->subscription)
            <div class="card">
              <div class="card-header">
                <h4>Matrícula</h4>
              </div>
              <div class="card-body -scroll-intern">
                <div class="mb-2">
                  <b>Código  </b><span class="text-muted">{{ $student->subscription->reference }}</span>
                </div>
                <div class="mb-2">
                  <b>Preço  </b><span class="text-muted">{{ money(number_format($student->subscription->total, 2), 'BRL') }}</span>
                </div>
                @if($student->subscription->descont)
                  <div class="mb-2">
                    <b>Desconto  </b><span class="text-muted">{{ money($student->subscription->descont), 'BRL' }}</span>
                  </div>
                @endif
                <div class="mb-2">
                  <b>Tipo  </b><span class="text-muted">{{ $student->subscription->typeName }}</span>
                </div>
                <div class="mb-2">
                  <b>Status </b><span class="text-muted">{{ $student->subscription->statusName }}</span>
                </div>
                @if($student->isVirtual && !is_null($student->linkBolet))
                  <div class="mb-2">
                    <b>Link do boleto </b><span class="text-muted"><a href="{{ $student->linkBolet }}" target="_blank" title="Clique aqui">Clique aqui</a></span>
                  </div>
                @endif
                @if($student->subscription->isPaid)
                  <hr>
                  <div class="mb-2 mt-4 text-center">
                    <img src="{{ asset('/assets/img/congratis.png') }}">
                  </div>
                @endif
              </div>
            </div>
          @else
            <div class="card">
              <div class="card-header">
                <h4>Matrícula</h4>
              </div>
              <div class="card-body">
                <div class="mb-2">
                  <b>Preço  </b><span class="text-muted">{{ money($student->totalModalitiesSum, 'BRL') }}</span>
                </div>
                <div class="mb-2">
                  <b>Modalidades  </b><span class="text-muted">{{ $student->modalities->pluck('name')->implode(', ') }}</span>
                </div>
                <div class="form-group mb-2 mt-4">
                  <a href="{{ route('api.orders.subscription', ['code' => $student->code]) }}" class="btn btn-lg btn-success btn-icon mr-1 btn-block" title="Nova Matrícula"><i class="far fa-star"></i> Nova Matrícula</a>
                </div>
              </div>
            </div>
          @endif
        </div>
        <div class="col-sm-12 col-lg-8 col-md-8">
          @if($student->orders->count() > 0)
            <div class="card">
              <div class="card-header">
                <h4>Mensalidades</h4>
              </div>
              <div class="card-body -table mb-0">
                <div class="table-responsive">
                  <table class="table table-striped overflow mb-0">
                    <tbody>
                    <tr class="tr-a">
                      <th>#</th>
                      <th>Data</th>
                      <th>Aluno</th>
                      <th>Status</th>
                    </tr>
                    @foreach($student->tuitions as $result)
                      <tr class="tr-item td-border">
                        <td class="td-item">{{ $result->reference }}</td>
                        <td class="td-item">{{ $result->created_at->format('d/m/Y H:i') }}</td>
                        <td class="td-item">{{ $result->student->full_name }}</td>
                        <td class="td-item"> {!! $result->statusBadge !!}</td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          @endif
        </div>
      </div>
    </div>
  </section>

@endsection
