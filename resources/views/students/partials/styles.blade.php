<link rel="stylesheet" href="{{ asset('/assets/modules/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/modules/fontawesome/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/bootstrap-social/bootstrap-social.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/components.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/custom.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/student.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/izitoast/dist/css/iziToast.min.css') }}">
