<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
  <a href="{{ route('students.dashboard.index') }}" class="navbar-brand sidebar-gone-hide">
    <img src="{{ asset('/assets/img/brand.png') }}" width="80">
    <span class="text-dark">Tm team</span>
  </a>
  <div class="navbar-nav">
  </div>
  <div class="nav-collapse">
  </div>
  <form class="form-inline ml-auto">
  </form>
  <ul class="navbar-nav navbar-right">

    <li class="dropdown">
      <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
        <img alt="image" src="{{ auth()->user()->photo }}" class="rounded-circle mr-1">
        <div class="d-sm-none d-lg-inline-block text-dark">Hi, {{ auth()->user()->full_name }}
        </div>
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <a href="{{ route('students.frequencies.index') }}" class="dropdown-item has-icon">
          <i class="far fa-list-alt"></i> Frequências
        </a>
        <a href="{{ route('students.accounts.index') }}" class="dropdown-item has-icon">
          <i class="far fa-user"></i> Perfil
        </a>
        <a href="{{ route('students.orders.index') }}" class="dropdown-item has-icon">
          <i class="far fa-star"></i> Pagamentos
        </a>
        <div class="dropdown-divider"></div>
        <a href="{{ route('admin.session.logout') }}" class="dropdown-item has-icon text-danger">
          <i class="fas fa-sign-out-alt"></i> Sair
        </a>
      </div>
    </li>
  </ul>
</nav>
