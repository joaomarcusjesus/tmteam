@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header mb-3">
    <h1>Treinos</h1>
    @can('add_classrooms_submodules')
      <div class="section-header-button mr-2">
        <a href="{{ route('admin.classrooms.index') }}" class="btn btn-danger btn-icon btn-lg mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <a href="{{ route('admin.classrooms_categories.create') }}" class="btn btn-success btn-icon btn-lg" title="Adicionar"><i class="fas fa-plus"></i> Adicionar</a>
      </div>
    @endcan
    {!! Breadcrumbs::render('classrooms_categories.index') !!}
  </div>
  <div class="section-body">
    <div class="row mt-4">
      <div class="col-lg-12 col-sm-12">
        <h6 class="mb-3">Listagem de treinos</h6>
        <div class="card">
          <div class="card-body -table mb-0">
            <div class="table-responsive">
              <table class="table table-striped overflow mb-0">
                <tbody>
                  <tr class="tr-a">
                    <th>Nome</th>
                    <th>Status</th>
                    <th>Ações</th>
                  </tr>
                  @foreach($results as $result)
                    <tr class="tr-item td-border">
                      <td class="td-item">{{ $result->name }}</td>
                      <td class="td-item">{!! isActive($result->active) !!}</td>
                      <td class="td-item">

                        @can('edit_classrooms_submodules')
                          <a href="{{ route('admin.classrooms_categories.edit', ['id' => $result->id]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"> <i class="icon far fa-edit lg"></i></a>
                        @endcan
                        @can('delete_classrooms_submodules')
                          <a  href="#" class="js-confirm-delete" data-link="{{ route('admin.classrooms_categories.destroy', ['id' => $result->id]) }}" data-title="{{ $result->student->full_name }}"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Excluir"> <i class="icon far fa-trash-alt lg"></i></a>
                        @endcan
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 offset-lg-4 col-sm-12 mb-2">
          {!! $results->render() !!}
      </div>
    </div>
  </div>
</section>
@endsection

