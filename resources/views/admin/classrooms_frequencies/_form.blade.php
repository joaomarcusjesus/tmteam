<div class="col-lg-12 col-sm-12">
  <div class="card">
    <div class="card-header -warning">
      <h4>
        <i class="far fa-edit lga"></i>
        Detalhes
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('date', 'Data', ['class' => 'label-required']) !!}
            {!! Form::text('date', isset($result->date) ? $result->date->format('d/m/Y H:i') : old('date'), ['class' => 'form-control js-datetimepicker']) !!}
            @if($errors->has('date'))
              <span class="text-danger">{{ $errors->first('date') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('student_id', 'Aluno', ['class' => 'label-required']) !!}
            {!! Form::select('student_id', $students, old('student_id'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('student_id'))
              <span class="text-danger">{{ $errors->first('student_id') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-12">
          <div class="form-group mb-2">
            {!! Form::label('body', 'Observações', ['class' => '']) !!}
            {!! Form::textarea('body', old('body'), ['class' => 'form-control']) !!}
            @if($errors->has('body'))
              <span class="text-danger">{{ $errors->first('body') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
