@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header mb-3">
    <h1>{{ $classroom->name }} - Frequências</h1>
    @can('add_classrooms')
      <div class="section-header-button mr-2">
        <a href="{{ route('admin.classrooms.index') }}" class="btn btn-danger btn-icon btn-lg mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <a href="{{ route('admin.classrooms_frequencies.create', ['classroom_id' => $classroom->id]) }}" class="btn btn-success btn-icon btn-lg" title="Adicionar"><i class="fas fa-plus"></i> Adicionar</a>
      </div>
    @endcan
    {!! Breadcrumbs::render('classrooms_frequencies.index', $classroom) !!}
  </div>
  <div class="row">
    <div class="col-12">
      <h6 class="mb-3">Filtros</h6>
      <div class="card mb-0">
        <div class="card-body mb-0">
          {!! Form::open(['class' => 'form', 'novalidate', 'route' => ['admin.classrooms_frequencies.index', 'classroom_id' => $classroom->id], 'method' => 'GET']) !!}
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 mb-0 form-group">
                {!! Form::select('aluno', $students, Request::get('aluno'), ['class' => 'form-control', 'placeholder' => 'Aluno']) !!}
              </div>
              <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                  {!! Form::text('data', Request::get('data'), ['class' => 'form-control js-datepicker', 'placeholder' => 'Data']) !!}
                </div>
              <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                {!! Form::select('status', [0 => 'Não', 1 => 'Sim'], Request::get('status'), ['class' => 'form-control', 'placeholder' => 'Revisado? ']) !!}
              </div>
              <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                <a href="#" class="btn btn-icon btn-block btn-danger" title="Limpar Campos">
                  <i class="fas fa-ban lg-icon"></i>
                </a>
              </div>
              <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                <button type="submit" class="btn btn-icon btn-block btn-success">
                  <i class="fas fa-search lg-icon"></i>
                </button>
              </div>
            </div>
         {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

  <div class="section-body">
    <div class="row mt-4">
      <div class="col-lg-12 col-sm-12">
        <h6 class="mb-3">Listagem de frequências cadastradas</h6>
        <div class="card">
          <div class="card-body -table mb-0">
            <div class="table-responsive">
              <table class="table table-striped overflow mb-0">
                <tbody>
                  <tr class="tr-a">
                    <th>Aluno</th>
                    <th>Horário</th>
                    <th>Revisado?</th>
                    <th>Ações</th>
                  </tr>
                  @foreach($results as $result)
                    <tr class="tr-item td-border">
                      <td class="td-item">{{ $result->student->full_name }}</td>
                      <td class="td-item">{{ $result->date->format('d/m/Y H:i') }}</td>
                      <td class="td-item">{!! isReleased($result->released) !!}</td>
                      <td class="td-item">

                        @can('view_classrooms_submodules')
                        <a href="{{ route('admin.classrooms_frequencies.show', ['classroom_id' => $classroom->id, 'id' => $result->id]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar"> <i class="icon far fa-eye lg"></i></a>
                        @endcan

                        @can('edit_classrooms_submodules')
                          <a href="{{ route('admin.classrooms_frequencies.edit', ['classroom_id' => $classroom->id, 'id' => $result->id]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"> <i class="icon far fa-edit lg"></i></a>
                        @endcan
                        @can('delete_classrooms_submodules')
                          <a  href="#" class="js-confirm-delete" data-link="{{ route('admin.classrooms_frequencies.destroy', ['classroom_id' => $classroom->id, 'id' => $result->id]) }}" data-title="{{ $result->student->full_name }}"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Excluir"> <i class="icon far fa-trash-alt lg"></i></a>
                        @endcan
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 offset-lg-4 col-sm-12 mb-2">
          {!! $results->render() !!}
      </div>
    </div>
  </div>
</section>
@endsection

