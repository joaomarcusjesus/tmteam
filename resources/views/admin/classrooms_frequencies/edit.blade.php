@extends('admin.layouts.default')

@section('content')

  {!! Form::model($result, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.classrooms_frequencies.update', 'classroom_id' => $classroom->id, 'id' => $result->id, 'files' => true]]) !!}
  <section class="section">
    <div class="section-header mb-0">
      <div class="section-header-back">
        <a href="{{ route('admin.classrooms_frequencies.index', ['classroom_id' => $classroom->id]) }}" class="btn btn-icon" title="Voltar">
          <i class="fas fa-arrow-left"></i>
        </a>
      </div>
      <h1>Editar Turma - {{ $classroom->name }} - Frequência</h1>
      {!! Breadcrumbs::render('classrooms_frequencies.edit', $classroom, $result) !!}
    </div>

    <div class="section-options">
      <div class="text-right mb-2">
        <label class="mt-2 mr-2">
          {!! Form::hidden('released', 0) !!}
          {!! Form::checkbox('released', true, isset($result) ? $result->released : false, ['class' => 'custom-switch-input', 'id' => 'released']) !!}
          <span class="custom-switch-indicator"></span>
          <span class="custom-switch-description">Revisado?</span>
        </label>
        <a href="{{ route('admin.classrooms_frequencies.index', ['classroom_id' => $classroom->id]) }}" class="btn btn-danger btn-lg btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <button type="submit" class="btn btn-icon icon-left btn-success btn-lg"><i class="fas fa-check"></i> Salvar</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="section-body">
      <div class="row">
        @include('admin.classrooms_frequencies._form')
      </div>
    </div>
  </section>
  {!! Form::close() !!}

@endsection
