@extends('admin.layouts.default')

@section('content')
  <section class="section">
    <div class="section-header mb-0">
      <h1>Visualizar Frequência</h1>
      {!! Breadcrumbs::render('classrooms_frequencies.show', $classroom, $result) !!}
    </div>
    <div class="section-options">
      @can('view_classrooms_submodules')
        <div class="text-right mb-2">
          <a href="{{ route('admin.classrooms_frequencies.index', ['classroom_id' => $classroom->id]) }}" class="btn btn-lg btn-danger btn-icon" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        </div>
      @endcan
    </div>
    <div class="clearfix"></div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
           <div class="card">
            <div class="card-header">
              <h4><i class="far fa-clipboard lga"></i> Turma</h4>
            </div>
            <div class="card-body">
              <div class="mb-2">
                <b>Nome  </b><span clas="text-muted">{{ $result->classroom->name }}</span>
              </div>
              <div class="mb-2">
                <b>Professor  </b><span clas="text-muted">{{ $result->classroom->teacher->full_name }}</span>
              </div>
              <div class="mb-2">
                <b>Molidade </b><span clas="text-muted">{{ $result->classroom->modality->name }}</span>
              </div>
              <b>Status </b><span clas="text-muted">{!! isActive($result->classroom->active)!!}</span>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h4><i class="far fa-user lga"></i> Professor</h4>
            </div>
            <div class="card-body">
              <div class="mb-2">
                <b>Nome  </b><span clas="text-muted">{{ $result->classroom->teacher->full_name }}</span>
              </div>
              @if($result->classroom->teacher->nickname)
              <div class="mb-2">
                <b>Apelido  </b><span clas="text-muted">{{ $result->classroom->teacher->nickname }}</span>
              </div>
              @endif
              <div class="mb-2">
                <b>E-mail </b><span clas="text-muted">{{ $result->classroom->teacher->email }}</span>
              </div>
              @if($result->classroom->teacher->cpf_or_cnpj)
                <div class="mb-2">
                  <b>Identificação </b><span clas="text-muted">{{ $result->classroom->teacher->identificationMask }}</span>
                </div>
              @endif
              @if($result->classroom->teacher->phone)
                <div class="mb-2">
                  <b>Telefone </b><span clas="text-muted">{{ $result->classroom->teacher->phoneMask }}</span>
                </div>
              @endif
            </div>
          </div>
        </div>
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <div class="card-header -info">
              <h4><i class="fas fa-tasks lga"></i> Frequências do Aluno</h4>
            </div>
            <div class="card-body -table mb-0">
              <div class="table-responsive">
                <table class="table table-striped table-hover overflow mb-0">
                  <tbody>
                    <tr class="tr-a">
                      <th>Horário</th>
                      <th>Revisado?</th>
                      <th>Observação</th>
                    </tr>
                    @foreach($student->frequencies as $frequency)
                      <tr class="tr-item td-border">
                        <td class="td-item">{{ $frequency->created_at->format('d/m/Y H:i') }}</td>
                        <td class="td-item">{!! isReleased($result->released) !!}</td>
                        <td class="td-item">{{ $frequency->body }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
