@extends('admin.layouts.default')

@section('content')

  {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => 'admin.graduations_rules.store', 'files' => true]) !!}
  <section class="section">
    <div class="section-header mb-0">
      <h1>Nova Regra</h1>
      {!! Breadcrumbs::render('graduations_rules.create') !!}
    </div>

    <div class="section-options">
      <div class="text-right mb-2">
        <label class="mt-2 mr-2">
          {!! Form::hidden('no_limit', 0) !!}
          {!! Form::checkbox('no_limit', true, isset($result) ? $result->no_limit : false, ['class' => 'custom-switch-input', 'id' => 'no_limit']) !!}
          <span class="custom-switch-indicator"></span>
          <span class="custom-switch-description">Sem limite?</span>
        </label>
        <label class="mt-2 mr-2">
          {!! Form::hidden('is_kids', 0) !!}
          {!! Form::checkbox('is_kids', true, isset($result) ? $result->is_kids : false, ['class' => 'custom-switch-input', 'id' => 'is_kids']) !!}
          <span class="custom-switch-indicator"></span>
          <span class="custom-switch-description">Kids?</span>
        </label>
        <label class="mt-2 mr-2">
          {!! Form::hidden('active', 0) !!}
          {!! Form::checkbox('active', true, isset($result) ? $result->active : false, ['class' => 'custom-switch-input', 'id' => 'active']) !!}
          <span class="custom-switch-indicator"></span>
          <span class="custom-switch-description">Ativo?</span>
        </label>
        <a href="{{ route('admin.graduations_rules.index') }}" class="btn btn-danger btn-lg btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <button type="submit" class="btn btn-icon icon-left btn-success btn-lg"><i class="fas fa-check"></i> Salvar</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="section-body">
      <div class="row">
       @include('admin.graduations_rules._form')
      </div>
    </div>
  </section>
  {!! Form::close() !!}

@endsection
