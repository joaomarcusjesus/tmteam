<div class="col-lg-12 col-sm-12">
  <div class="card">
    <div class="card-header -warning">
      <h4>
        <i class="far fa-edit lga"></i>
        Detalhes
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('name', 'Nome', ['class' => 'label-required']) !!}
            {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
            @if($errors->has('name'))
              <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('classrooms', 'Aulas', ['class' => '']) !!}
            {!! Form::text('classrooms', old('classrooms'), ['class' => 'form-control']) !!}
            @if($errors->has('classrooms'))
              <span class="text-danger">{{ $errors->first('classrooms') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('limit', 'Limite', ['class' => '']) !!}
            {!! Form::text('limit', old('limit'), ['class' => 'form-control']) !!}
            @if($errors->has('limit'))
              <span class="text-danger">{{ $errors->first('limit') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('period', 'Período', ['class' => '']) !!}
            {!! Form::text('period', old('period'), ['class' => 'form-control']) !!}
            @if($errors->has('period'))
              <span class="text-danger">{{ $errors->first('period') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('month', 'Meses', ['class' => '']) !!}
            {!! Form::text('month', old('month'), ['class' => 'form-control']) !!}
            @if($errors->has('month'))
              <span class="text-danger">{{ $errors->first('month') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-4">
            {!! Form::label('band_id', 'Faixa', ['class' => 'label-required']) !!}
            {!! Form::select('band_id',$bands, old('band_id'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('band_id'))
              <span class="text-danger">{{ $errors->first('band_id') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
