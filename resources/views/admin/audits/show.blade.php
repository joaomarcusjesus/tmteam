@extends('admin.layouts.default')

@section('content')

  <section class="section">
    <div class="section-header mb-0">
      <h1>Visualizar</h1>
      {!! Breadcrumbs::render('audits.show', $result) !!}
    </div>
    <div class="section-options">
      @can('view_audits')
        <div class="text-right mb-2">
          <a href="{{ route('admin.audits.index') }}" class="btn btn-lg btn-danger btn-icon" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        </div>
      @endcan
    </div>
    <div class="clearfix"></div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
          <div class="card">
            <div class="card-header -warning">
              <h4>
                <i class="far fa-user mr-2 lga"></i> Auditor
              </h4>
            </div>
            <div class="card-body">
              <div class="mb-2">
                <b>Nome  </b><span clas="text-muted">{{ $result->user->full_name }}</span>
                <br>
                <b>E-mail  </b><span clas="text-muted">{{ $result->user->email }}</span>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h4>
                <i class="fas fa-cog mr-2 lga"></i> Log
              </h4>
            </div>
            <div class="card-body">
              <div class="mb-2">
                <span clas="text-muted">{{ $result->created_at->formatLocalized('%d %B %Y') }}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
