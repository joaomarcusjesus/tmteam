<div class="col-lg-12 col-sm-12">
  <div class="card">
    <div class="card-header -warning">
      <h4>
        <i class="far fa-edit lga"></i>
        Detalhes
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('type', 'Formato', ['class' => 'label-required']) !!}
            {!! Form::select('type', [1 => 'Mensal', 2 => 'Anual', 3 => 'Outro'], old('type'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('type'))
              <span class="text-danger">{{ $errors->first('type') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('date', 'Data do Pagamento', ['class' => '']) !!}
            {!! Form::text('date', old('date'), ['class' => 'form-control js-datepicker',]) !!}
            @if($errors->has('date'))
              <span class="text-danger">{{ $errors->first('date') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-4">
            {!! Form::label('type_id', 'Tipo', ['class' => 'label-required']) !!}
            {!! Form::select('type_id', $types, old('type_id'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('type_id'))
              <span class="text-danger">{{ $errors->first('type_id') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('value', 'Valor', ['class' => 'label-required']) !!}
            {!! Form::text('value', old('value'), ['class' => 'form-control js-mask-money']) !!}
            @if($errors->has('value'))
              <span class="text-danger">{{ $errors->first('value') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
