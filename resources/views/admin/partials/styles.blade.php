<!-- General CSS Files -->
<link rel="stylesheet" href="{{ asset('/assets/modules/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/modules/fontawesome/css/all.min.css') }}">

<link rel="stylesheet" href="{{ asset('/assets/bootstrap-social/bootstrap-social.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

<!-- CSS Libraries -->

<!-- Template CSS -->
<link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/components.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/custom.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/izitoast/dist/css/iziToast.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/js-duallistbox/bootstrap-duallistbox.min.css') }}">
