<div class="col-lg-12 col-sm-12">
  <div class="card">
    <div class="card-header -warning">
      <h4>
        <i class="far fa-edit lga"></i>
        Detalhes
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('name', 'Nome', ['class' => 'label-required']) !!}
            {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
            @if($errors->has('name'))
              <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
          </div>
        </div>
      <div class="col-sm-12 col-md-4">
        <div class="form-group mb-4">
          {!! Form::label('color', 'Color', ['class' => 'label-required']) !!}
          {!! Form::select('color', [1 => 'Branca', 2 => 'Cinza', 3 => 'Preta', 4 => 'Amarela', 5 => 'Marrom', 6 => 'Laranja', 7 => 'Verde', 8 => 'Azul', 9 => 'Roxa'], old('color'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
          @if($errors->has('color'))
            <span class="text-danger">{{ $errors->first('color') }}</span>
          @endif
        </div>
      </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('age_begin', 'Faixa Etária Inicial', ['class' => 'label-required']) !!}
            {!! Form::text('age_begin', old('age_begin'), ['class' => 'form-control']) !!}
            @if($errors->has('age_begin'))
              <span class="text-danger">{{ $errors->first('age_begin') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('age_finish', 'Faixa Etária Final', ['class' => 'label-required']) !!}
            {!! Form::text('age_finish', old('age_finish'), ['class' => 'form-control']) !!}
            @if($errors->has('age_finish'))
              <span class="text-danger">{{ $errors->first('age_finish') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
