@extends('admin.layouts.default')

@section('content')

  <section class="section">
    <div class="section-header">
      <h1>Faixas</h1>

      @can('add_bands')
        <div class="section-header-button mr-2">
          <a href="{{ route('admin.bands.create') }}" class="btn btn-success btn-lg mr-2" title="Adicionar"> <i class="fas fa-plus"></i> Adicionar</a>
          <a href="{{ route('admin.bands_categories.index') }}" class="btn btn-info btn-lg" title="Categorias"> <i class="fas fa-tag"></i> Categorias</a>
        </div>
      @endcan

      {!! Breadcrumbs::render('bands.index') !!}
    </div>

    <div class="section-body">
      <div class="row" id="sortable" data-url="{{ route('admin.bands.reorder') }}">
        @foreach($results as $result)
          <div class="col-lg-3 col-md-3 cols-sm-12" data-id="{{ $result->id }}">
            <div class="card -br card-sortable">
              <div class="card-body text-center">
                <div class="sortable-text mb-2 mt-2"><b>{{ $result->name }}</b></div>
                <div class="sortable-text mb-2 mt-2"><span class="faixa {{ $result->colorClass }}"></span></div>
              </div>
              <div class="card-footer text-right">
                @can('view_bands')
                  <a href="{{ route('admin.bands.edit', ['id' => $result->id]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"> <i class="icon far fa-edit lg"></i></a>
                @endcan

                @can('delete_bands')
                  <a  href="#" class="js-confirm-delete" data-link="{{ route('admin.bands.destroy', ['id' => $result->id]) }}" data-title="{{ $result->full_name }}"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Excluir"> <i class="icon far fa-trash-alt lg"></i></a>
                @endcan
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endsection
