<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="theme-color" content="#2c4e87">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  {!! SEO::generate() !!}
  @include('students.partials.styles')
  @include('admin.partials.favicons')
</head>
<body class="layout-3">
  <div id="app">
   <div class="main-wrapper container">
     @include('students.partials.nav')
     @include('toast::messages-jquery')
     <div class="main-content">
      @yield('content')
     </div>
     </div>
  </div>
  @include('students.partials.scripts')
</body>
</html>
