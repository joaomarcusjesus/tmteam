@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header mb-3">
    <h1>Matrículas</h1>
    {!! Breadcrumbs::render('subscriptions.index') !!}
  </div>
  <div class="row">
    <div class="col-12">
      <h6 class="mb-3">Filtros</h6>
      <div class="card mb-0">
        <div class="card-body mb-0">
          {!! Form::open(['class' => 'form', 'novalidate', 'route' => 'admin.subscriptions.index', 'method' => 'GET']) !!}
            <div class="row">
              <div class="col-lg-8 col-md-8 col-sm-12 mb-0 form-group">
                {!! Form::text('nome', Request::get('nome'), ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
              </div>
              <div class="col-lg-2 col-md-2 col-sm-12 mb-0 form-group">
                {!! Form::select('status', [0 => 'Desativado', 1 => 'Ativo'], old(''), ['class' => 'form-control', 'placeholder' => 'Status ']) !!}
              </div>
              <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                <a href="#" class="btn btn-icon btn-block btn-danger" title="Limpar Campos">
                  <i class="fas fa-ban lg-icon"></i>
                </a>
              </div>
              <div class="col-lg-1 col-md-1 col-sm-12 mb-0 form-group">
                <button type="submit" class="btn btn-icon btn-block btn-success">
                  <i class="fas fa-search lg-icon"></i>
                </button>
              </div>
            </div>
         {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

  <div class="section-body">
    <div class="row mt-4">
      <div class="col-lg-12 col-sm-12">
        <h6 class="mb-3">Listagem de matrículas cadastradas</h6>
        <div class="card">
          <div class="card-body -table mb-0">
            <div class="table-responsive">
              <table class="table table-striped overflow mb-0">
                <tbody>
                  <tr class="tr-a">
                    <th>Código</th>
                    <th>Aluno</th>
                    <th>Preço</th>
                    <th>Status</th>
                    <th>Ações</th>
                  </tr>
                  @foreach($results as $result)
                    <tr class="tr-item td-border">
                      <td class="td-item">{{ $result->reference }}</td>
                      <td class="td-item"><img class="border-100" src="{{ $result->student->photo }}" alt="{{ $result->student->full_name }}" width="25"> {{ $result->student->full_name }}</td>
                      <td class="td-item">{!! $result->student->totalModalities !!}</td>
                      <td class="td-item">{!! isPaid($result->status) !!}</td>
                      <td class="td-item">

                        @can('view_subscriptions')
                        <a href="{{ route('admin.subscriptions.show', ['code' => $result->code]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar"> <i class="icon far fa-eye lg"></i></a>
                        @endcan

                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 offset-md-3 col-lg-4 offset-lg-4 col-sm-12 mb-2">
          {!! $results->render() !!}
      </div>
    </div>
  </div>
</section>
@endsection

