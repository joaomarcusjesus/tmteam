@extends('admin.layouts.default')

@section('content')
  <section class="section">
    <div class="section-header mb-0">
      <h1>Visualizar Matrícula</h1>
      {!! Breadcrumbs::render('subscriptions.show', $result) !!}
    </div>
    <div class="section-options">
      @can('view_subscriptions')
        <div class="text-right mb-2">
          <a href="{{ route('admin.subscriptions.index') }}" class="btn btn-lg btn-danger btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        </div>
      @endcan
    </div>
    <div class="clearfix"></div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
          <div class="card">
            <div class="card-header">
              <h4><i class="far fa-star lga"></i> Matrícula</h4>
            </div>
            <div class="card-body">
              @if($result)
                <div class="mb-2">
                  <b>Código </b><span clas="text-muted">{{ $result->reference }}</span>
                </div>
              @endif

              @if($result)
                <div class="mb-2">
                  <b>Status </b><span clas="text-muted">{!! isPaid($result->status) !!}</span>
                </div>
              @endif

              É preciso realizar o pagamento da matrícula, por favor acesse o link abaixo.
              <br>
              <a href="" class="trigger--fire-modal-1" id="js-matriculation" title="Clique aqui">Clique aqui</a>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h4><i class="fas fa-user-ninja lga"></i> Aluno</h4>
            </div>
            <div class="card-body">
              <div class="mb-2">
                <b>Nome  </b><span clas="text-muted">{{ $result->student->full_name }}</span>
              </div>
              <div class="mb-2">
                <b>E-mail </b><span clas="text-muted">{{ $result->student->email }}</span>
              </div>
              @if($result->student->cpf_or_cnpj)
                <div class="mb-2">
                  <b>Identificação </b><span clas="text-muted">{{ $result->student->identificationMask }}</span>
                </div>
              @endif
              @if($result->student->phone)
                <div class="mb-2">
                  <b>Telefone </b><span clas="text-muted">{{ $result->student->phoneMask }}</span>
                </div>
              @endif
              <hr>
              <div class="mb-2">
                <b>Rua  </b><span clas="text-muted">{{ $result->student->placeable->street }}, {{ $result->student->placeable->number }}</span>
              </div>
              <div class="mb-2">
                <b>Cidade </b><span clas="text-muted">{{ $result->student->placeable->city }}, {{ $result->student->placeable->state }}</span>
              </div>
              <div class="mb-2">
                <b>CEP </b><span clas="text-muted">{{ $result->student->placeable->cep }}</span>
              </div>
              @if($result->student->placeable->complement)
                <div class="mb-2">
                  <b>Complemento </b><span clas="text-muted">{{ $result->student->placeable->complement}}</span>
                </div>
              @endif
            </div>
          </div>
          @if($result->student->responsible)
            <div class="card">
              <div class="card-header">
                <h4><i class="fas fa-user-tie lga"></i>  Responsável</h4>
              </div>
              <div class="card-body">
                <div class="mb-2">
                  <b>Nome  </b><span clas="text-muted">{{ $result->student->responsible->full_name }}</span>
                </div>
                <div class="mb-2">
                  <b>E-mail </b><span clas="text-muted">{{ $result->student->responsible->email }}</span>
                </div>
                @if($result->student->responsible->cpf_or_cnpj)
                  <div class="mb-2">
                    <b>Identificação </b><span clas="text-muted">{{ $result->student->responsible->identificationMask }}</span>
                  </div>
                @endif
                @if($result->student->responsible->phone)
                  <div class="mb-2">
                    <b>Telefone </b><span clas="text-muted">{{ $result->student->responsible->phoneMask }}</span>
                  </div>
                @endif
              </div>
            </div>
          @endif
        </div>
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <div class="card-header -info">
              <h4><i class="fas fa-dumbbell lga"></i> Modalidades</h4>
            </div>
             <div class="card-body -table mb-0">
              <div class="table-responsive">
                <table class="table table-hover table-striped overflow mb-0">
                   <tbody>
                    <tr class="tr-a">
                      <th>Nome</th>
                      <th>Preço</th>
                      <th>Status</th>
                    </tr>
                    @foreach($result->student->modalities as $modality)
                      <tr class="tr-item td-border">
                        <td class="td-item">{{ $modality->name }}</td>
                        <td class="td-item">{{ money($modality->price, 'BRL') }}</td>
                        <td class="td-item">{!! isActive($modality->active) !!}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer">
              <p class="text-right mb-0">Total de mensalidade: <b>{!! $result->student->totalModalities !!}</b></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
