<div class="col-lg-4 col-sm-12">
  <div class="card">
    <div class="card-header">
      <h4>
        <i class="far fa-heart star"></i>
        Pagamento
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="form-group mb-2">
            {!! Form::label('type', 'Forma de Pagamento', ['class' => 'label-required']) !!}
            {!! Form::select('type', [1 => 'Presencial', 2 => 'Virtual'], old('type'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('type'))
              <span class="text-danger">{{ $errors->first('type') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-6">
          <div class="form-group mb-2">
            {!! Form::label('descont', 'Desconto', ['class' => '']) !!}
            {!! Form::number('descont', old('descont'), ['class' => 'form-control']) !!}
            @if($errors->has('descont'))
              <span class="text-danger">{{ $errors->first('descont') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
