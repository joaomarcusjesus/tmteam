@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Dashboard</h1>
    {!! Breadcrumbs::render('dashboard') !!}
  </div>
  <div class="section-body">
    @hasrole('root')
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
          <div class="card card-statistic-2">
            <div class="card-icon shadow-second bg-success">
              <i class="fas fa-dollar-sign"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total de matrículas</h4>
              </div>
              <div class="card-body">
                0
              </div>
            </div>
          </div>
          <div class="card card-statistic-2">
            <div class="card-icon shadow-second bg-warning">
              <i class="far fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total de alunos</h4>
              </div>
              <div class="card-body">
                0
              </div>
            </div>
          </div>
          <div class="card card-statistic-2">
            <div class="card-icon shadow-second bg-info">
              <i class="fas fa-graduation-cap"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total de graduações por mês</h4>
              </div>
              <div class="card-body">
                0
              </div>
            </div>
          </div>
          <div class="card card-statistic-2">
            <div class="card-icon shadow-second bg-info">
              <i class="fas fa-star"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total de professores</h4>
              </div>
              <div class="card-body">
                0
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card graph-card dashboard-card">
            <div class="card-header -info">
              <h4> <icon class="fas fa-chart-line lga"></icon> Desempenho</h4>
            </div>
            <div class="card-body">
            </div>
          </div>
        </div>
      </div>
    @endhasrole

    @hasrole('professor')
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
          <div class="card card-statistic-2">
            <div class="card-icon shadow-second bg-success">
              <i class="fas fa-dollar-sign"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Meus ganhos</h4>
              </div>
              <div class="card-body">
                0
              </div>
            </div>
          </div>
          <div class="card card-statistic-2">
            <div class="card-icon shadow-second bg-warning">
              <i class="far fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Meus Alunos</h4>
              </div>
              <div class="card-body">
                0
              </div>
            </div>
          </div>
          <div class="card card-statistic-2">
            <div class="card-icon shadow-second bg-info">
              <i class="fas fa-graduation-cap"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Próximas graduações</h4>
              </div>
              <div class="card-body">
                0
              </div>
            </div>
          </div>
          <div class="card card-statistic-2">
            <div class="card-icon shadow-second bg-info">
              <i class="fas fa-star"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Minhas Turmas</h4>
              </div>
              <div class="card-body">
                0
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card graph-card dashboard-card">
            <div class="card-header -info">
              <h4> <icon class="fas fa-bullhorn lga"></icon> Quadro de avisos</h4>
            </div>
            <div class="card-body">
            </div>
          </div>
        </div>
      </div>
    @endhasrole
  </div>
</section>

@endsection
