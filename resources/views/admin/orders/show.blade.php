@extends('admin.layouts.default')

@section('content')
  <section class="section">
    <div class="section-header mb-0">
      <h1>Visualizar Venda - {{ $result->reference }}</h1>
      {!! Breadcrumbs::render('orders.show', $result) !!}
    </div>
    <div class="section-options">
      <div class="text-right mb-2">
        <a href="{{ route('admin.orders.index') }}" class="btn btn-lg btn-danger btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
          <div class="card">
            <div class="card-header">
              <h4><i class="fas fa-barcode lga"></i> Venda</h4>
            </div>
            <div class="card-body -scroll-intern">
              <div class="mb-2">
                <b>Código de segurança  </b><span class="text-muted">{{ $result->registration }}</span>
              </div>
              @if($result->code_gateway)
                <div class="mb-2">
                  <b>Código de integração  </b><span class="text-muted">{{ $result->code_gateway }}</span>
                </div>
              @endif
              <div class="mb-2">
                <b>Data  </b><span class="text-muted">{{ $result->created_at->format('d/m/Y') }}</span>
              </div>
              <div class="mb-2">
                <b>Preço  </b><span class="text-muted">{{ money(number_format($result->amount, 2), 'BRL') }}</span>
              </div>
              <div class="mb-2">
                <b>Aluno  </b><span class="text-muted">{{ $result->student->full_name }}</span>
              </div>
              <div class="mb-2">
                <b>Tipo  </b><span class="text-muted">{{ $result->typeName }}</span>
              </div>
              <div class="mb-2">
                <b>Status  </b><span class="text-muted">{!! $result->statusBadge !!}</span>
              </div>
              @if($result->boleto_url)
                <div class="mb-2">
                  <b>Link do boleto  </b><span class="text-muted"><a href="{{ $result->boleto_url }}" target="_blank" title="Link do boleto">Clique aqui</a></span>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
