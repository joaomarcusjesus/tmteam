@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header mb-3">
    <h1>Vendas</h1>
    {!! Breadcrumbs::render('orders.index') !!}
  </div>
  <div class="section-body">
    <div class="row mt-4">
      <div class="col-lg-12 col-sm-12">
        <h6 class="mb-3">Listagem de Vendas</h6>
        <div class="card">
          <div class="card-body -table mb-0">
            <div class="table-responsive">
              <table class="table table-striped overflow mb-0">
                <tbody>
                  <tr class="tr-a">
                    <th>#</th>
                    <th>Data</th>
                    <th>Aluno</th>
                    <th>Status</th>
                    <th>Ações</th>
                  </tr>
                  @foreach($results as $result)
                    <tr class="tr-item td-border">
                      <td class="td-item">{{ $result->reference }}</td>
                      <td class="td-item">{{ $result->created_at->format('d/m/Y H:i') }}</td>
                      <td class="td-item">{{ $result->student->full_name }}</td>
                      <td class="td-item"> {!! $result->statusBadge !!}</td>
                      <td class="td-item">
                        @can('view_orders')
                          <a href="{{ route('admin.orders.show', ['code' => $result->code]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar"> <i class="icon lg far fa-eye"></i></a>
                        @endcan
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 offset-lg-4 col-sm-12 mb-2">
          {!! $results->render() !!}
      </div>
    </div>
  </div>
</section>
@endsection

