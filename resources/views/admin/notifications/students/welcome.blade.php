@component('mail::message')
  # Olá, {{ $user->first_name }}

  Em nome de toda a equipe da {{ config('app.name') }} gostaríamos de te dar as boas-vindas!

  Acesse a área do aluno e finalize o seu acesso.

  @component('mail::button', ['url' => $url, 'color' => 'green'])
    Área do aluno
  @endcomponent

  Atenciosamente,<br>
  {{ config('app.name') }}
@endcomponent
