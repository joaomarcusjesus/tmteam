@extends('admin.layouts.default')

@section('content')

  {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => 'admin.teachers.store', 'files' => true]) !!}
  <section class="section">
    <div class="section-header mb-0">
      <h1>Novo Professor</h1>
      {!! Breadcrumbs::render('teachers.create') !!}
    </div>

    <div class="section-options">
      <div class="text-right mb-2">
        <a href="{{ route('admin.teachers.index') }}" class="btn btn-danger btn-lg btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <button type="submit" class="btn btn-icon icon-left btn-success btn-lg"><i class="fas fa-check"></i> Salvar</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="section-body">
      <div class="row">
       @include('admin.teachers._form')
      </div>
    </div>
  </section>
  {!! Form::close() !!}

@endsection
