@extends('admin.layouts.default')

@section('content')
  <section class="section">
    <div class="section-header mb-0">
      <h1>Visualizar Professor</h1>
      {!! Breadcrumbs::render('teachers.show', $result) !!}
    </div>
    <div class="section-options">
      @can('view_teachers')
        <div class="text-right mb-2">
          <a href="{{ route('admin.teachers.index') }}" class="btn btn-lg btn-danger btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        </div>
      @endcan
    </div>
    <div class="clearfix"></div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
          <div class="card">
            <div class="card-header">
              <h4><i class="fas fa-chalkboard-teacher lga"></i> Professor</h4>
            </div>
            <div class="card-body">
              <div class="mb-2">
                <b>Nome  </b><span clas="text-muted">{{ $result->full_name }}</span>
              </div>
              <div class="mb-2">
                <b>E-mail </b><span clas="text-muted">{{ $result->email }}</span>
              </div>
              <div class="mb-2">
                <b>Último acesso</b><span clas="text-muted"> {{ $result->last_login }}</span>
              </div>
              @if($result->cpf_or_cnpj)
                <div class="mb-2">
                  <b>Identificação </b><span clas="text-muted">{{ $result->identificationMask }}</span>
                </div>
              @endif
              @if($result->phone)
                <div class="mb-2">
                  <b>Telefone </b><span clas="text-muted">{{ $result->phoneMask }}</span>
                </div>
              @endif
            </div>
          </div>
        </div>
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <div class="card-header -info">
              <h4><i class="fas fa-dumbbell lga"></i> Modalidades</h4>
            </div>
             <div class="card-body -table mb-0">
              <div class="table-responsive">
                <table class="table table-hover table-striped overflow mb-0">
                   <tbody>
                    <tr class="tr-a">
                      <th>Nome</th>
                      <th>Preço</th>
                      <th>Status</th>
                    </tr>
                    @foreach($result->modalities as $modality)
                      <tr class="tr-item td-border">
                        <td class="td-item">{{ $modality->name }}</td>
                        <td class="td-item">{{ money($modality->price, 'BRL') }}</td>
                        <td class="td-item">{!! isActive($modality->active) !!}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
