@extends('admin.layouts.default')

@section('content')

  {!! Form::model($result, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.teachers.update', 'id' => $result->id, 'files' => true]]) !!}
  <section class="section">
    <div class="section-header mb-0">
      <h1>Editar Professor</h1>
      {!! Breadcrumbs::render('teachers.edit', $result) !!}
    </div>

    <div class="section-options">
      <div class="text-right mb-2">
        <a href="{{ route('admin.teachers.index') }}" class="btn btn-danger btn-lg btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <button type="submit" class="btn btn-icon icon-left btn-success btn-lg"><i class="fas fa-save"></i> Atualizar</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="section-body">
      <div class="row">
        @include('admin.teachers._form')
      </div>
    </div>
  </section>
  {!! Form::close() !!}

@endsection
