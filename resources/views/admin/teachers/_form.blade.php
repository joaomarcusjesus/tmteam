
<div class="col-lg-6 col-sm-12">
  <div class="card">
    <div class="card-header -warning">
      <h4>
        <i class="far fa-user-circle lga"></i>
        Dados do Professor
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-6">
          <div class="form-group mb-4">
            {!! Form::label('teachers[first_name]', 'Nome', ['class' => 'label-required']) !!}
            {!! Form::text('teachers[first_name]', isset($result->first_name) ? $result->first_name : old('teachers.first_name'), ['class' => 'form-control']) !!}
            @if($errors->has('teachers.first_name'))
              <span class="text-danger">{{ $errors->first('teachers.first_name') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-6">
          <div class="form-group mb-4">
            {!! Form::label('teachers[last_name]', 'Sobrenome', ['class' => 'label-required']) !!}
            {!! Form::text('teachers[last_name]', isset($result->last_name) ? $result->last_name : old('teachers.last_name'), ['class' => 'form-control']) !!}
            @if($errors->has('teachers.last_name'))
              <span class="text-danger">{{ $errors->first('teachers.last_name') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-4">
            {!! Form::label('teachers[email]', 'E-mail', ['class' => 'label-required']) !!}
            {!! Form::text('teachers[email]', isset($result->email) ? $result->email : old('teachers.email'), ['class' => 'form-control']) !!}
            @if($errors->has('teachers.email') || $errors->has('users.email'))
              <span class="text-danger">{{ $errors->first('teachers.email') }}</span>
              <span class="text-danger">{{ $errors->first('users.email') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-4">
            {!! Form::label('teachers[cpf_or_cnpj]', 'CPF', ['class' => 'label-required']) !!}
            {!! Form::text('teachers[cpf_or_cnpj]', isset($result->cpf_or_cnpj) ? $result->cpf_or_cnpj : old('teachers.cpf_or_cnpj'), ['class' => 'form-control js-mask-cpf']) !!}
            @if($errors->has('teachers.cpf_or_cnpj') || $errors->has('users.cpf_or_cnpj'))
              <span class="text-danger">{{ $errors->first('teachers.cpf_or_cnpj') }}</span>
              <span class="text-danger">{{ $errors->first('users.cpf_or_cnpj') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('teachers[phone]', 'Telefone', ['class' => 'label-required']) !!}
            {!! Form::text('teachers[phone]',  isset($result->phone) ? $result->phone : old('teachers.phone'), ['class' => 'form-control js-mask-phone']) !!}
            @if($errors->has('teachers.phone'))
              <span class="text-danger">{{ $errors->first('teachers.phone') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('teachers[birthday]', 'Data de Nascimento', ['class' => '']) !!}
            {!! Form::text('teachers[birthday]', isset($result->birthday) ? $result->birthday->format('d/m/Y') : old('teachers.birthday'), ['class' => 'form-control js-datepicker']) !!}
            @if($errors->has('teachers.birthday'))
              <span class="text-danger">{{ $errors->first('teachers.birthday') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-4">
            {!! Form::label('teachers[nickname]', 'Apelido', ['class' => '']) !!}
            {!! Form::text('teachers[nickname]', isset($result->nickname) ? $result->nickname : old('teachers.nickname'), ['class' => 'form-control']) !!}
            @if($errors->has('teachers.nickname'))
              <span class="text-danger">{{ $errors->first('teachers.nickname') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('teachers[facebook]', 'Facebook', ['class' => '']) !!}
            {!! Form::text('teachers[facebook]', isset($result->facebook) ? $result->facebook : old('teachers.facebook'), ['class' => 'form-control']) !!}
            @if($errors->has('teachers.facebook'))
              <span class="text-danger">{{ $errors->first('teachers.facebook') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-3">
            {!! Form::label('teachers[instagram]', 'Instagram', ['class' => '']) !!}
            {!! Form::text('teachers[instagram]', isset($result->instagram) ? $result->instagram : old('teachers.instagram'), ['class' => 'form-control']) !!}
            @if($errors->has('teachers.instagram'))
              <span class="text-danger">{{ $errors->first('teachers.instagram') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-12">
          <div class="form-group mb-2">
            {!! Form::label('teachers[body]', 'Observações', ['class' => '']) !!}
            {!! Form::textarea('teachers[body]', isset($result->body) ? $result->body : old('teachers.body'), ['class' => 'form-control']) !!}
            @if($errors->has('teachers.body'))
              <span class="text-danger">{{ $errors->first('teachers.body') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-lg-6 col-sm-12">

   <div class="card">
    <div class="card-header -info">
      <h4>
        <i class="fas fa-dumbbell  lga"></i>
        Modalidades
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="form-group mb-2">
            {!! Form::label('modalities', 'Modalidades', ['class' => 'label-required']) !!}
            {!! Form::select('modalities[]', $modalities, isset($result->modalities) ? $result->modalities->pluck('id') : old('modalities'), ['class' => 'form-control js-box js-select-target', 'multiple']) !!}
            @if($errors->has('modalities'))
              <span class="text-danger">{{ $errors->first('modalities') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
