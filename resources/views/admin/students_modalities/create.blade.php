@extends('admin.layouts.default')

@section('content')

  {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => 'admin.modalities.store', 'files' => true]) !!}
  <section class="section">
    <div class="section-header mb-0">
      <h1>Nova Modalidade</h1>
      {!! Breadcrumbs::render('modalities.create') !!}
    </div>

    <div class="section-options">
      <div class="text-right mb-2">
        <label class="mt-2 mr-2">
          {!! Form::hidden('active', 0) !!}
          {!! Form::checkbox('active', true, isset($result) ? $result->active : false, ['class' => 'custom-switch-input', 'id' => 'active']) !!}
          <span class="custom-switch-indicator"></span>
          <span class="custom-switch-description">Ativo?</span>
        </label>
        <a href="{{ route('admin.modalities.index') }}" class="btn btn-danger btn-lg btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <button type="submit" class="btn btn-icon icon-left btn-success btn-lg"><i class="fas fa-check"></i> Salvar</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="section-body">
      <div class="row">
       @include('admin.modalities._form')
      </div>
    </div>
  </section>
  {!! Form::close() !!}

@endsection
