<div class="col-lg-12 col-sm-12">
  <div class="card">
    <div class="card-header -warning">
      <h4>
        <i class="far fa-edit lga"></i>
        Detalhes
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('name', 'Nome', ['class' => 'label-required']) !!}
            {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
            @if($errors->has('name'))
              <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('price', 'Preço', ['class' => 'label-required']) !!}
            {!! Form::text('price', old('price'), ['class' => 'form-control js-mask-money']) !!}
            @if($errors->has('price'))
              <span class="text-danger">{{ $errors->first('price') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('descont', 'Desconto', ['class' => '']) !!}
            {!! Form::number('descont', old('descont'), ['class' => 'form-control']) !!}
            @if($errors->has('descont'))
              <span class="text-danger">{{ $errors->first('descont') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
