@extends('admin.layouts.default')

@section('content')

  <section class="section">
    <div class="section-header mb-3">
      <h1>Aniversários</h1>
      {!! Breadcrumbs::render('teachers.index') !!}
    </div>
    <div class="section-body">
      <div class="row mt-4">
        <div class="col-lg-12 col-sm-12">
          <h6 class="mb-3">Listagem de Aniversários</h6>
          <div class="card">
            <div class="card-body -table mb-0">
              <div class="table-responsive">
                <table class="table table-striped overflow mb-0">
                  <tbody>
                  <tr class="tr-a">
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Aniversário</th>
                    <th>Status</th>
                  </tr>
                  @foreach($results as $result)
                    <tr class="tr-item td-border">
                      <td class="td-item"><img class="border-100" src="{{ $result->photo }}" alt="{{ $result->full_name }}" width="25"> {{ $result->full_name }}</td>
                      <td class="td-item">{{ $result->email }}</td>
                      <td class="td-item">{{ $result->birthday->format('d-m-Y') }}</td>
                      <td class="td-item">{!! isActive($result->active) !!}</td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 offset-md-3 col-lg-4 offset-lg-4 col-sm-12 mb-2">
          {!! $results->render() !!}
        </div>
      </div>
    </div>
  </section>
@endsection

