
<div class="col-lg-12 col-sm-12">
  <div class="card">
    <div class="card-header">
      <h4>
        <i class="far fa-user-circle lga"></i>
        Ficha Técnica
      </h4>
    </div>
    <div class="card-body">
      @include('admin.students.partials.form._students')
      @include('admin.students.partials.form._responsible')
      @include('admin.students.partials.form._address')
      @include('admin.students.partials.form._modalities')
      @include('admin.students.partials.form._medias')
    </div>
  </div>
</div>

@push('scripts')
<script>
$(document).ready(function() {
  $('.js-age').blur(function() {
    if ($(this).val() < 18) {
      $('.js-responsible').css('display', 'block');
    } else {
      $('.js-responsible').css('display', 'none');
    }
  });
});
</script>
@endpush
