<div class="js-responsible" style="display: none;">
  <div class="row">
    <div class="col-lg-12 mt-0 mb-2">
      <div class="section-title mt-2">Dados do responsável</div>
    </div>
    <div class="col-sm-12 col-md-6">
      <div class="form-group mb-4">
        {!! Form::label('responsible[first_name]', 'Nome', ['class' => '']) !!}
        {!! Form::text('responsible[first_name]', old('responsible.first_name'), ['class' => 'form-control']) !!}
        @if($errors->has('responsible.first_name'))
          <span class="text-danger">{{ $errors->first('responsible.first_name') }}</span>
        @endif
      </div>
    </div>
    <div class="col-sm-12 col-md-6">
      <div class="form-group mb-4">
        {!! Form::label('responsible[last_name]', 'Sobrenome', ['class' => '']) !!}
        {!! Form::text('responsible[last_name]', old('responsible.last_name'), ['class' => 'form-control']) !!}
        @if($errors->has('responsible.last_name'))
          <span class="text-danger">{{ $errors->first('responsible.last_name') }}</span>
        @endif
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
      <div class="form-group mb-4">
        {!! Form::label('responsible[email]', 'E-mail', ['class' => '']) !!}
        {!! Form::text('responsible[email]', old('responsible.email'), ['class' => 'form-control']) !!}
        @if($errors->has('responsible.email'))
          <span class="text-danger">{{ $errors->first('responsible.email') }}</span>
        @endif
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
      <div class="form-group mb-4">
        {!! Form::label('responsible[cpf_or_cnpj]', 'CPF', ['class' => '']) !!}
        {!! Form::text('responsible[cpf_or_cnpj]', old('responsible.cpf_or_cnpj'), ['class' => 'form-control js-mask-cpf']) !!}
        @if($errors->has('responsible.cpf_or_cnpj'))
          <span class="text-danger">{{ $errors->first('responsible.cpf_or_cnpj') }}</span>
        @endif
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
      <div class="form-group mb-2">
        {!! Form::label('responsible[phone]', 'Telefone', ['class' => '']) !!}
        {!! Form::text('responsible[phone]', old('responsible.phone'), ['class' => 'form-control js-mask-phone']) !!}
        @if($errors->has('responsible.phone'))
          <span class="text-danger">{{ $errors->first('responsible.phone') }}</span>
        @endif
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
      <div class="form-group mb-2">
        {!! Form::label('responsible[birthday]', 'Data de Nascimento', ['class' => '']) !!}
        {!! Form::text('responsible[birthday]', isset($result->responsible) ? $result->responsible->birthday->format('d/m/Y'): old('responsible.birthday'), ['class' => 'form-control js-datepicker']) !!}
        @if($errors->has('responsible.birthday'))
          <span class="text-danger">{{ $errors->first('responsible.birthday') }}</span>
        @endif
      </div>
    </div>
  </div>
</div>
