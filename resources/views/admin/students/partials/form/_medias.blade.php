<div class="row">
  <div class="col-lg-12 mt-0 mb-2">
    <div class="section-title mt-2">Arquivos</div>
  </div>
  <div class="col-sm-12 col-md-6">
    {!! Form::label('cover', 'Foto do Aluno', []) !!}
    <div class="file-upload-wrapper">
      <input name="cover" type="file" class="file-upload-field" value="">
      @if($errors->has('cover'))
        <span class="text-danger">{{ $errors->first('cover') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-6">
    {!! Form::label('fisic', 'Aptidão Física', []) !!}
    <div class="file-upload-wrapper">
      <input name="fisic" type="file" class="file-upload-field" value="">
      @if($errors->has('fisic'))
        <span class="text-danger">{{ $errors->first('fisic') }}</span>
      @endif
    </div>
  </div>
</div>
