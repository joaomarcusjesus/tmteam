<div class="row">
  <div class="col-lg-12 mt-0 mb-2">
    <div class="section-title mt-2">Molidades</div>
  </div>
  <div class="col-sm-12 col-md-12">
    <div class="form-group mb-2">
      {!! Form::select('modalities[]', $modalities, isset($result->modalities) ? $result->modalities->pluck('id') : old('modalities'), ['class' => 'form-control js-box js-modality', 'multiple']) !!}
      @if($errors->has('modalities'))
        <span class="text-danger">{{ $errors->first('modalities') }}</span>
      @endif
    </div>
  </div>
</div>
