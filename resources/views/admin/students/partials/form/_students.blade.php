<div class="row">
  <div class="col-lg-12 mt-0 mb-2">
    <div class="section-title mt-2">Dados pessoais</div>
  </div>
  <div class="col-sm-12 col-md-6">
    <div class="form-group mb-4">
      {!! Form::label('students[first_name]', 'Nome', ['class' => '']) !!}
      {!! Form::text('students[first_name]', isset($result->first_name) ? $result->first_name : old('students.first_name'), ['class' => 'form-control']) !!}
      @if($errors->has('students.first_name'))
        <span class="text-danger">{{ $errors->first('students.first_name') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-6">
    <div class="form-group mb-4">
      {!! Form::label('students[last_name]', 'Sobrenome', ['class' => '']) !!}
      {!! Form::text('students[last_name]', isset($result->last_name) ? $result->last_name : old('students.last_name'), ['class' => 'form-control']) !!}
      @if($errors->has('students.last_name'))
        <span class="text-danger">{{ $errors->first('students.last_name') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="form-group mb-4">
      {!! Form::label('students[nickname]', 'Apelido', ['class' => '']) !!}
      {!! Form::text('students[nickname]', isset($result->nickname) ? $result->nickname : old('students.nickname'), ['class' => 'form-control']) !!}
      @if($errors->has('students.nickname'))
        <span class="text-danger">{{ $errors->first('students.nickname') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="form-group mb-4">
      {!! Form::label('students[cbjj]', 'CBJJ', ['class' => '']) !!}
      {!! Form::text('students[cbjj]', isset($result->cbjj) ? $result->cbjj : old('students.cbjj'), ['class' => 'form-control']) !!}
      @if($errors->has('students.cbjj'))
        <span class="text-danger">{{ $errors->first('students.cbjj') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="form-group mb-4">
      {!! Form::label('students[email]', 'E-mail', ['class' => '']) !!}
      {!! Form::text('students[email]', isset($result->email) ? $result->email : old('students.email'), ['class' => 'form-control']) !!}
      @if($errors->has('students.email') || $errors->has('users.email'))
        <span class="text-danger">{{ $errors->first('students.email') }}</span>
        <span class="text-danger">{{ $errors->first('users.email') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="form-group mb-4">
      {!! Form::label('students[cpf_or_cnpj]', 'CPF', ['class' => '']) !!}
      {!! Form::text('students[cpf_or_cnpj]', isset($result->cpf_or_cnpj) ? $result->cpf_or_cnpj : old('students.cpf_or_cnpj'), ['class' => 'form-control js-mask-cpf']) !!}
       @if($errors->has('students.cpf_or_cnpj') || $errors->has('users.cpf_or_cnpj'))
        <span class="text-danger">{{ $errors->first('students.cpf_or_cnpj') }}</span>
        <span class="text-danger">{{ $errors->first('users.cpf_or_cnpj') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="form-group mb-2">
      {!! Form::label('students[phone]', 'Telefone', ['class' => '']) !!}
      {!! Form::text('students[phone]',  isset($result->phone) ? $result->phone : old('students.phone'), ['class' => 'form-control js-mask-phone']) !!}
      @if($errors->has('students.phone'))
        <span class="text-danger">{{ $errors->first('students.phone') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="form-group mb-4">
      {!! Form::label('students[birthday]', 'Data de Nascimento', ['class' => '']) !!}
      {!! Form::text('students[birthday]', isset($result->birthday) ? $result->birthday->format('d/m/Y') : old('students.birthday'), ['class' => 'form-control js-datepicker']) !!}
      @if($errors->has('students.birthday'))
        <span class="text-danger">{{ $errors->first('students.birthday') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="form-group mb-4">
      {!! Form::label('students[sexo]', 'Sexo', ['class' => '']) !!}
      {!! Form::select('students[sexo]', [1 => 'Masculino', 2 => 'Feminino'], isset($result->sexo) ? $result->sexo : old('students.sexo'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
      @if($errors->has('students.sexo'))
        <span class="text-danger">{{ $errors->first('students.sexo') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="form-group mb-4">
      {!! Form::label('students[age]', 'Idade', ['class' => '']) !!}
      {!! Form::number('students[age]', isset($result->age) ? $result->age : old('students.age'), ['class' => 'form-control js-age']) !!}
      @if($errors->has('students.age'))
        <span class="text-danger">{{ $errors->first('students.age') }}</span>
      @endif
    </div>
  </div>
  <div class="col-sm-12 col-md-4">
    <div class="form-group mb-4">
      {!! Form::label('students[origin_id]', 'Origem', ['class' => '']) !!}
      {!! Form::select('students[origin_id]', $origins,  isset($result->origin) ? $result->origin->id : old('students.origin_id'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
      @if($errors->has('students.origin_id'))
        <span class="text-danger">{{ $errors->first('students.origin_id') }}</span>
      @endif
    </div>
  </div>
</div>
