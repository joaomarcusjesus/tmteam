@if($jiujitsu && $bands->count() > 0)
  <div class="card p-b0">
      <div class="card-header">
        <h4><i class="far fa-star lga"></i> Escolher Faixa</h4>
      </div>
    {!! Form::open(['method' => 'post', 'novalidate', 'route' => ['admin.students.band', 'code' => $result->code]]) !!}
    <div class="card-body">
        <div class="row">
          <div class="form-group col-lg-4 mb-2">
            {!! Form::label('brandCategory', 'Categoria', ['class' => '']) !!}
            {!! Form::select('brandCategory', $bandCategories, old('brandCategory'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('brandCategory'))
              <span class="text-danger">{{ $errors->first('brandCategory') }}</span>
            @endif
          </div>
          <div class="form-group col-lg-4 mb-2">
            {!! Form::label('brand', 'Faixa', ['class' => '']) !!}
            {!! Form::select('brand', $bands, old('brand'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('brand'))
              <span class="text-danger">{{ $errors->first('brand') }}</span>
            @endif
          </div>
          <div class="form-group col-lg-4 mb-2">
            {!! Form::label('degree', 'Grau', ['class' => '']) !!}
            {!! Form::select('degree', [1 => '1', 2 => '2', 3 => '4', 4 => '4'], old('degree'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('degree'))
              <span class="text-danger">{{ $errors->first('degree') }}</span>
            @endif
          </div>
        </div>
      </div>
      <div class="card-footer">
         <div class="form-group mb-0 text-right">
          <button type="submit" class="btn btn-lg btn-success btn-icon mr-1" title="Atualizar"><i class="far fa-save"></i> Atualizar</button>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
@endif
