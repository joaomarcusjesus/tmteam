@if($result->modalities->count() > 0)
<div class="card">
  <div class="card-header">
    <h4><i class="fas fa-dumbbell lga"></i> Modalidades</h4>
  </div>
  <div class="card-body -table mb-0">
    <div class="table-responsive">
      <table class="table table-striped overflow mb-0">
         <tbody>
          <tr class="tr-a">
            <th>Nome</th>
            <th>Preço</th>
            <th>Status</th>
          </tr>
          @foreach($result->modalities as $modality)
            <tr class="tr-item td-border">
              <td class="td-item">{{ $modality->name }}</td>
              <td class="td-item">{{ money($modality->price, 'BRL') }}</td>
              <td class="td-item">{!! isActive($modality->active) !!}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer">
    <p class="text-right mb-0">Total da mensalidade: <b>{!! $result->totalModalities !!}</b></p>
  </div>
</div>
@endif
