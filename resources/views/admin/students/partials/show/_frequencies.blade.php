@if($result->frequencies->count() > 0)
  <div class="card">
    <div class="card-header">
      <h4><i class="fas far fa-list-alt lga"></i> Frequências</h4>
    </div>
    <div class="card-body -table mb-0">
      <table class="table table-striped overflow mb-0">
        <tbody>
          <tr class="tr-a">
            <th>Aluno</th>
            <th>Horário</th>
            <th>Revisado?</th>
            <th>Ações</th>
          </tr>
          @foreach($result->frequencies as $frenquency)
            <tr class="tr-item td-border">
              <td class="td-item">{{ $frenquency->student->full_name }}</td>
              <td class="td-item">{{ $frenquency->sendDate }}</td>
              <td class="td-item">{!! isReleased($frenquency->released) !!}</td>
              <td class="td-item">

                @can('view_classrooms_submodules')
                <a href="{{ route('admin.classrooms_frequencies.show', ['classroom_id' => $frenquency->classroom->id, 'id' => $frenquency->id]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar"> <i class="icon far fa-eye lg"></i></a>
                @endcan
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="card-footer">
      <p class="text-right mb-0">Presenças: <b>{!! $result->totalFrequencies !!}</b> - Faltas: <b>{!! $result->totalFaults !!}</b></p>
    </div>
  </div>
@endif
