<div class="card">
  <div class="card-header">
    <h4><i class="fas fa-user lga"></i> Aluno</h4>
  </div>
  <div class="card-body -scroll-intern">
    <div class="mb-2">
      <b>Nome  </b><span class="text-muted">{{ $result->full_name }}</span>
    </div>
    @if($result->band)
      <div class="mb-2">
        <b class="mr-2">Faixa </b><span class="faixa {{ $result->band->colorClass }}"> {{ $result->band->name }} - {{ $result->degreeName }}</span>
      </div>
    @endif
    @if($result->nickname)
      <div class="mb-2">
        <b>Apelido </b><span class="text-muted">{{ $result->nickname }}</span>
      </div>
    @endif
    @if($result->age)
      <div class="mb-2">
        <b>Idade </b><span class="text-muted">{{ $result->ageName }}</span>
      </div>
    @endif
    @if($result->cbjj)
      <div class="mb-2">
        <b>CBJJ </b><span class="text-muted">{{ $result->cbjj }}</span>
      </div>
    @endif
    @if($result->sexo)
      <div class="mb-2">
        <b>Sexo </b><span class="text-muted">{{ $result->sexoName }}</span>
      </div>
    @endif
    <div class="mb-2">
      <b>E-mail </b><span class="text-muted">{{ $result->email }}</span>
    </div>
    @if($result->cpf_or_cnpj)
      <div class="mb-2">
        <b>Identificação </b><span class="text-muted">{{ $result->identificationMask }}</span>
      </div>
    @endif
    @if($result->phone)
      <div class="mb-2">
        <b>Telefone </b><span class="text-muted">{{ $result->phoneMask }}</span>
      </div>
    @endif
    @if($result->origin)
      <div class="mb-2">
        <b>Origem </b><span class="text-muted">{{ $result->origin->name }}</span>
      </div>
    @endif
    <div class="mb-2">
      <b>Aula Experimental? </b><span class="text-muted">{{ $result->experimentalName }}</span>
    </div>
    <div class="mb-2">
      <b>Tem responsável? </b><span class="text-muted">{{ $result->responsibleName }}</span>
    </div>
    @if($result->placeable)
      <hr>
      <div class="mb-2">
        <b>Rua  </b><span class="text-muted">{{ $result->placeable->street }}, {{ $result->placeable->number }}</span>
      </div>
      <div class="mb-2">
        <b>Cidade </b><span class="text-muted">{{ $result->placeable->city }}, {{ $result->placeable->state }}</span>
      </div>
      <div class="mb-2">
        <b>CEP </b><span class="text-muted">{{ $result->placeable->cep }}</span>
      </div>
      @if($result->placeable->complement)
        <div class="mb-2">
          <b>Complemento </b><span class="text-muted">{{ $result->placeable->complement}}</span>
        </div>
      @endif
    @endif
  </div>
</div>
