@if($result->subscription)
  <div class="card">
    <div class="card-header">
      <h4><i class="fas fa-star lga"></i> Matrícula</h4>
    </div>
    <div class="card-body -scroll-intern">
      <div class="mb-2">
        <b>Código  </b><span clas="text-muted">{{ $result->subscription->reference }}</span>
      </div>
      <div class="mb-2">
        <b>Preço  </b><span clas="text-muted">{{ money(number_format($result->subscription->total, 2), 'BRL') }}</span>
      </div>
      @if($result->subscription->descont)
        <div class="mb-2">
          <b>Desconto  </b><span clas="text-muted">{{ money($result->subscription->descont), 'BRL' }}</span>
        </div>
      @endif
      <div class="mb-2">
        <b>Tipo  </b><span clas="text-muted">{{ $result->subscription->typeName }}</span>
      </div>
      <div class="mb-2">
        <b>Status </b><span clas="text-muted">{{ $result->subscription->statusName }}</span>
      </div>
      @if($result->isVirtual && !is_null($result->linkBolet))
        <div class="mb-2">
          <b>Link do boleto </b><span clas="text-muted"><a href="{{ $result->linkBolet }}" target="_blank" title="Clique aqui">Clique aqui</a></span>
        </div>
      @endif
      @if($result->subscription->isPaid)
        <hr>
        <div class="mb-2 mt-4 text-center">
          <img src="{{ asset('/assets/img/congratis.png') }}">
        </div>
      @endif
    </div>
  </div>
 @else
 {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.students.subscription', 'code' => $result->code], 'files' => true]) !!}
  <div class="card">
    <div class="card-header">
      <h4><i class="fas fa-star lga"></i> Matrícula</h4>
    </div>
    <div class="card-body -scroll-intern">
       <div class="form-group mb-4">
        {!! Form::label('type', 'Forma de Pagamento', ['class' => '']) !!}
        {!! Form::select('type', [1 => 'Presencial', 2 => 'Virtual'], old('type'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
        @if($errors->has('type'))
          <span class="text-danger">{{ $errors->first('type') }}</span>
        @endif
      </div>
      <div class="form-group mb-4">
        {!! Form::label('descont', 'Desconto', ['class' => '']) !!}
        {!! Form::number('descont', old('descont'), ['class' => 'form-control']) !!}
        @if($errors->has('descont'))
          <span class="text-danger">{{ $errors->first('descont') }}</span>
        @endif
      </div>
      <div class="form-group mb-2">
        <button type="submit" class="btn btn-lg btn-success btn-icon mr-1 btn-block" title="Nova Matrícula"><i class="far fa-star"></i> Nova Matrícula</button>
      </div>
    </div>
  </div>

{!! Form::close() !!}
 @endif
