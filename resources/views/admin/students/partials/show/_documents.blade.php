@if($result->aptitude)
  <div class="card">
    <div class="card-header">
      <h4><i class="fas fa-file lga"></i> Aptidão Física</h4>
    </div>
    <div class="card-body -scroll-intern">
      <div class="mb-2 mt-2">
        <b>Documento enviado </b><span class="text-muted"><a href="{{ $result->fisic }}" target="_blank" title="Clique aqui">Clique aqui</a></span>
      </div>
      <div class="mb-4">
        <b>Status </b><span class="text-muted">Validado</span>
      </div>
      <div class="mb-2 mt-4 text-center">
        <img src="{{ asset('/assets/img/congratis.png') }}">
      </div>
    </div>
  </div>
@else

  <div class="card">
    <div class="card-header">
      <h4><i class="fas fa-file lga"></i> Aptidão Física</h4>
    </div>
    <div class="card-body -scroll-intern">
      {!! Form::open(['method' => 'post', 'novalidate', 'route' => ['admin.students.fisic', 'code' => $result->code]]) !!}
      @if($result->fisic)
        <div class="mb-4 mt-2">
          <b>Documento enviado </b><span class="text-muted"><a href="{{ $result->fisic }}" target="_blank" title="Clique aqui">Clique aqui</a></span>
        </div>
      @endif
      <div class="form-group mb-4">
        {!! Form::label('type', 'Estenter Prazo', ['class' => '']) !!}
        {!! Form::select('type', [1 => '10 dias', 2 => '15 dias', 3 => '30 dias', 4 => 'Já foi valiado'], old('type'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
        @if($errors->has('type'))
          <span class="text-danger">{{ $errors->first('type') }}</span>
        @endif
      </div>
      <div class="custom-control custom-checkbox mb-4">
        {{ Form::checkbox("follow", true, isset($result->follow) ? true : false, ['class' => 'custom-control-input', 'id' => "follow"]) }}
        <label class="custom-control-label" for='follow'>Quero acompanhar</label>
      </div>
      <div class="custom-control custom-checkbox mb-4">
        {{ Form::checkbox("release", true, isset($result->release) ? true : false, ['class' => 'custom-control-input', 'id' => "release"]) }}
        <label class="custom-control-label" for='release'>Validado</label>
      </div>
      <div class="form-group mb-2">
        <button type="submit" class="btn btn-lg btn-success btn-icon mr-1 btn-block" title="Atualizar"><i class="far fa-save"></i> Atualizar</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
@endif
