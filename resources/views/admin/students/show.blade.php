@extends('admin.layouts.default')

@section('content')
  <section class="section">
    <div class="section-header mb-0">
      <h1>Visualizar Aluno - {{ $result->first_name }}</h1>
      {!! Breadcrumbs::render('students.show', $result) !!}
    </div>
    <div class="section-options">
      @can('view_students')
        <div class="text-right mb-2">
          <a href="{{ route('admin.students.index') }}" class="btn btn-lg btn-danger btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
          <a href="{{ route('admin.students.edit', ['id' => $result->id]) }}" class="btn btn-lg btn-warning btn-icon" title="Voltar"><i class="fas fa-edit"></i> Editar</a>
        </div>
      @endcan
    </div>
    <div class="clearfix"></div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
          @include('admin.students.partials.show._profile')
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          @include('admin.students.partials.show._subscriptions')
        </div>
        <div class="col-12 col-md-4 col-lg-4">
          @include('admin.students.partials.show._documents')
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
          @include('admin.students.partials.show._modalities')
          @include('admin.students.partials.show._brands')
          @include('admin.students.partials.show._frequencies')
        </div>
      </div>
    </div>
  </section>
@endsection
