@extends('admin.layouts.default')

@section('content')

  {!! Form::model($result, ['method' => 'PUT', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.students.update', 'id' => $result->id, 'files' => true]]) !!}
  <section class="section">
    <div class="section-header mb-0">
      <h1>Editar Aluno</h1>
      {!! Breadcrumbs::render('students.edit', $result) !!}
    </div>

    <div class="section-options">
      <div class="text-right mb-2">
        <a href="{{ route('admin.students.index') }}" class="btn btn-danger btn-lg btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <button type="submit" class="btn btn-icon icon-left btn-success btn-lg"><i class="fas fa-save"></i> Atualizar</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="section-body">
      <div class="row">
        @include('admin.students._form')
      </div>
    </div>
  </section>
  {!! Form::close() !!}

@endsection
