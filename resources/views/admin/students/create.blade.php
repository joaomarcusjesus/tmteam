@extends('admin.layouts.default')

@section('content')

  {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => 'admin.students.store', 'files' => true]) !!}
  <section class="section">
    <div class="section-header mb-0">
      <h1>Novo Aluno</h1>
      {!! Breadcrumbs::render('students.create') !!}
    </div>

    <div class="section-options">
      <div class="text-right mb-2">
        <label class="mt-2 mr-2">
          {!! Form::hidden('experimental', 0) !!}
          {!! Form::checkbox('experimental', true, isset($result) ? $result->experimental : false, ['class' => 'custom-switch-input', 'id' => 'experimental']) !!}
          <span class="custom-switch-indicator"></span>
          <span class="custom-switch-description">Aula Experimental?</span>
        </label>
        <a href="{{ route('admin.students.index') }}" class="btn btn-danger btn-lg btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <button type="submit" class="btn btn-icon icon-left btn-success btn-lg"><i class="fas fa-check"></i> Salvar</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="section-body">
      <div class="row">
       @include('admin.students._form')
      </div>
    </div>
  </section>
  {!! Form::close() !!}

@endsection
