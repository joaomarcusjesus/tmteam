@extends('admin.layouts.default')

@section('content')

  {!! Form::open(['method' => 'POST', 'novalidate', 'role' => 'form', 'class' => 'form', 'route' => ['admin.students_tuitions.store', 'code' => $student->code], 'files' => true]) !!}
  <section class="section">
    <div class="section-header mb-0">
      <h1>Nova Mensalidade</h1>
      {!! Breadcrumbs::render('students_tuitions.create', $student) !!}
    </div>

    <div class="section-options">
      <div class="text-right mb-2">
        <label class="mt-2 mr-2">
          {!! Form::hidden('signature', 0) !!}
          {!! Form::checkbox('signature', true, isset($result) ? $result->signature : false, ['class' => 'custom-switch-input', 'id' => 'signature']) !!}
          <span class="custom-switch-indicator"></span>
          <span class="custom-switch-description">Assinatura?</span>
        </label>
        <label class="mt-2 mr-2">
          {!! Form::hidden('isSchedule', 0) !!}
          {!! Form::checkbox('isSchedule', true, isset($result) ? $result->isSchedule : false, ['class' => 'custom-switch-input', 'id' => 'isSchedule']) !!}
          <span class="custom-switch-indicator"></span>
          <span class="custom-switch-description">Agendamento?</span>
        </label>
        <a href="{{ route('admin.students_tuitions.index', ['code' => $student->code]) }}" class="btn btn-danger btn-lg btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        <button type="submit" class="btn btn-icon icon-left btn-success btn-lg"><i class="fas fa-check"></i> Salvar</button>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="section-body">
      <div class="row">
        @include('admin.students_tuitions._form')
      </div>
    </div>
  </section>
  {!! Form::close() !!}

@endsection
