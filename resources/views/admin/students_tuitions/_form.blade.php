<div class="col-lg-12 col-sm-12">
  <div class="card">
    <div class="card-header -warning">
      <h4>
        <i class="far fa-edit lga"></i>
        Detalhes
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('schedule', 'Data da Cobrança', ['class' => '']) !!}
            {!! Form::text('schedule', old('date_begin'), ['class' => 'form-control js-datepicker']) !!}
            @if($errors->has('schedule'))
              <span class="text-danger">{{ $errors->first('schedule') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('type', 'Forma de Pagamento', ['class' => '']) !!}
            {!! Form::select('type', [1 => 'Presencial', 2 => 'Virtual'], old('type'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('type'))
              <span class="text-danger">{{ $errors->first('type') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('amount', 'Preço', ['class' => '']) !!}
            {!! Form::text('amount', $student->modalities->count() > 0 ? $student->totalModalitiesSum : old('amount'), ['class' => 'form-control js-mask-money']) !!}
            @if($errors->has('amount'))
              <span class="text-danger">{{ $errors->first('amount') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-4">
          <div class="form-group mb-2">
            {!! Form::label('descont', 'Desconto', ['class' => '']) !!}
            {!! Form::text('descont', old('descont'), ['class' => 'form-control js-mask-money']) !!}
            @if($errors->has('descont'))
              <span class="text-danger">{{ $errors->first('descont') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
