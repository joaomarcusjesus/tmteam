@extends('admin.layouts.default')

@section('content')

  <section class="section">
    <div class="section-header mb-3">
      <h1>{{ $student->first_name }} - Mensalidades</h1>
      <div class="section-header-button mr-1">
       <a href="{{ route('admin.students.index') }}" class="btn btn-danger btn-icon btn-lg mr-2" title="Voltar"><i class="fas fa-arrow-left"></i> Voltar</a>
        @can('add_students_submodules')
          <a href="{{ route('admin.students_tuitions.create', ['code' => $student->code]) }}" class="btn btn-success btn-icon btn-lg" title="Adicionar"><i class="fas fa-plus"></i> Adicionar</a>
        @endcan
      </div>
      {!! Breadcrumbs::render('students_tuitions.index', $student) !!}
    </div>
    <div class="section-body">
      <div class="row mt-4">
        <div class="col-lg-12 col-sm-12">
          <h6 class="mb-3">Listagem de mensalidades</h6>
          <div class="card">
            <div class="card-body -table mb-0">
              <div class="table-responsive">
                <table class="table table-striped overflow mb-0">
                  <tbody>
                  <tr class="tr-a">
                    <th>Data da Cobrança</th>
                    <th>Preço</th>
                    <th>Tipo</th>
                    <th>Status</th>
                    <th>Ações</th>
                  </tr>
                  @foreach($results as $result)
                    <tr class="tr-item td-border">
                      <td class="td-item">{{ $result->schedule->format('d/m/Y') }}</td>
                      <td class="td-item">{{ money($result->total, 'BRL') }}</td>
                      <td class="td-item">{{ $result->typeName }}</td>
                      <td class="td-item">{!! $result->statusBadge !!}</td>
                      <td class="td-item">
                        @can('view_students_submodules')
                          <a href="{{ route('admin.students_tuitions.show', ['code' => $student->code, 'id' => $result->id]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar"> <i class="icon lg far fa-eye"></i></a>
                        @endcan

                        @can('delete_students_submodules')
                          <a  href="#" class="js-confirm-delete" data-link="{{ route('admin.students_tuitions.destroy', ['code' => $student->code, 'id' => $result->id]) }}" data-title="Mensalidade na data {{ $result->schedule->format('d/m/Y') }}"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Excluir"> <i class="icon far fa-trash-alt lg"></i></a>
                        @endcan
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 offset-md-3 col-lg-4 offset-lg-4 col-sm-12 mb-2">
          {!! $results->render() !!}
        </div>
      </div>
    </div>
  </section>
@endsection

