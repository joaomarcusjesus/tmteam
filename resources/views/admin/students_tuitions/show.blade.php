@extends('admin.layouts.default')

@section('content')
  <section class="section">
    <div class="section-header mb-0">
      <h1>Visualizar Mensalidade - {{ $result->reference }}</h1>
      {!! Breadcrumbs::render('students_tuitions.show', $student, $result) !!}
    </div>
    <div class="section-options">
      <div class="text-right mb-2">
        <a href="{{ route('admin.students_tuitions.index', ['code' => $student->code]) }}" class="btn btn-lg btn-danger btn-icon mr-1" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
          <div class="card">
            <div class="card-header">
              <h4><i class="fas fa-barcode lga"></i> Mensalidade</h4>
            </div>
            <div class="card-body -scroll-intern">
              <div class="mb-2">
                <b>Data da cobrança  </b><span class="text-muted">{{ $result->schedule->format('d/m/Y') }}</span>
              </div>
              <div class="mb-2">
                <b>Preço  </b><span class="text-muted">{{ money($result->total, 'BRL') }}</span>
              </div>
              @if($result->descont)
              <div class="mb-2">
                <b>Desconto  </b><span class="text-muted">{{ money($result->descont, 'BRL') }}</span>
              </div>
              @endif
              <div class="mb-2">
                <b>Tipo  </b><span class="text-muted">{{ $result->typeName }}</span>
              </div>
              <div class="mb-2">
                <b>Status  </b><span class="text-muted">{{ $result->statusName }}</span>
              </div>
              <div class="mb-2">
                <b>Ativo?  </b><span class="text-muted">{{ $result->activeName }}</span>
              </div>
              <div class="mb-2">
                <b>Assinatura?  </b><span class="text-muted">{{ $result->signatureName }}</span>
              </div>
              <div class="mb-2">
                <b>Agendamento?  </b><span class="text-muted">{{ $result->scheduleName }}</span>
              </div>
            </div>
          </div>
        </div>
        @if($result->orders->count() > 0)
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <div class="card-header">
              <h4><i class="far fa-star lga"></i> Pedidos</h4>
            </div>
            <div class="card-body -table mb-0">
              <div class="table-responsive">
                <table class="table table-striped overflow mb-0">
                  <tbody>
                  <tr class="tr-a">
                    <th>#</th>
                    <th>Preço</th>
                    <th>Tipo</th>
                    <th>Data</th>
                    <th>Ações</th>
                  </tr>
                  @foreach($result->orders as $order)
                    <tr class="tr-item td-border">
                      <td class="td-item">{{ $order->reference }}</td>
                      <td class="td-item">{{ money(number_format($order->amount, 2), 'BRL') }}</td>
                      <td class="td-item">{{ $order->typeName }}</td>
                      <td class="td-item">{{ $order->created_at->format('d/m/Y H:i') }}</td>
                      <td class="td-item">
                        @can('view_students_submodules')
                          <a href="{{ route('admin.orders.show', ['code' => $order->code]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar"> <i class="icon lg far fa-eye"></i></a>
                        @endcan
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
  </section>
@endsection
