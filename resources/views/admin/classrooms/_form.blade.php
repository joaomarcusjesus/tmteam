<div class="col-lg-6 col-sm-12">
  <div class="card">
    <div class="card-header -warning">
      <h4>
        <i class="far fa-edit lga"></i>
        Detalhes
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-6">
          <div class="form-group mb-2">
            {!! Form::label('name', 'Nome', ['class' => 'label-required']) !!}
            {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
            @if($errors->has('name'))
              <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-6">
          <div class="form-group mb-2">
            {!! Form::label('teacher_id', 'Professor', ['class' => 'label-required']) !!}
            {!! Form::select('teacher_id', $teachers, old('teacher_id'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
            @if($errors->has('teacher_id'))
              <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-6">
          <div class="form-group mb-2">
            {!! Form::label('modality_id', 'Modalidade', ['class' => 'label-required']) !!}
            {!! Form::select('modality_id', $modalities, old('modality_id'), ['class' => 'form-control js-select', 'data-route' => route('api.modalities.filter')]) !!}
            @if($errors->has('modality_id'))
              <span class="text-danger">{{ $errors->first('modality_id') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-lg-6 col-sm-12">
  <div class="card">
    <div class="card-header -info">
      <h4>
        <i class="fas fa-user-ninja lga"></i>
        Alunos
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="form-group mb-2">
            {!! Form::label('students', 'Alunos', ['class' => 'label-required']) !!}
            {!! Form::select('students[]', $students ,isset($result->students) ?  $result->students->pluck('id') : old('students'), ['class' => 'form-control js-box js-select-target', 'multiple']) !!}
            @if($errors->has('students'))
              <span class="text-danger">{{ $errors->first('students') }}</span>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')

<script type="text/javascript">

  $('.js-select').change(function() {
    var category_id = $(this).val()
    var route = $(this).data('route') + '/' + category_id
    var _this = $(this)

    $('.js-select-target').empty();

    if (category_id == 'undefined') {
      return false
    }

    $.ajax({
      type: "GET",
      url: route,
      context: _this,
      headers: {
        'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content')
      },
      dataType: 'json',
      success: function(resp){
        if (resp.success) {
          $('.js-select-target').prop('disabled', false);
          $.each(resp.data, function (i, item) {
            $('.js-select-target').append($('<option>', {
              value: item.id,
              text : item.first_name
            }));
          });
         $(".js-box").bootstrapDualListbox('refresh', true);
        }
      }
    })
  })

</script>

@endpush
