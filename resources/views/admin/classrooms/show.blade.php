@extends('admin.layouts.default')

@section('content')
  <section class="section">
    <div class="section-header mb-0">
      <h1>Visualizar Turma</h1>
      {!! Breadcrumbs::render('classrooms.show', $result) !!}
    </div>
    <div class="section-options">
      @can('view_classrooms_submodules')
        <div class="text-right mb-2">
          <a href="{{ route('admin.classrooms.index') }}" class="btn btn-lg btn-danger btn-icon" title="Voltar"><i class="fas fa-angle-left"></i> Voltar</a>
        </div>
      @endcan
    </div>
    <div class="clearfix"></div>
    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
           <div class="card">
            <div class="card-header">
              <h4><i class="far fa-clipboard lga"></i> Turma</h4>
            </div>
            <div class="card-body">
              <div class="mb-2">
                <b>Nome  </b><span clas="text-muted">{{ $result->name }}</span>
              </div>
              <div class="mb-2">
                <b>Professor  </b><span clas="text-muted">{{ $result->teacher->full_name }}</span>
              </div>
              <div class="mb-2">
                <b>Molidade </b><span clas="text-muted">{{ $result->modality->name }}</span>
              </div>
              <b>Status </b><span clas="text-muted">{!! isActive($result->active)!!}</span>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h4><i class="far fa-user lga"></i> Professor</h4>
            </div>
            <div class="card-body">
              <div class="mb-2">
                <b>Nome  </b><span clas="text-muted">{{ $result->teacher->full_name }}</span>
              </div>
              @if($result->teacher->nickname)
              <div class="mb-2">
                <b>Apelido  </b><span clas="text-muted">{{ $result->teacher->nickname }}</span>
              </div>
              @endif
              <div class="mb-2">
                <b>E-mail </b><span clas="text-muted">{{ $result->teacher->email }}</span>
              </div>
              @if($result->teacher->cpf_or_cnpj)
                <div class="mb-2">
                  <b>Identificação </b><span clas="text-muted">{{ $result->teacher->identificationMask }}</span>
                </div>
              @endif
              @if($result->teacher->phone)
                <div class="mb-2">
                  <b>Telefone </b><span clas="text-muted">{{ $result->teacher->phoneMask }}</span>
                </div>
              @endif
            </div>
          </div>
        </div>
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <div class="card-header -info">
              <h4><i class="fas fa-tasks lga"></i> Frequências dos Alunos</h4>
            </div>
            <div class="card-body -table mb-0">
              <div class="table-responsive">
                <table class="table table-hover table-striped overflow mb-0">
                  <tbody>
                    @foreach($result->students as $student)
                      <tr class="tr-a border-bottom hover-click collapsed" data-toggle="collapse" href="#collapseExample-{{ $student->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{ $student->id}}" >
                        <th> <img class="border-100" src="{{ $student->photo }}" alt="{{ $student->full_name }}" width="25"> {{ $student->full_name }}  <a class="collapsed" data-toggle="collapse" href="#collapseExample-{{ $student->id}}" role="button" aria-expanded="false" aria-controls="collapseExample-{{ $student->id}}" data-original-title="Detalhes"> <i class="icon fas fa-angle-down lg"></i></a> </th>
                      </tr>
                      <tr class="tr-a border-bottom collapse" id="collapseExample-{{ $student->id}}" style="">
                        <th class="reset-padding reset-margin">
                          <div class="card mb-0">
                            <div class="card-body -table mb-0">
                              <div class="table-responsive">
                                <table class="table table-striped table-hover overflow mb-0">
                                  <tbody>
                                    <tr class="tr-a">
                                      <th>Horário</th>
                                      <th>Revisado?</th>
                                      <th>Observação</th>
                                      <th>Ações</th>
                                    </tr>
                                    @foreach($student->frequencies as $frequency)
                                      <tr class="tr-item td-border">
                                        <td class="td-item">{{ $frequency->date }}</td>
                                        <td class="td-item">{!! isReleased($frequency->released) !!}</td>
                                        <td class="td-item">{{ $frequency->body }}</td>
                                        <td class="td-item">
                                          @can('view_classrooms_submodules')
                                            <a href="{{ route('admin.classrooms_frequencies.show', ['classroom_id' => $result->id, 'id' => $frequency->id]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visualizar"> <i class="icon far fa-eye lg"></i></a>
                                          @endcan
                                        </td>
                                      </tr>
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </th>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
