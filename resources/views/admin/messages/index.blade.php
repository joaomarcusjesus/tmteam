@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Mensagens</h1>
  </div>

  <div class="section-body">
    <div class="row">
      <div class="col-12 col-sm-6 col-lg-4">
        <div class="card">
          <div class="card-header">
            <h4><i class="far fa-envelope"></i> Contatos</h4>
          </div>
          <div class="card-body -scroll">
            <ul class="list-unstyled list-unstyled-border">
              @foreach($results as $result)
                <li class="media -box-message mb-2 {{ $loop->first ? '' : '-border-top' }} {{ (Request::is('admin/mensagens/' . $result->id)) ? 'active-message' : '' }}">
                  <img alt="image" class="mr-3 rounded-circle" width="50" src="{{ asset('/assets/img/default.png') }}">
                  <div class="">
                    <a href="{{ route('admin.messages.index', ['id' => $result->id]) }}" class="hoverAll" title="{{ $result->name }}"></a>
                    <div class="mt-0 mb-1 font-weight-bold">{{ $result->name }}</div>
                    <div class="mt-0 mb-1 font-weight-bold text-muted ">{{ $result->email }}</div>
                    <div class="text-muted text-small font-600-bold">{{ $result->phoneMask }} - {{ $result->created_at->format('d/m/Y H:i') }}</div>
                    <div class="text-muted text-small font-600-bold">Visualizado</div>
                  </div>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
      @if($message)
      <div class="col-12 col-sm-6 col-lg-8">
        <div class="card">
          <div class="card-body -scroll dashboard-message">
            <div class="container">
              <div class="header text-center">
                <h2 class="mt-3">Visualizar Mensagem {{ $message->protocol }}</h2>
                <small class="text-muted">{{ $message->sendDate }}</small>
              </div>
              <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center content mt-1">
                  <h4 class="mt-3">{{ $message->name }} diz: </h4>
                  <p class="justify">{{ $message->body }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>
</section>

@endsection
