<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('subscriptions', function (Blueprint $table) {
      $table->increments('id');
      $table->string('code', 255)->unique();
      $table->decimal('amount', 10, 2)->nullable();
      $table->decimal('descont', 10, 2)->nullable()->default(0);
      $table->unsignedInteger('type')->nullable();
      $table->unsignedInteger('status')->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->boolean('active')->nullable()->default(0);
      $table->foreign('user_id')->references('id')->on('users');
      $table->unsignedInteger('student_id')->index();
      $table->foreign('student_id')->references('id')->on('students');
      $table->timestamps();
    });

    Schema::create('subscriptions_modalities', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('subscription_id')->index();
      $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
      $table->unsignedInteger('modality_id')->index();
      $table->foreign('modality_id')->references('id')->on('students_modalities')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('subscriptions_modalities');
    Schema::dropIfExists('subscriptions');
  }
}
