<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBandsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bands_categories', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255)->unique();
      $table->string('name', 255)->unique();
      $table->unsignedInteger('age_begin')->default(0);
      $table->unsignedInteger('age_finish')->default(0);
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('bands', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255)->unique();
      $table->string('name', 255)->unique();
      $table->unsignedInteger('degree')->default(0);
      $table->unsignedInteger('age_begin')->default(0);
      $table->unsignedInteger('age_finish')->default(0);
      $table->unsignedInteger('color')->default();
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('bands');
    Schema::dropIfExists('bands_categories');
  }
}
