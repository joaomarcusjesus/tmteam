<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('classrooms', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255);
      $table->string('name', 255)->unique();
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('modality_id')->index();
      $table->foreign('modality_id')->references('id')->on('students_modalities');
      $table->unsignedInteger('teacher_id')->index();
      $table->foreign('teacher_id')->references('id')->on('teachers');
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('classrooms_students', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('classroom_id')->index();
      $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('cascade');
      $table->unsignedInteger('student_id')->index();
      $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
    });

    Schema::create('classrooms_frequencies', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('classroom_id')->index();
      $table->foreign('classroom_id')->references('id')->on('classrooms');
      $table->unsignedInteger('student_id')->index();
      $table->foreign('student_id')->references('id')->on('students');
      $table->unsignedInteger('type')->default(0);
      $table->text('body')->nullable();
      $table->date('date')->nullable();
      $table->boolean('released')->nullable()->default(0);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('classrooms_frequencies');
    Schema::dropIfExists('classrooms_students');
    Schema::dropIfExists('classrooms');
  }
}
