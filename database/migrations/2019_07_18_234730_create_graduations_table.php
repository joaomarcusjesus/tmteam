<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraduationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('graduations_rules', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255);
      $table->string('name', 255)->unique();
      $table->unsignedInteger('limit')->default(0)->nullable();
      $table->unsignedInteger('period')->default(0)->nullable();
      $table->unsignedInteger('month')->default(0)->nullable();
      $table->unsignedInteger('classrooms')->default();
      $table->boolean('active')->nullable()->default(0);
      $table->boolean('no_limit')->nullable()->default(0);
      $table->boolean('is_kids')->nullable()->default(0);
      $table->unsignedInteger('band_id')->index();
      $table->foreign('band_id')->references('id')->on('bands');
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('graduations', function (Blueprint $table) {
      $table->increments('id');
      $table->string('code', 255)->unique();
      $table->boolean('student_active')->nullable()->default(0);
      $table->boolean('is_kids')->nullable()->default(0);
      $table->unsignedInteger('band_id')->index();
      $table->foreign('band_id')->references('id')->on('bands');
      $table->unsignedInteger('student_id')->index();
      $table->foreign('student_id')->references('id')->on('students');
      $table->unsignedInteger('teacher_id')->index();
      $table->foreign('teacher_id')->references('id')->on('teachers');
      $table->unsignedInteger('classroom_id')->index();
      $table->foreign('classroom_id')->references('id')->on('classrooms');
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->unsignedInteger('rule_id')->index();
      $table->foreign('rule_id')->references('id')->on('graduations_rules');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('graduations');
    Schema::dropIfExists('graduations_rules');
  }
}
