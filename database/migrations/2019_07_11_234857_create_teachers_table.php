<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('teachers', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255)->unique();
      $table->string('first_name', 255);
      $table->string('last_name', 255);
      $table->string('nickname', 255)->unique()->nullable();
      $table->string('email', 255)->unique();
      $table->longText('body')->nullable();
      $table->char('cpf_or_cnpj', 40)->unique();
      $table->date('birthday')->nullable();
      $table->char('phone', 40)->nullable();
      $table->boolean('active')->nullable()->default(0);
      $table->boolean('receive_messages')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->rememberToken();
      $table->timestamps();
    });

    Schema::create('teacher_modality', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('teacher_id')->index();
      $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');
      $table->unsignedInteger('modality_id')->index();
      $table->foreign('modality_id')->references('id')->on('students_modalities')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('teacher_modality');
      Schema::dropIfExists('teachers');
  }
}
