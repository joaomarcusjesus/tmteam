<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('bills_types', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255);
      $table->string('name', 255)->unique();
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('bills_items', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255);
      $table->string('name', 255)->unique();
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('bill_type_item', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('type_id')->index();
      $table->foreign('type_id')->references('id')->on('bills_types')->onDelete('cascade');
      $table->unsignedInteger('item_id')->index();
      $table->foreign('item_id')->references('id')->on('bills_items')->onDelete('cascade');
    });

    Schema::create('bills', function (Blueprint $table) {
      $table->increments('id');
      $table->string('code', 255)->unique();
      $table->unsignedInteger('type')->nullable();
      $table->date('date')->nullable();
      $table->decimal('value', 10, 2)->nullable();
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->unsignedInteger('type_id')->index();
      $table->foreign('type_id')->references('id')->on('bills_types');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('bill_type_item');
    Schema::dropIfExists('bills_types');
    Schema::dropIfExists('bills_items');
    Schema::dropIfExists('bills');
  }
}
