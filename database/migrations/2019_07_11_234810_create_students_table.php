<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('students_modalities', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255);
      $table->string('name', 255)->unique();
      $table->decimal('price', 10, 2)->nullable();
      $table->boolean('descont')->nullable()->default(0);
      $table->boolean('active')->nullable()->default(0);
      $table->boolean('jiu_jitsu')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('students_origins', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255)->unique();
      $table->string('name', 255)->unique();
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->timestamps();
    });

    Schema::create('students', function (Blueprint $table) {
      $table->increments('id');
      $table->string('code', 255)->unique();
      $table->string('registration', 255)->unique();
      $table->string('slug', 255)->unique();
      $table->string('nickname', 255)->unique()->nullable();
      $table->string('first_name', 255);
      $table->string('last_name', 255);
      $table->string('cbjj', 255)->nullable();
      $table->string('email', 255)->unique();
      $table->char('cpf_or_cnpj', 40)->unique()->nullable();
      $table->char('phone', 40)->nullable();
      $table->unsignedInteger('age')->nullable();
      $table->unsignedInteger('degree')->nullable();
      $table->unsignedInteger('sexo')->nullable();
      $table->date('birthday')->nullable();
      $table->boolean('active')->nullable()->default(0);
      $table->boolean('experimental')->nullable()->default(0);
      $table->unsignedInteger('origin_id')->index();
      $table->foreign('origin_id')->references('id')->on('students_origins');
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->unsignedInteger('band_id')->index()->nullable();
      $table->foreign('band_id')->references('id')->on('bands');
      $table->timestamps();
    });

    Schema::create('students_responsibles', function (Blueprint $table) {
      $table->increments('id');
      $table->string('slug', 255)->unique();
      $table->string('first_name', 255);
      $table->string('last_name', 255);
      $table->string('email', 255);
      $table->char('cpf_or_cnpj', 40)->nullable();
      $table->char('phone', 40)->nullable();
      $table->unsignedInteger('age')->nullable();
      $table->unsignedInteger('sexo')->nullable();
      $table->date('birthday')->nullable();
      $table->boolean('active')->nullable()->default(0);
      $table->unsignedInteger('user_id')->index();
      $table->foreign('user_id')->references('id')->on('users');
      $table->unsignedInteger('student_id')->index();
      $table->foreign('student_id')->references('id')->on('students');
      $table->timestamps();
    });

    Schema::create('students_aptitudes', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('status')->nullable()->default(1);
      $table->boolean('release')->nullable()->default(0);
      $table->boolean('follow')->nullable()->default(0);
      $table->unsignedInteger('student_id')->index();
      $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
      $table->timestamps();
    });

    Schema::create('student_modality', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('student_id')->index();
      $table->unsignedInteger('status')->nullable()->default(1);
      $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
      $table->unsignedInteger('modality_id')->index();
      $table->foreign('modality_id')->references('id')->on('students_modalities')->onDelete('cascade');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('student_modality');
    Schema::dropIfExists('students');
    Schema::dropIfExists('students_responsibles');
    Schema::dropIfExists('students_origins');
    Schema::dropIfExists('students_documents');
  }
}
