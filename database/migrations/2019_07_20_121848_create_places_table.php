<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('places', function (Blueprint $table) {
      $table->increments('id');
      $table->morphs('placeable');
      $table->char('cep', 40)->nullable();
      $table->string('street', 255)->nullable();
      $table->string('city', 255)->nullable();
      $table->string('neighborhood', 255)->nullable();
      $table->string('state', 255)->nullable();
      $table->string('complement', 255)->nullable();
      $table->char('number', 20)->nullable();
      $table->float('lat', 10, 6)->nullable();
      $table->float('lng', 10, 6)->nullable();
      $table->nullableTimestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('places');
  }
}
