<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Classroom\Classroom;
use App\Models\Teacher\Teacher;
use App\Models\Student\StudentModality as Modality;
use App\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(Classroom::class, function (Faker $faker) {
  return [
    'name' => $faker->unique()->name,
    'active' => $faker->boolean(true),
    'user_id' => User::first()->id,
    'modality_id' => factory(Modality::class)->create()->id,
    'teacher_id' => factory(Teacher::class)->create()->id,
  ];
});
