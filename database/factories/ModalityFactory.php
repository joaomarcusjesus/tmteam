<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Student\StudentModality as Modality;
use App\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(Modality::class, function (Faker $faker) {
  return [
    'name' => $faker->unique()->name,
    'price' => $faker->numberBetween(1000, 9000),
    'descont' => $faker->numberBetween(20, 100),
    'active' => $faker->boolean(true),
    'user_id' => factory(User::class)->create()->id,
  ];
});
