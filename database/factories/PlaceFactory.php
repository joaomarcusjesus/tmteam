<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Place\Place;
use App\Models\Unit\Unit;
use App\Models\Student\Student;
use App\Models\Student\StudentResponsible;
use App\Models\Teacher\Teacher;
use Faker\Generator as Faker;

$polymorphisms = [
  Student::class => 1,
  StudentResponsible::class => 2,
  Teacher::class => 4
];

$factory->define(Place::class, function (Faker $faker) use ($polymorphisms) {
  return [
     'cep' => $faker->postcode,
     'street' => $faker->streetAddress,
     'neighborhood' => $faker->streetName,
     'state' => $faker->stateAbbr,
     'complement' => $faker->secondaryAddress,
     'city' => $faker->city,
     'number' => $faker->buildingNumber,
     'lng' => $faker->longitude,
     'lat' => $faker->latitude,
     'placeable_type' => array_rand($polymorphisms, 1),
     'placeable_id' => factory(array_rand($polymorphisms, 1))->create()->id,
  ];
});
