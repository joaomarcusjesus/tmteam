<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Graduation\Graduation;
use App\Models\Graduation\GraduationRule;
use App\Models\Classroom\Classroom;
use App\Models\Band\Band;
use App\Models\Teacher\Teacher;
use App\Models\Student\Student;
use App\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(Graduation::class, function (Faker $faker) {
  return [
    'student_active' => $faker->boolean(true),
    'is_kids' => $faker->boolean(true),
    'user_id' => factory(User::class)->create()->id,
    'classroom_id' => factory(Classroom::class)->create()->id,
    'rule_id' => factory(GraduationRule::class)->create()->id,
    'band_id' => factory(Band::class)->create()->id,
    'student_id' => factory(Student::class)->create()->id,
    'teacher_id' => factory(Teacher::class)->create()->id,
  ];
});
