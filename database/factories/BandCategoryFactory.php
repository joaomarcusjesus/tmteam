<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Band\BandCategory;
use App\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(BandCategory::class, function (Faker $faker) {
  return [
    'name' => $faker->unique()->name,
    'age_begin' => $faker->numberBetween(1,5),
    'age_finish' => $faker->numberBetween(5,10),
    'active' => $faker->boolean(true),
    'user_id' => User::first()->id,
  ];
});
