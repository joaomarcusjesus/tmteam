<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Bill\Bill;
use App\Models\Bill\BillType;
use App\Models\Subscription\Subscription;
use App\Models\Auth\User;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Bill::class, function (Faker $faker) {
  return [
   'type' => $faker->numberBetween(1, 2),
   'date' => Carbon::now()->format('d/m/Y'),
   'value' => $faker->numberBetween(1000, 9000),
   'active' => $faker->boolean(true),
   'type_id' => factory(BillType::class)->create()->id,
   'user_id' => User::first()->id
  ];
});
