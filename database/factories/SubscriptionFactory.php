<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Student\Student;
use App\Models\Subscription\Subscription;
use App\Models\Auth\User;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Subscription::class, function (Faker $faker) {
  return [
   'status' => $faker->numberBetween(1, 2),
   'active' => $faker->boolean(true),
   'user_id' => User::first()->id,
   'student_id' => factory(Student::class)->create()->id,
  ];
});
