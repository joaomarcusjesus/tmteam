<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Classroom\ClassroomFrequency;
use App\Models\Classroom\Classroom;
use App\Models\Student\Student;
use Faker\Generator as Faker;

$factory->define(ClassroomFrequency::class, function (Faker $faker) {
  return [
    'classroom_id' => factory(Classroom::class)->create()->id,
    'student_id' => factory(Student::class)->create()->id,
    'date' => '15/09/2019 08:00',
    'type' => $faker->numberBetween(1, 2),
    'released' => $faker->boolean(true),
  ];
});
