<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Band\Band;
use App\Models\Graduation\GraduationRule;
use App\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(GraduationRule::class, function (Faker $faker) {
  return [
    'name' => $faker->unique()->name,
    'limit' => $faker->numberBetween(1, 5),
    'period' => $faker->numberBetween(1, 5),
    'month' => $faker->numberBetween(1, 5),
    'classrooms' => $faker->numberBetween(1, 5),
    'no_limit' => $faker->boolean(true),
    'is_kids' => $faker->boolean(true),
    'band_id' => factory(Band::class)->create()->id,
    'user_id' => factory(User::class)->create()->id,
  ];
});
