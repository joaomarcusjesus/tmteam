<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */
use App\Models\Teacher\Teacher;
use App\Models\Unit\Unit;
use Faker\Generator as Faker;
use App\Models\Auth\User;
use Carbon\Carbon;

$factory->define(Teacher::class, function (Faker $faker) {
  return [
    'first_name' => $faker->unique()->name,
    'last_name' => $faker->lastname,
    'nickname' => $faker->unique()->name,
    'email' => $faker->unique()->safeEmail,
    'phone' => $faker->randomNumber(5),
    'cpf_or_cnpj' => $faker->randomNumber(5),
    'active' => $faker->boolean(true),
    'user_id' => User::first()->id,
  ];
});
