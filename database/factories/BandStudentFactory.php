<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Band\Band;
use App\Models\Band\BandStudent;
use App\Models\Student\Student;
use App\Models\Auth\User;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(BandStudent::class, function (Faker $faker) {
  return [
    'body' => $faker->text,
    'feedback' => $faker->text,
    'period' => Carbon::now()->format('d/m/Y'),
    'degree' => $faker->numberBetween(1,5),
    'progress' => $faker->numberBetween(1,5),
    'approved' => $faker->boolean(true),
    'active' => $faker->boolean(true),
    'band_id' => factory(Band::class)->create()->id,
    'student_id' => factory(Student::class)->create()->id
  ];
});
