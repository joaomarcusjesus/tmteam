<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Bill\BillType;
use App\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(BillType::class, function (Faker $faker) {
  return [
    'name' => $faker->unique()->name,
    'active' => $faker->boolean(true),
    'user_id' => User::first()->id,
  ];
});
