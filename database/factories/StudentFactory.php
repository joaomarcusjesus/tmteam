<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Student\StudentResponsible;
use App\Models\Student\Student;
use App\Models\Student\StudentOrigin;
use Faker\Generator as Faker;
use App\Models\Auth\User;
use Carbon\Carbon;

$factory->define(Student::class, function (Faker $faker) {
  return [
    'first_name' => $faker->unique()->name,
    'nickname' => $faker->unique()->name,
    'last_name' => $faker->lastname,
    'email' => $faker->unique()->safeEmail,
    'phone' => $faker->randomNumber(5),
    'cpf_or_cnpj' => $faker->randomNumber(5),
    'birthday' => Carbon::now()->format('d/m/Y'),
    'age' => $faker->randomNumber(2),
    'sexo' => $faker->numberBetween(1,2),
    'experimental' => true,
    'active' => $faker->boolean(true),
    'user_id' => User::first()->id,
    'origin_id' => factory(StudentOrigin::class)->create()->id,
  ];
});
