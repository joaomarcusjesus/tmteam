<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Band\Band;
use App\Models\Band\BandCategory;
use App\Models\Auth\User;
use Faker\Generator as Faker;

$factory->define(Band::class, function (Faker $faker) {
  return [
    'name' => $faker->unique()->name,
    'color' => $faker->numberBetween(1,7),
    'active' => $faker->boolean(true),
    'user_id' => User::first()->id,
    'category_id' => factory(BandCategory::class)->create()->id,
  ];
});
