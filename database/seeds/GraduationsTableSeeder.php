<?php

use Illuminate\Database\Seeder;
use App\Models\Graduation\Graduation;

class GraduationsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(Graduation::class, 1)->create();
  }
}
