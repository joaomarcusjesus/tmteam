<?php

use Illuminate\Database\Seeder;
use App\Models\Band\Band;


class BandsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $branca = Band::create([
      'name' => 'Branca',
      'color' => 1,
      'active' => 1,
      'user_id' => 1,
      'age_begin' => 4,
      'age_finish' => 100,
    ]);

    $cinza = Band::create([
      'name' => 'Cinza',
      'color' => 2,
      'active' => 1,
      'user_id' => 1,
      'age_begin' => 4,
      'age_finish' => 15
    ]);

    $amarela = Band::create([
      'name' => 'Amarela',
      'color' => 4,
      'active' => 1,
      'user_id' => 1,
      'age_begin' => 7,
      'age_finish' => 15
    ]);

    $laranja = Band::create([
      'name' => 'Laranja',
      'color' => 6,
      'active' => 1,
      'user_id' => 1,
      'age_begin' => 10,
      'age_finish' => 15
    ]);

    $green = Band::create([
      'name' => 'Verde',
      'color' => 7,
      'active' => 1,
      'user_id' => 1,
      'age_begin' => 13,
      'age_finish' => 15
    ]);

    $blue = Band::create([
      'name' => 'Azul',
      'color' => 8,
      'active' => 1,
      'user_id' => 1,
      'age_begin' => 16,
      'age_finish' => 100
    ]);

    $purble = Band::create([
      'name' => 'Roxa',
      'color' => 9,
      'active' => 1,
      'user_id' => 1,
      'age_begin' => 16,
      'age_finish' => 100
    ]);

    $brown = Band::create([
      'name' => 'Marrom',
      'color' => 5,
      'active' => 1,
      'user_id' => 1,
      'age_begin' => 18,
      'age_finish' => 100
    ]);

    $black = Band::create([
      'name' => 'Preta',
      'color' => 3,
      'active' => 1,
      'user_id' => 1,
      'age_begin' => 19,
      'age_finish' => 100
    ]);
  }
}
