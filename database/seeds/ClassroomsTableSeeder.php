<?php

use Illuminate\Database\Seeder;
use App\Models\Classroom\ClassroomFrequency;
use App\Models\Classroom\Classroom;
use App\Models\Student\Student;
use Carbon\Carbon;

class ClassroomsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Create Classroom Frequency
    $classroomFrequency = factory(ClassroomFrequency::class, 1)->create();

    // Create Arr
    $arr = [];

    // Each classroom
    foreach ($classroomFrequency->all() as $key => $classroom) {
      $arr[$key]['classroom_id'] = $classroom->classroom_id;
      $arr[$key]['student_id'] = $classroom->student_id;
      $arr[$key]['type'] = $classroom->type;
      $arr[$key]['released'] = $classroom->released;
      $arr[$key]['created_at'] = $classroom->created_at;
      $arr[$key]['updated_at'] = $classroom->updated_at;
    }

    // Create factory classroom
    factory(Classroom::class, 1)->create()->each(function ($classroom) use ($arr) {
      $classroom->frequencies()->attach($arr);
      $classroom->students()->attach(Student::all());
    });
  }
}
