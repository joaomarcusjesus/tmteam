<?php

use Illuminate\Database\Seeder;
use App\Models\Band\BandCategory;

class BandCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $category_1 = BandCategory::create([
        'name' =>  'Pré-Mirim I',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 4,
        'age_finish' => 5
      ]);

      $category_2 = BandCategory::create([
        'name' =>  'Pré-Mirim II',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 5,
        'age_finish' => 6
      ]);

      $category_3 = BandCategory::create([
        'name' =>  'Pré-Mirim III',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 6,
        'age_finish' => 7
      ]);

      $category_4 = BandCategory::create([
        'name' =>  'Mirim I',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 7,
        'age_finish' => 8
      ]);

      $category_5 = BandCategory::create([
        'name' =>  'Mirim II',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 8,
        'age_finish' => 9
      ]);

      $category_6 = BandCategory::create([
        'name' =>  'Mirim III',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 9,
        'age_finish' => 10
      ]);

      $category_7 = BandCategory::create([
        'name' =>  'INFANTIL I',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 10,
        'age_finish' => 11
      ]);

      $category_8 = BandCategory::create([
        'name' =>  'INFANTIL II',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 11,
        'age_finish' => 12
      ]);

      $category_9 = BandCategory::create([
        'name' =>  'INFANTIL III',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 12,
        'age_finish' => 13
      ]);

      $category_10 = BandCategory::create([
        'name' =>  'INFANT0-JUVENIL I',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 13,
        'age_finish' => 14
      ]);

      $category_11 = BandCategory::create([
        'name' =>  'INFANT0-JUVENIL II',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 14,
        'age_finish' => 15
      ]);

      $category_12 = BandCategory::create([
        'name' =>  'INFANT0-JUVENIL III',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 15,
        'age_finish' => 16
      ]);

      $category_13 = BandCategory::create([
        'name' =>  'JUVENIL I',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 16,
        'age_finish' => 17
      ]);

      $category_14 = BandCategory::create([
        'name' =>  'JUVENIL II',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 17,
        'age_finish' => 18
      ]);

      $category_15 = BandCategory::create([
        'name' =>  'ADULTO',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 18,
        'age_finish' => 30
      ]);

      $category_16 = BandCategory::create([
        'name' =>  'MASTER I',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 30,
        'age_finish' => 36
      ]);

      $category_17 = BandCategory::create([
        'name' =>  'MASTER II',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 36,
        'age_finish' => 41
      ]);

      $category_18 = BandCategory::create([
        'name' =>  'MASTER III',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 41,
        'age_finish' => 46
      ]);

      $category_19 = BandCategory::create([
        'name' =>  'MASTER IV',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 46,
        'age_finish' => 51
      ]);

      $category_20 = BandCategory::create([
        'name' =>  'MASTER V',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 51,
        'age_finish' => 56
      ]);

      $category_21 = BandCategory::create([
        'name' =>  'MASTER VI',
        'active' => 1,
        'user_id' => 1,
        'age_begin' => 56,
        'age_finish' => 100
      ]);
    }
}
