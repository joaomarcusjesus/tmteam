<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
  * Seed the application's database.
  *
  * @return void
  */
  public function run()
  {
    $this->call(PermissionsTableSeeder::class);
    $this->call(RolesTableSeeder::class);
    $this->call(UsersTableSeeder::class);
    $this->call(ModalitiesTableSeeder::class);
    $this->call(OriginsTableSeeder::class);
    $this->call(StudentsTableSeeder::class);
    $this->call(TeachersTableSeeder::class);
    $this->call(BandsTableSeeder::class);
    $this->call(BillsTableSeeder::class);
    $this->call(SubscriptionsTableSeeder::class);
    //$this->call(GraduationsTableSeeder::class);
    $this->call(ClassroomsTableSeeder::class);
    $this->call(PlacesTableSeeder::class);
    $this->call(BandCategoryTableSeeder::class);
  }
}
