<?php

use Illuminate\Database\Seeder;
use App\Models\Student\Student;
use App\Models\Student\StudentResponsible;
use App\Models\Student\StudentModality;

class StudentsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(StudentResponsible::class, 1)->create();
  }
}
