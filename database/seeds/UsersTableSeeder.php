<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\User;
use App\Models\Unit\Unit;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Create joao
    $joao = User::create([
      'first_name' => 'Joao',
      'last_name' => 'Marcus',
      'email' => 'joaomarcus@qualitare.com.br',
      'phone' => null,
      'password' => bcrypt('senha'),
      'receive_messages' => true,
      'active' => true
    ]);

    // Create felipe
    $felipe = User::create([
      'first_name' => 'Felipe',
      'last_name' => 'Castro',
      'email' => 'felipecastro@qualitare.com.br',
      'phone' => null,
      'password' => bcrypt('senha'),
      'receive_messages' => true,
      'active' => true
    ]);

    // Create betemuller
    $betemuller = User::create([
      'first_name' => 'Betemuller',
      'last_name' => 'Azevedo',
      'email' => 'betemuller@qualitare.com.br',
      'phone' => null,
      'password' => bcrypt('senha'),
      'receive_messages' => false,
      'active' => true
    ]);

    // Create jayro
    $jayro = User::create([
      'first_name' => 'Jayro',
      'last_name' => 'Pita',
      'email' => 'jayro@qualitare.com.br',
      'phone' => null,
      'password' => bcrypt('senha'),
      'receive_messages' => false,
      'active' => true,
    ]);

    // Create marcolino
    $marcolino = User::create([
      'first_name' => 'Thyago',
      'last_name' => 'Marcolino',
      'email' => 'contato@tmteam.com.br',
      'phone' => null,
      'password' => bcrypt('senha'),
      'receive_messages' => false,
      'active' => true,
    ]);

    // Assing role
    $marcolino->assignRole('root');
    $jayro->assignRole('gestor');
    $betemuller->assignRole('gestor');
    $felipe->assignRole('gestor');
    $joao->assignRole('root');
  }
}
