<?php

use Illuminate\Database\Seeder;
use App\Models\Subscription\Subscription;

class SubscriptionsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(Subscription::class, 1)->create();
  }
}
