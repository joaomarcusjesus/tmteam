<?php

use Illuminate\Database\Seeder;
use App\Models\Teacher\Teacher;
use App\Models\Modality\Modality;

class TeachersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(Teacher::class, 1)->create()->each(function ($teacher) {
      $teacher->modalities()->attach(Modality::all());
    });
  }
}
