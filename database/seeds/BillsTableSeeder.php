<?php

use Illuminate\Database\Seeder;
use App\Models\Bill\Bill;
use App\Models\Bill\BillType;
use App\Models\Bill\BillItem;

class BillsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(BillItem::class, 2)->create();

    factory(BillType::class, 1)->create()->each(function ($type) {
      $type->items()->attach(BillItem::all());
      $type->bills()->saveMany(factory(Bill::class, 1)->create());
    });
  }
}
