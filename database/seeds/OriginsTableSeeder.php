<?php

use Illuminate\Database\Seeder;
use App\Models\Student\StudentOrigin as Origin;
use App\Models\Auth\User;

class OriginsTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    Origin::create(['name' => 'Site', 'user_id' => User::first()->id, 'active' => true]);
    Origin::create(['name' => 'Instagram', 'user_id' => User::first()->id, 'active' => true]);
    Origin::create(['name' => 'Facebook', 'user_id' => User::first()->id, 'active' => true]);
    Origin::create(['name' => 'Whatsapp', 'user_id' => User::first()->id, 'active' => true]);
    Origin::create(['name' => 'Telefone', 'user_id' => User::first()->id, 'active' => true]);
    Origin::create(['name' => 'Presencial', 'user_id' => User::first()->id, 'active' => true]);
    Origin::create(['name' => 'Outros', 'user_id' => User::first()->id, 'active' => true]);
  }
}
