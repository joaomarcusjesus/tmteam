<?php

use Illuminate\Database\Seeder;
use App\Models\Student\StudentModality as Modality;
use App\Models\Auth\User;

class ModalitiesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $jiu_jitsu = Modality::create([
      'name' => 'jiu-jitsu',
      'price' => 200,
      'descont' => 20,
      'active' => 1,
      'user_id' => factory(User::class)->create()->id,
    ]);

    $jiu_jitsu = Modality::create([
      'name' => 'Boxe',
      'price' => 200,
      'descont' => 20,
      'active' => 1,
      'user_id' => factory(User::class)->create()->id,
    ]);

    $jiu_jitsu = Modality::create([
      'name' => 'Karate',
      'price' => 200,
      'descont' => 20,
      'active' => 1,
      'user_id' => factory(User::class)->create()->id,
    ]);

    $jiu_jitsu = Modality::create([
      'name' => 'Judo',
      'price' => 200,
      'descont' => 20,
      'active' => 1,
      'user_id' => factory(User::class)->create()->id,
    ]);
  }


}
