<?php

return [
  'modules' => [
    [
      [
        'header' => 'Centro de Treinamento',
        'permission' => 'view_dashboard',
        'items' =>
        [
          [
            'name' => 'Dashboard',
            'path' => 'dashboard',
            'permission' => 'view_dashboard',
            'icon' => 'fas fa-chart-line lga',
            'route' => 'students.dashboard.index'
          ],
        ],
      ],
    ],
  ],
];
