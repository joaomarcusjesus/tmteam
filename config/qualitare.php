<?php

return [
  'modules' => [
    [
      [
        'header' => 'Estatísticas',
        'permission' => 'view_dashboard',
        'items' =>
        [
          [
            'name' => 'Dashboard',
            'path' => 'dashboard',
            'permission' => 'view_dashboard',
            'icon' => 'fas fa-chart-line lga',
            'route' => 'admin.dashboard.index'
          ],
        ],
      ],
      [
        'header' => 'Centro de Treinamento',
        'permission' => 'view_centers',
        'items' =>
        [
          [
            'name' => 'Alunos',
            'path' => 'students',
            'permission' => 'view_students',
            'icon' => 'fas fa-user-ninja lga',
            'route' => 'admin.students.index'
          ],
          [
            'name' => 'Origens',
            'path' => 'students',
            'icon' => 'fas fa-location-arrow lga',
            'permission' => 'view_students_origins',
            'route' => 'admin.students_origins.index'
          ],
          [
            'name' => 'Graduações',
            'path' => 'graduations',
            'permission' => 'view_graduations',
            'icon' => 'fas fa-graduation-cap lga',
            'route' => 'admin.graduations.index'
          ],
          [
            'name' => 'Faixas',
            'path' => 'bands',
            'permission' => 'view_bands',
            'icon' => 'far fa-star lga',
            'route' => 'admin.bands.index'
          ],
          [
            'name' => 'Turmas',
            'path' => 'users',
            'permission' => 'view_classrooms',
            'icon' => 'far fa-clipboard lga',
            'route' => 'admin.classrooms.index'
          ],
          [
            'name' => 'Professores',
            'path' => 'users',
            'permission' => 'view_teachers',
            'icon' => 'fas fa-chalkboard-teacher lga',
            'route' => 'admin.teachers.index'
          ],
          [
            'name' => 'Aniversários',
            'path' => 'users',
            'permission' => 'view_birthdays',
            'icon' => 'fas fa-birthday-cake lga',
            'route' => 'admin.birthdays.index'
          ]
        ],
      ],
      [
        'header' => 'Finanças',
        'permission' => 'view_controls',
        'items' =>
        [
          [
            'name' => 'Vendas',
            'path' => 'orders',
            'permission' => 'view_orders',
            'icon' => 'fas fa-wallet lga',
            'route' => 'admin.orders.index'
          ],
          [
            'name' => 'Balanço',
            'path' => 'bills',
            'permission' => 'view_bills',
            'icon' => 'fas fa-wallet lga',
            'route' => 'admin.bills.index'
          ],
        ],
      ],
      [
        'header' => 'Competições',
        'permission' => 'view_controls',
        'items' =>
          [
            [
              'name' => 'Ranking',
              'path' => 'orders',
              'permission' => 'view_orders',
              'icon' => 'fas fa-wallet lga',
              'route' => 'admin.orders.index'
            ],
            [
              'name' => 'Campeonatos',
              'path' => 'bills',
              'permission' => 'view_bills',
              'icon' => 'fas fa-wallet lga',
              'route' => 'admin.bills.index'
            ],
          ],
      ],
      [
        'header' => 'Configurações',
        'permission' => 'view_settings',
        'items' =>
        [
          [
            'name' => 'Usuários',
            'path' => 'users',
            'permission' => 'view_users',
            'icon' => 'far fa-user lga',
            'route' => 'admin.users.index'
          ],
          [
            'name' => 'Grupos',
            'path' => 'roles',
            'permission' => 'view_roles',
            'icon' => 'fas fa-users lga',
            'route' => 'admin.roles.index'
          ],
          [
            'name' => 'Permissões',
            'permission' => 'view_permissions',
            'icon' => 'fas fa-key lga',
            'route' => 'admin.permissions.index'
          ],
          [
            'name' => 'Auditoria',
            'path' => 'audits',
            'permission' => 'view_audits',
            'icon' => 'fas fa-redo lga',
            'route' => 'admin.audits.index'
          ],
        ],
      ],
    ],
  ],
  'audit' => [
    'types' => [
        'Band'                                    => 'Faixa',
        'BandCategory'                            => 'Faixa Categoria',
        'Bill'                                    => 'Despesa',
        'BillItem'                                => 'Despesa Item',
        'BillType'                                => 'Despesa Tipo',
        'Classroom'                               => 'Turmas',
        'ClassroomFrequency'                      => 'Turmas Frequência',
        'Document'                                => 'Documento',
        'Graduation'                              => 'Graduação',
        'GraduationRule'                          => 'Graduação Regra',
        'StudentModality'                         => 'Aluno Molidade',
        'StudentOrigin'                           => 'Aluno Origem',
        'StudentResponsible'                      => 'Aluno Responsável',
        'Subscription'                            => 'Matrícula',
        'Teacher'                                 => 'Professor',
        'Student'                                 => 'Estudante'
    ],
    'actions' => [
      'created'       => 'Criação',
      'updated'       => 'Alteração',
      'deleted'       => 'Exclusão',
      'restored'      => 'Reativação'
    ],
  ],
  'email' => [
    'default' => 'suporte@qualitare.com.br'
  ],
  'colors' => [
    'primary'     => '#FFB119',
    'second'      => '#333333',
    'third'       => '#2B2D42',
    'four'        => '#8D99AE',
    'five'        => '#F2F2F6'
  ]
];
