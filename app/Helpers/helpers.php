<?php

/**
 * Set status name
 */
if (! function_exists('isActive')):
  function isActive($status)
  {
    $status_name = '';

    switch($status):
      case 0:
        $status_name = '<span class="badge badge-danger -status">Desativado</span>';
        break;
      case 1:
        $status_name = '<span class="badge badge-success -status">Ativo</span>';
        break;
    endswitch;

    return $status_name;
  }
endif;

/**
 * Set status name
 */
if (! function_exists('isPaid')):
  function isPaid($status)
  {
    $status_name = '';

    switch($status):
      case 1:
        $status_name = '<span class="badge badge-info -info">Aguardando pagamento</span>';
        break;
      case 2:
        $status_name = '<span class="badge badge-success -status">Pagamento Efetuado</span>';
        break;
      case 3:
        $status_name = '<span class="badge badge-success -status">Em atraso</span>';
        break;
    endswitch;

    return $status_name;
  }
endif;

/**
 * Set status name
 */
if (! function_exists('isReleased')):
  function isReleased($status)
  {
    $status_name = '';

    switch($status):
      case 0:
        $status_name = '<span class="badge badge-danger -status">Não</span>';
        break;
      case 1:
        $status_name = '<span class="badge badge-success -status">Sim</span>';
        break;
    endswitch;

    return $status_name;
  }
endif;

/**
 * Set status name
 */
if (! function_exists('isDocumentStatus')):
  function isDocumentStatus($status)
  {
    $status_name = '';

    switch($status):
      case 1:
        $status_name = '<span class="badge badge-info -status">Aguardando</span>';
        break;
      case 2:
        $status_name = '<span class="badge badge-success -status">Aprovado</span>';
        break;
      case 3:
        $status_name = '<span class="badge badge-danger -status">Reprovado</span>';
        break;
    endswitch;

    return $status_name;
  }
endif;

/**
 * Set post status name by color
 */
if (! function_exists('isDocumentStatusColor')):
  function isDocumentStatusColor($status)
  {
    $color = '';

    switch($status):
      case 1:
        $color = '-info text-white';
        break;
    endswitch;

    return $color;
  }
endif;

/**
 * Set post status name by color
 */
if (! function_exists('isStudentStyleIcon')):
  function isStudentStyleIcon($status)
  {
    $icon = '';

    switch($status):
      case 1:
        $icon = 'lg-white';
        break;
      case 2:
        $icon = 'lg';
        break;
      case 3:
        $icon = 'lg';
        break;
    endswitch;

    return $icon;
  }
endif;

/**
 * Set post status name by color
 */
if (! function_exists('isStatus')):
  function isStatus($status)
  {
    $status = '';

    switch($status):
      case 1:
        $status = 'Aguardando Pagamento';
        break;
      case 2:
        $status = 'Pago com successo';
        break;
      case 3:
        $status = 'Não foi efetuado o pagamento';
        break;
    endswitch;

    return $status;
  }
endif;

/**
 * Set trim reference
 */
if (! function_exists('trimReference')):
  function trimReference($reference)
  {
    $fristStep = trim(preg_replace('#[^0-9]#', '', $reference));
    return ltrim($fristStep, '0');
  }
endif;

/**
 * Set budget status name by color
 */
if (! function_exists('isBudgetStatusColor')):
  function isBudgetStatusColor($status)
  {
    $color = '';

    switch($status):
      case 2:
        $color = '-info text-white';
        break;
      // case 4:
      //   $color = '-success text-white';
      //   break;
    endswitch;

    return $color;
  }
endif;

/**
 * Set mask in string
 */
if (! function_exists('mask')):
  function mask($mask, $value)
  {
    for ($i=0; $i < strlen($value); $i++):
      $mask[strpos($mask,'#')] = $value[$i];
    endfor;

    return $mask;
  }
endif;

/**
 * Set mask phone
 */
if (! function_exists('maskPhone')):
  function maskPhone($phone)
  {
    if(strlen($phone) === 10):
      $phone = mask('(##) ####-####', $phone);
    elseif(strlen($phone) === 11):
      $phone = mask('(##) #####-####', $phone);
    endif;

    return $phone;
  }
endif;

/**
 * Format Ucwords PT-BR
 */
if (! function_exists('titleCaseBR')):
  function titleCaseBR($string)
  {
    $delimiters = [" ", "-", ".", "'", "O'", "Mc"];
    $exceptions = ["e", "o", "a", "é", "á", "de", "da", "dos", "das", "do", "I", "II", "III", "IV", "V", "VI"];

    $string = mb_convert_case($string, MB_CASE_TITLE, 'UTF-8');

    foreach ($delimiters as $dlnr => $delimiter):
      $words = explode($delimiter, $string);
      $newwords = [];
      foreach ($words as $wordnr => $word):
          if (in_array(mb_strtoupper($word, 'UTF-8'), $exceptions)):
            // check exceptions list for any words that should be in upper case
            $word = mb_strtoupper($word, 'UTF-8');
          elseif (in_array(mb_strtolower($word, 'UTF-8'), $exceptions)):
            // check exceptions list for any words that should be in upper case
            $word = mb_strtolower($word, 'UTF-8');
          elseif (!in_array($word, $exceptions)):
            // convert to uppercase (non-utf8 only)
            $word = ucfirst($word);
          endif;
          array_push($newwords, $word);
      endforeach;
      $string = join($delimiter, $newwords);
    endforeach;

    return $string;
  }
endif;

/**
 * Set post status name by color
 */
if (! function_exists('isStatus')):
  function isStatus($status)
  {
    $icon = '';

    switch($status):
      case 1:
        $icon = 'Aguardando Pagamento';
        break;
      case 2:
        $icon = 'Pagamento feito com sucesso';
        break;
      case 3:
        $icon = 'Pagamento falhou';
        break;
    endswitch;

    return $icon;
  }
endif;
