<?php

namespace App\Http\Controllers\Student\Frequencies;

use App\Http\Controllers\Student\BaseController;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class FrequencyController extends BaseController
{
  use SEOToolsTrait;

  /**
   * Constructor
   */
  public function __construct()
  {
    // Dependency Injection
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index()
  {
    $student = auth()->user()->student;

    // Set meta tags
    $this->seo()->setTitle('Frequências | Aluno');

    // Return view
    return view('students.frequencies.index', compact('student'));
  }

}
