<?php

namespace App\Http\Controllers\Student\Orders;

use App\Http\Controllers\Student\BaseController;
use App\Models\Student\Student;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class OrderController extends BaseController
{
  use SEOToolsTrait;

  protected $students;

  /**
   * Constructor
   */
  public function __construct(Student $students)
  {
    // Dependency Injection
    $this->students = $students;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index()
  {
    $student = auth()->user()->student;

    // Set meta tags
    $this->seo()->setTitle('Pedidos | Aluno');

    // Return view
    return view('students.orders.index', compact('student'));
  }

  public function show($code)
  {
    $result = $this->students->whereCode($code)->firstOrFail();

    // Set meta tags
    $this->seo()->setTitle('Pedidos | Aluno');

    // Return view
    return view('students.orders.show', compact('result'));
  }

}
