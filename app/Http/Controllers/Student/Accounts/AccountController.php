<?php

namespace App\Http\Controllers\Student\Accounts;

use App\Http\Controllers\Student\BaseController;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class AccountController extends BaseController
{
  use SEOToolsTrait;

  /**
   * Constructor
   */
  public function __construct()
  {
    // Dependency Injection
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index()
  {
    $student = auth()->user()->student;

    // Set meta tags
    $this->seo()->setTitle('Conta | Aluno');

    // Return view
    return view('students.accounts.index', compact('student'));
  }

}
