<?php

namespace App\Http\Controllers\Student\Dashboard;

use App\Http\Controllers\Student\BaseController;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class DashboardController extends BaseController
{
  use SEOToolsTrait;

  /**
   * Constructor
   */
  public function __construct()
  {
    // Dependency Injection
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index()
  {
    $student = auth()->user()->student;

    // Set meta tags
    $this->seo()->setTitle('Dashboard | Aluno');

    // Return view
    return view('students.dashboard.index', compact('student'));
  }

}
