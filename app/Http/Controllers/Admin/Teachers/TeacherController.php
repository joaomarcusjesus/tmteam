<?php

namespace App\Http\Controllers\Admin\Teachers;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Teacher\Teacher;
use App\Models\Auth\Activation;
use App\Models\Auth\User;
use App\Models\Modality\Modality;
use DB;

class TeacherController extends BaseController
{
  use SEOToolsTrait;

  private $teachers;
  private $modalities;
  private $users;

  /**
   * Constructor
   */
  public function __construct(Teacher $teachers, Modality $modalities, User $users)
  {
    // Middlewares
    $this->middleware('permission:view_teachers', ['only' => ['index']]);
    $this->middleware('permission:add_teachers', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_teachers', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_teachers', ['only' => ['destroy']]);

    // Dependency Injection
    $this->teachers = $teachers;
    $this->modalities = $modalities;
    $this->users = $users;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->teachers->orderBy('id', 'DESC');

    // Filter by nome param
    if ($request->filled('nome')):
      $name = $request->get('nome');
      $query->where('first_name', 'LIKE', "%{$name}%");
      $query->orWhere('last_name', 'LIKE', "%{$name}%");
      $query->orWhere('email', '=', $name);
    endif;

    // If filled status
    if ($request->filled('status')) {
      $query->whereActive($request->get('status'));
    }

    // Fetch all results
    $results = $query->paginate(15)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Professores');

    // Return view
    return view('admin.teachers.index', compact('results'));
  }

  /**
   * Show the form for  a new resource.
   * @return Response
   */
  public function show($id)
  {
    // Find result
    $result = $this->teachers->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Professores - Visualizar Professor');

    // Return view
    return view('admin.teachers.show', compact('result'));
  }

  /**
  * Show the form for creating a new resource.
  * @return Response
  */
  public function create()
  {
    // Fetch modalities
    $modalities = $this->modalities->select('id', 'name', 'active')->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Professores - Novo Professor');

    // Return view
    return view('admin.teachers.create', compact('modalities'));
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Arr
    $arr = $request->all();

    // Arr user
    $arr['users']['first_name']     = $arr['teachers']['first_name'];
    $arr['users']['last_name']      = $arr['teachers']['last_name'];
    $arr['users']['email']          = $arr['teachers']['email'];
    $arr['users']['phone']          = $arr['teachers']['phone'];
    $arr['users']['password']       = bcrypt('bemvindotmteam');

    // Arr teachers
    $arr['teachers']['cpf_or_cnpj'] = trim(preg_replace('#[^0-9]#', '', $arr['teachers']['cpf_or_cnpj']));

    // Validate
    $validate = validator($arr, [
      'users.first_name' => 'required|max:255',
      'users.last_name' => 'required|max:255',
      'users.email' => 'required|max:255|email|unique:users,email',
      'users.phone' => 'required|max:40',
      'teachers.first_name' => 'required|max:255',
      'teachers.last_name' => 'required|max:255',
      'teachers.body' => 'nullable',
      'teachers.brithday' => 'nullable|date_format:d/m/Y',
      'teachers.facebook' => 'nullable|max:255',
      'teachers.phone' => 'required|max:255',
      'teachers.instagram' => 'nullable|max:255',
      'modalities' => 'required',
      'teachers.nickname' => 'nullable|max:255|unique:teachers,nickname',
      'teachers.email' => 'required|max:255|email|unique:teachers,email',
      'teachers.cpf_or_cnpj' => 'required|max:50|unique:teachers,cpf_or_cnpj',
      'teachers.birthday' => 'required|date_format:d/m/Y',
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Professor.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Begin transaction
    DB::beginTransaction();

    // Create user
    $user = $this->users->create($arr['users']);

    // Assign role
    $user->assignRole('professor');

    // Merge user_id
    $arr['teachers']['user_id'] = $user->id;

    // Create teacher
    $teacher = $this->teachers->create($arr['teachers']);

    // Attach modalities
    $teacher->modalities()->attach($arr['modalities']);

    // If teacher and user
    if ($teacher && $user) {

      // Create Activation
      $activation = Activation::generateToken($user);

      // Notify Welcome
      $user->sendWelcomeTeacherNotification($activation->token, $user);

      // Commit
      DB::commit();

      // Success message
      toast()->success('Professor criado com sucesso.', 'Sucesso');

      // Redirect to list
      return redirect()->route('admin.teachers.show', ['id' => $teacher->id]);
    }

    // DB rollback
    DB::rollback();

    // Error message
    toast()->error('Falha na comunicação do banco de dados.', 'Error');

    // Redirect back
    return redirect()->back();
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->teachers->findOrFail($id);

    // Fetch modalities
    $modalities = $this->modalities->select('id', 'name', 'active')->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Professor - Editar Professor');

    // Return view
    return view('admin.teachers.edit', compact('result', 'modalities'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Fetch result
    $result = $this->teachers->findOrFail($id);

    // Arr
    $arr = $request->all();

    // Arr user
    $arr['users']['first_name']     = $arr['teachers']['first_name'];
    $arr['users']['last_name']      = $arr['teachers']['last_name'];
    $arr['users']['email']          = $arr['teachers']['email'];
    $arr['users']['phone']          = $arr['teachers']['phone'];

    // Arr teachers
    $arr['teachers']['cpf_or_cnpj'] = trim(preg_replace('#[^0-9]#', '', $arr['teachers']['cpf_or_cnpj']));

    // Validate
    $validate = validator($arr,[
      'users.first_name' => 'required|max:255',
      'users.last_name' => 'required|max:255',
      'users.email' => 'required|max:255|email|unique:users,email,' . $result->user->id,
      'users.phone' => 'required|max:40',
      'modalities' => 'required',
      'teachers.first_name' => 'required|max:255',
      'teachers.last_name' => 'required|max:255',
      'teachers.nickname' => 'nullable|max:255',
      'teachers.instagram' => 'nullable|max:255',
      'teachers.facebook' => 'nullable|max:255',
      'teachers.body' => 'nullable',
      'teachers.brithday' => 'nullable|date_format:d/m/Y',
      'teachers.email' => 'required|max:255|email|unique:teachers,email,' . $result->id,
      'teachers.cpf_or_cnpj' => 'required|max:50|unique:teachers,cpf_or_cnpj,' . $result->id,
      'teachers.phone' => 'required|max:255',
      'teachers.birthday' => 'nullable|date_format:d/m/Y',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Professor.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fill data and save
    $result->fill($arr['teachers'])->save();

    // Fill data and save
    $result->user->update($arr['users']);

    // Sync modalities
    $result->modalities()->sync($request->get('modalities'));

    // Success message
    toast()->success('Professor atualizado com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.teachers.show', ['id' => $result->id]);
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->teachers->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Professor removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Professor não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Professor.')->error();

    // Redirect to back page
    return redirect()->back();
  }

  /**
   * Activate the specified resource from storage.
   * @return Response
   */
  public function activate($id)
  {
    // Fetch result by id
    $result = $this->teachers->findOrFail($id);

    // Change status
    if ($result->active === false):
      $result->active = true;
      $result->save();

      // Success message
      toast()->success('Professor ativado com sucesso', 'Successo');
    else:
      // Warning message
      toast()->warning('Professor já ativa', 'Error');
    endif;

    // Redirect back
    return redirect()->back();
  }

  /**
   * Deactivate the specified resource from storage.
   * @return Response
   */
  public function deactivate($id)
  {
    // Fetch result by id
    $result = $this->teachers->findOrFail($id);

    // Change status
    if ($result->active === true):
      $result->active = false;
      $result->save();

      // Success message
      toast()->success('Professor desativado com sucesso', 'Successo');
    else:
      // Warning message
      toast()->warning('Professor já desativado', 'Error');
    endif;

    // Redirect back
    return redirect()->back();
  }

  public function sendEmail($id)
  {
    // Find result
    $result = $this->teachers->findOrFail($id);

    // Create Activation
    $activation = Activation::generateToken($result->user);

    // Notify Welcome
    $result->user->sendWelcomeTeacherNotification($activation->token, $result->user);

    // Success message
    toast()->success('Reenvio de e-mail enviado com sucesso.', 'Sucesso');

    // Return view
    return redirect()->back();
  }
}
