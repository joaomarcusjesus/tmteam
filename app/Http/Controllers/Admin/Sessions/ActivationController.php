<?php

namespace App\Http\Controllers\Admin\Sessions;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Auth;
use App\Models\Auth\Activation;
use App\Models\Auth\User;

class ActivationController extends BaseController
{
  public function showActivationForm($token)
  {
    $activation = Activation::validateToken($token);

    return view('admin.session.activation', compact('activation'));
  }

  public function store(Request $request)
  {
    // Fetch current user
    $user = User::whereEmail($request->get('email'))->firstOrFail();

    // Validate
    $validate = validator($request->all(), [
      'password' => 'required|min:8|confirmed',
      'password_confirmation' => 'required_with:password'
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar nova senha', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fill data
    $user->fill($request->except('password', 'password_confirmation', 'email'));

    // Check password is present
    if($request->filled('password')):
      $user->password = bcrypt($request->get('password'));
      $user->active = true;
    endif;

    // Save user
    $user->save();

    // Success message
    toast()->success('Senha atualizada com sucesso!', 'Sucesso');

    // Redirect to profile
    return redirect()->route('admin.session.login');
  }
}
