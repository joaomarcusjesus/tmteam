<?php

namespace App\Http\Controllers\Admin\Birthdays;

use App\Models\Student\Student;
use App\Models\Teacher\Teacher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BirthdayController extends Controller
{
  private $students;
  private $teachers;

  /**
   * Constructor
   */
  public function __construct(Student $students, Teacher $teachers)
  {
    // Middlewares
    $this->middleware('permission:view_birthdays', ['only' => ['index']]);

    // Dependency Injection
    $this->teachers = $teachers;
    $this->students = $students;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Query filter
    $students = $this->students->select('first_name', 'last_name', 'email', 'active', 'id', 'birthday')->active()->whereMonth('birthday', '=', date('m'));
    $teachers = $this->teachers->select('first_name', 'last_name', 'email', 'active', 'id', 'birthday')->active()->whereMonth('birthday', '=', date('m'));

    $query = $students->union($teachers);

    // Fetch all results
    $results = $query->paginate(25);

    // Return view
    return view('admin.birthdays.index', compact('results'));
  }
}
