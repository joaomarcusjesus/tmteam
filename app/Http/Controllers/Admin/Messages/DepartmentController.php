<?php

namespace App\Http\Controllers\Admin\Messages;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Auth\User;
use App\Models\Message\Department;
use Carbon\Carbon;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class DepartmentController extends BaseController
{
  use SEOToolsTrait;

  private $departments;

  /**
  * Constructor
  */
  public function __construct(Department $departments, User $users)
  {
    // Middlewares
    $this->middleware('permission:view_departments', ['only' => ['index']]);
    $this->middleware('permission:add_departments', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_departments', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_departments', ['only' => ['destroy']]);

    // Dependency Injection
    $this->departments = $departments;
    $this->users = $users;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Fetch all results
    $results = $this->departments->orderBy('id', 'DESC')->paginate(9);

    // Set meta tags
    $this->seo()->setTitle('Mensagens - Departamentos');

    // Return view
    return view('admin.departments.index', compact('results'));
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function create()
  {
    // Fetch all users by role
    $users = $this->users->byRole(['root', 'gestor'])->get()->pluck('full_name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Mensagens - Departamentos - Novo Departamento');

    // Return view
    return view('admin.departments.create', compact('users'));
  }

  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'title' => 'required|max:255',
      'users' => 'required|min:1',
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Departamento.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Create result
    $result = $this->departments->create($request->except('users'));

    // Attach users
    $result->users()->attach($request->get('users'));

    // Success message
    toast()->success('Departamento criado com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.departments.index');
  }

  public function edit($id)
  {
    // Fetch result by id
    $result = $this->departments->findOrFail($id);

    // Fetch all users by role
    $users = $this->users->byRole(['root', 'gestor'])->get()->pluck('full_name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Mensagens - Departamentos - Editar Departamento');

    // Return view
    return view('admin.departments.edit', compact('result', 'users'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'title' => 'required|max:255'
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Departamento.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->departments->findOrFail($id);

    // Fill data and save
    $result->fill($request->except('users'))->save();

    // Sync users
    $result->users()->sync($request->get('users'));

    // Success message
    toast()->success('Departamento atualizado com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.departments.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    if ($request->ajax()):

      // Fetch result
      $result = $this->departments->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Departamento removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Departamento não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover departamento.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
