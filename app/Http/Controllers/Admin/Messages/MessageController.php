<?php

namespace App\Http\Controllers\Admin\Messages;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Message\Message;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use Carbon\Carbon;

class MessageController extends BaseController
{
  use SEOToolsTrait;

  private $messages;

  /**
   * Constructor
   */
  public function __construct(Message $messages)
  {
    // Middlewares
    $this->middleware('permission:view_messages', ['only' => ['index']]);
    $this->middleware('permission:delete_messages', ['only' => ['destroy']]);

    // Dependency Injection
    $this->messages = $messages;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request, $id = null)
  {
    // Start query
    $query = $this->messages->latest();

    // If has message
    if (is_null($id)) {
      $message = $this->messages->first();
    } else {
      $message = $this->messages->whereId($id)->first();
    }

    // Fetch all results
    $results = $query->paginate(15)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Mensagens');

    // Return view
    return view('admin.messages.index', compact('results', 'message'));
  }

  /**
   * Show the specified resource.
   * @return Response
   */
  public function show($id)
  {
    // Fetch result by id
    $result = $this->messages->findOrFail($id);

    // Current user
    $currentUser = auth()->user();

    // If user not in root role
    if(!$currentUser->hasRole('root')):
      // Set viewed
      $result->viewed = true;

      // Attach current user to view message
      $result->users()->syncWithoutDetaching([$currentUser->id]);

      // Save viewed
      $result->save();
    endif;

    // Set meta tags
    $this->seo()->setTitle("Visualizar Mensagem - Mensagens");

    // Return view
    return view('admin.messages.show', compact('result'));
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    if ($request->ajax()):

      // Fetch result
      $result = $this->messages->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Mensagem removida com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Mensagem não encontrada.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover mensagem.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
