<?php


namespace App\Http\Controllers\Admin\Orders;


use App\Http\Controllers\Admin\BaseController;
use App\Models\Order\Order;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
  use SEOToolsTrait;

  private $orders;

  public function __construct(Order $orders)
  {
    // Middlewares
    $this->middleware('permission:view_orders', ['only' => ['index']]);
    $this->middleware('permission:add_orders', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_orders', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_orders', ['only' => ['destroy']]);

    $this->orders = $orders;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->orders;

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Pedidos');

    // Return view
    return view('admin.orders.index', compact('results'));
  }

  /**
   * Show the form for  a new resource.
   * @return Response
   */
  public function show($code)
  {
    // Find result
    $result = $this->orders->whereCode($code)->firstOrFail();

    // Set meta tags
    $this->seo()->setTitle('Pedidos - Visualizar Pedido');

    // Return view
    return view('admin.orders.show', compact('result'));
  }
}
