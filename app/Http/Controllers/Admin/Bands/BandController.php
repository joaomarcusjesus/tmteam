<?php

namespace App\Http\Controllers\Admin\Bands;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Band\Band;
use App\Models\Band\BandCategory as Category;

class BandController extends BaseController
{
  use SEOToolsTrait;

  private $bands;
  private $categories;

  /**
   * Constructor
   */
  public function __construct(Band $bands, Category $categories)
  {
    // Middlewares
    $this->middleware('permission:view_bands', ['only' => ['index']]);
    $this->middleware('permission:add_bands', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_bands', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_bands', ['only' => ['destroy']]);

    // Dependency Injection
    $this->bands = $bands;
    $this->categories = $categories;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->bands->orderBy('id', 'DESC');

    // Filter by name param
    if ($request->filled('nome')):
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    endif;

    // If filled status
    if ($request->filled('status')) {
      $query->whereActive($request->get('status'));
    }

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Faixas');

    // Return view
    return view('admin.bands.index', compact('results'));
  }

  /**
   * Show the form for  a new resource.
   * @return Response
   */
  public function show($id)
  {
    // Find result
    $result = $this->bands->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Faixas - Visualizar Faixa');

    // Return view
    return view('admin.bands.show', compact('result'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Fetch categories
    $categories = $this->categories->select('id', 'name')->asc()->get()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Faixas - Nova Faixa');

    // Return view
    return view('admin.bands.create', compact('categories'));
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:bands',
      'color' => 'required',
      'body' => 'nullable',
      'age_begin' => 'required|numeric',
      'age_finish' => 'required|numeric',
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Faixa.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->bands->create($request->all());

    // Success message
    toast()->success('Faixa criada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.bands.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->bands->findOrFail($id);

    // Fetch categories
    $categories = $this->categories->select('id', 'name')->asc()->get()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Faixa - Editar Faixa');

    // Return view
    return view('admin.bands.edit', compact('result', 'categories'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:bands,name,' . $id,
      'color' => 'required',
      'body' => 'nullable',
      'age_begin' => 'required|numeric',
      'age_finish' => 'required|numeric',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Faixa.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->bands->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Success message
    toast()->success('Faixa atualizada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.bands.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->bands->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Faixa removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Faixa não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Faixa.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
