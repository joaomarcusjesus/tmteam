<?php

namespace App\Http\Controllers\Admin\Bills;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Bill\Bill;
use App\Models\Bill\BillType;

class BillController extends BaseController
{
  use SEOToolsTrait;

  private $bills;
  private $types;

  /**
   * Constructor
   */
  public function __construct(Bill $bills, BillType $types)
  {
    // Middlewares
    $this->middleware('permission:view_bills', ['only' => ['index']]);
    $this->middleware('permission:add_bills', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_bills', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_bills', ['only' => ['destroy']]);

    // Dependency Injection
    $this->bills = $bills;
    $this->types = $types;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->bills->orderBy('id', 'DESC');

    $types = $this->types->select('id', 'name')->active()->asc()->pluck('name', 'id');

    // Filter by name param
    if ($request->filled('codigo')):
      $name = $request->get('codigo');
      $query->where('id', 'like', "%{$name}%");
    endif;

    // Filter by type param
    if ($request->filled('tipo')) {
      $tipo = $request->get('tipo');
      $query->whereHas('typeRelation', function ($q) use ($tipo) {
        $q->whereId($tipo);
      });
    }

    // If filled status
    if ($request->filled('status')) {
      $query->whereActive($request->get('status'));
    }

    // Fetch all results
    $results = $query->paginate(25)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Despesas');

    // Return view
    return view('admin.bills.index', compact('results', 'types'));
  }

  /**
   * Show the form for  a new resource.
   * @return Response
   */
  public function show($id)
  {
    // Find result
    $result = $this->bills->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Despesas - Visualizar Despesa');

    // Return view
    return view('admin.bills.show', compact('result'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Fetch types
    $types = $this->types->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Despesas - Nova Despesa');

    // Return view
    return view('admin.bills.create', compact('types'));
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'type' => 'required',
      'date' => 'nullable|date_format:d/m/Y',
      'value' => 'required',
      'type_id' => 'required',
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Despesa.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->bills->create($request->all());

    // Success message
    toast()->success('Despesa criada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.bills.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->bills->findOrFail($id);

    // Fetch types
    $types = $this->types->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Despesa - Editar Despesa');

    // Return view
    return view('admin.bills.edit', compact('result', 'types'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'type' => 'required',
      'date' => 'nullable|date_format:d/m/Y',
      'value' => 'required',
      'type_id' => 'required',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Despesa.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->bills->findOrFail($id);

    // Fill data and save
    $result->fill($request->except('students'))->save();

    // Success message
    toast()->success('Despesa atualizada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.bills.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->bills->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Despesa removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Despesa não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Despesa.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
