<?php

namespace App\Http\Controllers\Admin\Bills;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Bill\BillType;

class BillTypeController extends BaseController
{
  use SEOToolsTrait;

  private $types;

  /**
   * Constructor
   */
  public function __construct(BillType $types)
  {
    // Middlewares
    $this->middleware('permission:view_bills_submodules', ['only' => ['index']]);
    $this->middleware('permission:add_bills_submodules', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_bills_submodules', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_bills_submodules', ['only' => ['destroy']]);

    // Dependency Injection
    $this->types = $types;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->types->orderBy('id', 'DESC');

    // Filter by name param
    if ($request->filled('nome')):
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    endif;

    // If filled status
    if ($request->filled('status')) {
      $query->whereActive($request->get('status'));
    }

    // Fetch all results
    $results = $query->paginate(15)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle("Despesas - Tipos");

    // Return view
    return view('admin.bills_types.index', compact('results'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Set meta tags
    $this->seo()->setTitle("Despesas -  Tipos - Nova Tipo");

    // Return view
    return view('admin.bills_types.create');
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|unique:bills_types',
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Tipo.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->types->create($request->all());

    // Success message
    toast()->success('Tipo criada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.bills_types.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->types->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle("Despesas - Tipos - Editar Tipo");

    // Return view
    return view('admin.bills_types.edit', compact('result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:bills_types,name,' . $id,
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Tipo.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->types->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Success message
    toast()->success('Tipo atualizada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.bills_types.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->types->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Tipo removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Tipo não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Tipo.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
