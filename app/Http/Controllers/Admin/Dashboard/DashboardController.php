<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Models\Message\Message;

class DashboardController extends BaseController
{
  use SEOToolsTrait;

  private $messages;

  /**
   * Constructor
   */
  public function __construct(Message $messages)
  {
    // Dependency Injection
    $this->messages = $messages;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index()
  {
    if (auth()->user()->hasRole('aluno')) {
      return redirect()->route('students.dashboard.index');
    }

    // Fetch messages latest
    $messages = $this->messages->select('id', 'name', 'email', 'phone', 'created_at')->latest()->limit(6)->get();

    // Set meta tags
    $this->seo()->setTitle('Dashboard');

    // Return view
    return view('admin.dashboard.index', compact('messages'));
  }
}
