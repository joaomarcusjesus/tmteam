<?php

namespace App\Http\Controllers\Admin\Graduations;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Graduation\Graduation;
use App\Models\Graduation\GraduationRule;

class GraduationController extends BaseController
{
  use SEOToolsTrait;

  private $graduations;
  private $types;

  /**
   * Constructor
   */
  public function __construct(Graduation $graduations, GraduationRule $types)
  {
    // Middlewares
    $this->middleware('permission:view_graduations', ['only' => ['index']]);
    $this->middleware('permission:add_graduations', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_graduations', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_graduations', ['only' => ['destroy']]);

    // Dependency Injection
    $this->graduations = $graduations;
    $this->types = $types;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->graduations->orderBy('id', 'DESC');

    // Fetch all results
    $results = $query->paginate(25)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Graduações');

    // Return view
    return view('admin.graduations.index', compact('results'));
  }

  /**
   * Show the form for  a new resource.
   * @return Response
   */
  public function show($id)
  {
    // Find result
    $result = $this->graduations->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Graduações - Visualizar Graduação');

    // Return view
    return view('admin.graduations.show', compact('result'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Fetch rules
    $rules = $this->rules->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Graduações - Nova Graduação');

    // Return view
    return view('admin.graduations.create', compact('rules'));
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'type' => 'required',
      'date' => 'nullable|date_format:d/m/Y',
      'value' => 'required',
      'type_id' => 'required',
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Graduação.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->graduations->create($request->all());

    // Success message
    toast()->success('Graduação criada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.graduations.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->graduations->findOrFail($id);

    // Fetch types
    $types = $this->types->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Graduação - Editar Graduação');

    // Return view
    return view('admin.graduations.edit', compact('result', 'types'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'type' => 'required',
      'date' => 'nullable|date_format:d/m/Y',
      'value' => 'required',
      'type_id' => 'required',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Graduação.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->graduations->findOrFail($id);

    // Fill data and save
    $result->fill($request->except('students'))->save();

    // Success message
    toast()->success('Graduação atualizada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.graduations.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->graduations->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Graduação removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Graduação não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Graduação.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
