<?php

namespace App\Http\Controllers\Admin\Graduations;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Graduation\GraduationRule;
use App\Models\Band\Band;

class GraduationRuleController extends BaseController
{
  use SEOToolsTrait;

  private $rules;
  private $bands;

  /**
   * Constructor
   */
  public function __construct(GraduationRule $rules, Band $bands)
  {
    // Middlewares
    $this->middleware('permission:view_bills_submodules', ['only' => ['index']]);
    $this->middleware('permission:add_bills_submodules', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_bills_submodules', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_bills_submodules', ['only' => ['destroy']]);

    // Dependency Injection
    $this->rules = $rules;
    $this->bands = $bands;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->rules->orderBy('id', 'DESC');

    // Fetch all results
    $results = $query->paginate(15)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle("Graduações - Regras");

    // Return view
    return view('admin.graduations_rules.index', compact('results'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Fetch bands
    $bands = $this->bands->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle("Graduações -  Regras - Nova Regra");

    // Return view
    return view('admin.graduations_rules.create', compact('bands'));
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|unique:graduations_rules',
      'band_id' => 'required',
      'limit' => 'numeric',
      'period' => 'numeric',
      'month' => 'numeric',
      'classrooms' => 'numeric'
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Regra.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->rules->create($request->all());

    // Success message
    toast()->success('Regra criada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.graduations_rules.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->rules->findOrFail($id);

    // Fetch bands
    $bands = $this->bands->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle("Graduações - Regras - Editar Regra");

    // Return view
    return view('admin.graduations_rules.edit', compact('result', 'bands'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:graduations_rules,name,' . $id,
      'band_id' => 'required',
      'limit' => 'numeric',
      'period' => 'numeric',
      'month' => 'numeric',
      'classrooms' => 'numeric'
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Regra.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->rules->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Success message
    toast()->success('Regra atualizada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.graduations_rules.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->rules->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Regra removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Regra não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Regra.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
