<?php

namespace App\Http\Controllers\Admin\Modalities;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Modality\Modality;

class ModalityController extends BaseController
{
  use SEOToolsTrait;

  private $modalities;

  /**
   * Constructor
   */
  public function __construct(Modality $modalities)
  {
    // Middlewares
    $this->middleware('permission:view_modalities', ['only' => ['index']]);
    $this->middleware('permission:add_modalities', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_modalities', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_modalities', ['only' => ['destroy']]);

    // Dependency Injection
    $this->modalities = $modalities;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->modalities->select('id', 'name', 'price', 'descont', 'active')->orderBy('id', 'DESC');

    // Filter by name param
    if ($request->filled('nome')):
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    endif;

    // If filled status
    if ($request->filled('status')) {
      $query->whereActive($request->get('status'));
    }

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Modalidades');

    // Return view
    return view('admin.modalities.index', compact('results'));
  }

  /**
   * Show the form for  a new resource.
   * @return Response
   */
  public function show($id)
  {
    // Find result
    $result = $this->modalities->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Modalidades - Visualizar Modalidade');

    // Return view
    return view('admin.modalities.show', compact('result'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Set meta tags
    $this->seo()->setTitle('Modalidades - Nova Modalidade');

    // Return view
    return view('admin.modalities.create');
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:modalities,name',
      'price' => 'required',
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Modalidade.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create modality
    $modality = $this->modalities->create($request->all());

    // Success message
    toast()->success('Modalidade criada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.modalities.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->modalities->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Modalidade - Editar Modalidade');

    // Return view
    return view('admin.modalities.edit', compact('result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:modalities,name,' . $id,
      'price' => 'required',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Modalidade.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->modalities->findOrFail($id);

    // Fill data and save
    $result->fill($request->all() )->save();

    // Success message
    toast()->success('Modalidade atualizada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.modalities.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->modalities->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Modalidade removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Modalidade não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Modalidade.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
