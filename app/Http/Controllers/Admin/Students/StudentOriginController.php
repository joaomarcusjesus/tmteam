<?php

namespace App\Http\Controllers\Admin\Students;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Student\StudentOrigin as Origin;

class StudentOriginController extends BaseController
{
  use SEOToolsTrait;

  private $origins;

  /**
   * Constructor
   */
  public function __construct(Origin $origins)
  {
    // Middlewares
    $this->middleware('permission:view_students_origins', ['only' => ['index']]);
    $this->middleware('permission:add_students_origins', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_students_origins', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_students_origins', ['only' => ['destroy']]);

    // Dependency Injection
    $this->origins = $origins;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->origins->select('id', 'name', 'active')->orderBy('id', 'DESC');

    // Filter by name param
    if ($request->filled('nome')):
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    endif;

    // If filled status
    if ($request->filled('status')) {
      $query->whereActive($request->get('status'));
    }

    // Fetch all results
    $results = $query->paginate(15)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Origens');

    // Return view
    return view('admin.students_origins.index', compact('results'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Set meta tags
    $this->seo()->setTitle('Origens - Nova Origem');

    // Return view
    return view('admin.students_origins.create');
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:students_origins,name'
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Origem.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->origins->create($request->all());

    // Success message
    toast()->success('Origem criada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.students_origins.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->origins->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Origens - Editar Origem');

    // Return view
    return view('admin.students_origins.edit', compact('result'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:students_origins,name,' . $id
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Origem.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result by id
    $result = $this->origins->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Success message
    toast()->success('Origem atualizada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.students_origins.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->origins->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Origem removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Origem não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Origem.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
