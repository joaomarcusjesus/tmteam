<?php

namespace App\Http\Controllers\Admin\Students;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Band\Band;
use App\Models\Band\BandCategory;
use App\Models\Student\StudentAptitude;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Student\Student;
use App\Models\Student\StudentOrigin;
use App\Models\Auth\Activation;
use App\Models\Auth\User;
use App\Models\Order\Order;
use App\Models\Subscription\Subscription;
use App\Services\Orders\OrderService;
use App\Models\Student\StudentModality as Modality;
use App\Models\Student\StudentResponsible as Responsible;
use DB;

class StudentController extends BaseController
{
  use SEOToolsTrait;

  private $students;
  private $origins;
  private $modalities;
  private $responsibles;
  private $subscriptions;
  private $users;
  private $bands;
  private $categories;
  private $orders;
  private $services;
  private $aptitudes;

  /**
   * Constructor
   */
  public function __construct(User $users,
                             Student $students,
                             StudentOrigin $origins,
                             Modality $modalities,
                             Responsible $responsibles,
                             Band $bands,
                             Order $orders,
                             OrderService $services,
                             Subscription $subscriptions,
                             BandCategory $categories, StudentAptitude $aptitudes)
  {
    // Middlewares
    $this->middleware('permission:view_students', ['only' => ['index']]);
    $this->middleware('permission:add_students', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_students', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_students', ['only' => ['destroy']]);

    // Dependency Injection
    $this->origins = $origins;
    $this->students = $students;
    $this->modalities = $modalities;
    $this->responsibles = $responsibles;
    $this->users = $users;
    $this->bands = $bands;
    $this->categories = $categories;
    $this->services = $services;
    $this->subscriptions = $subscriptions;
    $this->orders = $orders;
    $this->aptitudes = $aptitudes;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->students->orderBy('id', 'DESC');

    // Filter by nome param
    if ($request->filled('nome')):
      $name = $request->get('nome');
      $query->where('first_name', 'LIKE', "%{$name}%");
      $query->orWhere('last_name', 'LIKE', "%{$name}%");
      $query->orWhere('email', '=', $name);
    endif;


    // If filled status
    if ($request->filled('status')) {
      $query->whereActive($request->get('status'));
    }

    // Fetch all results
    $results = $query->paginate(15)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Alunos');

    // Return view
    return view('admin.students.index', compact('results'));
  }

  /**
   * Show the form for  a new resource.
   * @return Response
   */
  public function show($code)
  {
    // Find result
    $result = $this->students->whereCode($code)->firstOrFail();

    // Fetch bands
    $bands = $this->bands->where('age_begin', '<=', $result->age)->where('age_finish', '>=', $result->age)->pluck('name', 'id');

    // Fetch categories
    $bandCategories = $this->categories->where('age_begin', '<=', $result->age)->where('age_finish', '>', $result->age)->pluck('name', 'id');

    $jiujitsu = false;

    foreach($result->modalities as $modality):
      if ($modality->slug == 'jiu-jitsu'):
        $jiujitsu = true;
      endif;
    endforeach;

    // Set meta tags
    $this->seo()->setTitle('Alunos - Visualizar Aluno');

    // Return view
    return view('admin.students.show', compact('result', 'bands', 'bandCategories', 'jiujitsu'));
  }

  /**
  * Show the form for creating a new resource.
  * @return Response
  */
  public function create()
  {
    // Fetch modalities
    $modalities = $this->modalities->select('id', 'name', 'active')->asc()->active()->pluck('name', 'id');

    // Fetch origins
    $origins = $this->origins->select('id', 'name', 'active')->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Alunos - Novo Aluno');

    // Return view
    return view('admin.students.create', compact('modalities', 'origins'));
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    $data = $request->all();

    $email = $this->users->whereEmail($request->email)->first();
    $cpf = $this->users->where('cpf_or_cnpj', '==', $request->cpf_or_cnpj)->first();

    if($email):

      toast()->error('Já existe um usuário com esse e-mail.', 'Error');

      return redirect()->back();

    endif;

    if($cpf):

      toast()->error('Já existe um usuário com esse cpf.', 'Error');

      return redirect()->back();

    endif;

    $validate = validator($data, [
      'students.first_name' => 'required|max:255',
      'students.last_name' => 'required|max:255',
      'students.nickname' => 'nullable|max:255|unique:students,nickname',
      'students.cbjj' => 'nullable|max:255|unique:students,cbjj',
      'students.email' => 'required|max:255|email|unique:students,email',
      'students.cpf_or_cnpj' => 'nullable|max:255|unique:students,cpf_or_cnpj',
      'students.phone' => 'nullable|max:255',
      'students.age' => 'required',
      'students.origin_id' => 'required',
      'students.sexo' => 'required',
      'students.birthday' => 'required|date_format:d/m/Y',
      'address.street' => 'required|max:255',
      'address.cep' => 'required|max:255',
      'address.neighborhood' => 'required|max:255',
      'address.number' => 'required|max:10',
      'address.city' => 'required|max:255',
      'address.state' => 'required|max:255',
      'responsible.first_name' => 'required_if:age,<,18|max:255',
      'responsible.last_name' => 'required_if:age,<,18|max:255',
      'responsible.email' => 'required_if:age,<,18|max:255|unique:students_responsibles,email' ,
      'responsible.cpf_or_cnpj' => 'required_if:age,<,18|max:50|unique:students_responsibles,cpf_or_cnpj',
      'responsible.phone' => 'required_if:age,<,18|max:40',
      'responsible.birthday' => 'required_if:age,<,18|nullable|date_format:d/m/Y',
      'responsible.age' => 'required_if:age,<,18',
      'responsible.sexo' => 'required_if:age,<,18',
    ]);

    if($validate->fails()):

      toast()->error('Falha ao adicionar Aluno.', 'Error');

      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());

    endif;

    DB::beginTransaction();

    $user = User::create([
      'first_name' => $data['students']['first_name'],
      'last_name' => $data['students']['last_name'],
      'email' => $data['students']['email'],
      'password' => bcrypt('bemvindotmteam'),
      'active' => true,
    ]);

    $user->assignRole('aluno');

    $data['students']['user_id'] = $user->id;
    $data['students']['actve'] = true;
    $data['students']['experimental'] = $request->get('experimental');

    $student = $this->students->create($data['students']);

    if (!is_null($request->get('responsible')['cpf_or_cnpj'])) {
      $data['responsible']['student_id'] = $student->id;
      $this->responsibles->create($request->get('responsible'));
    }

    $student->placeable()->create($request->get('address'));

    $student->modalities()->attach($request->get('modalities'));

    $cover = $request->file('cover');

    if ($cover):
      $filename = md5($cover->getClientOriginalName()) . '.' . $cover->getClientOriginalExtension();
      $student->addMedia($cover)
          ->usingName($student->full_name)
          ->usingFileName($filename)
          ->toMediaCollection('photo');
    endif;

    $fisic = $request->file('fisic');

    if ($fisic):
      $filename = md5($fisic->getClientOriginalName()) . '.' . $fisic->getClientOriginalExtension();
      $student->addMedia($fisic)
          ->usingName($student->full_name)
          ->usingFileName($filename)
          ->toMediaCollection('fisic');
    endif;

    if ($student && $user) {

      $activation = Activation::generateToken($user);

      DB::commit();

      $student->sendWelcomeNotification($user, $activation->token);

      toast()->success('Aluno adicionado com sucesso.', 'Sucesso');

      return redirect()->route('admin.students.show', ['code' => $student->code]);
    }

    DB::rollback();

    toast()->error('Falha ao adicionar Aluno.', 'Error');

    return redirect()->back()->withInput()->withErrors($validate->getMessageBag());

  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->students->findOrFail($id);

    // Fetch modalities
    $modalities = $this->modalities->select('id', 'name', 'active')->asc()->active()->pluck('name', 'id');

    // Fetch origins
    $origins = $this->origins->select('id', 'name', 'active')->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Aluno - Editar Aluno');

    // Return view
    return view('admin.students.edit', compact('result', 'modalities', 'origins'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Fetch result
    $result = $this->students->findOrFail($id);

    // Arr
    $arr = $request->all();

    // Arr user
    $arr['users']['first_name']     = $arr['students']['first_name'];
    $arr['users']['last_name']      = $arr['students']['last_name'];
    $arr['users']['email']          = $arr['students']['email'];
    $arr['users']['phone']          = $arr['students']['phone'];

    // Arr Trim
    $arr['students']['cpf_or_cnpj'] = trim(preg_replace('#[^0-9]#', '', $arr['students']['cpf_or_cnpj']));
    $arr['responsible']['cpf_or_cnpj'] = trim(preg_replace('#[^0-9]#', '', $arr['responsible']['cpf_or_cnpj']));

    $email = $this->users->whereEmail($request->email)->first();
    $cpf = $this->users->where('cpf_or_cnpj', '==', $request->cpf_or_cnpj)->first();

    if($email):

      toast()->error('Já existe um usuário com esse e-mail.', 'Error');

      return redirect()->back();

    endif;

    if($cpf):

      toast()->error('Já existe um usuário com esse cpf.', 'Error');

      return redirect()->back();

    endif;

    // Validate
    $validate = validator($arr,[
      'users.first_name' => 'required|max:255',
      'users.last_name' => 'required|max:255',
      'users.email' => 'required|max:255|email|unique:users,email,' . $result->user->id,
      'users.phone' => 'required|max:40',
      'modalities' => 'required',
      'students.first_name' => 'required|max:255',
      'students.last_name' => 'required|max:255',
      'students.email' => 'required|max:255|email|unique:students,email,' . $id,
      'students.cpf_or_cnpj' => 'required|max:50|unique:students,cpf_or_cnpj,' . $id,
      'students.phone' => 'required|max:255',
      'students.birthday' => 'nullable|date_format:d/m/Y',
      'students.age' => 'required',
      'students.sexo' => 'required',
      'students.origin_id' => 'required',
      'responsible.first_name' => 'required_if:age,<,18|max:255',
      'responsible.last_name' => 'required_if:age,<,18|max:255',
      'responsible.email' => 'required_if:age,<,18|max:255|unique:students_responsibles,email,$result->responsible->id',
      'responsible.cpf_or_cnpj' => 'required_if:age,<,18|max:50|unique:students_responsibles,cpf_or_cnpj,$result->responsible->id',
      'responsible.phone' => 'required_if:age,<,18|max:40',
      'responsible.birthday' => 'required_if:age,<,18|nullable|date_format:d/m/Y',
      'responsible.age' => 'required_if:age,<,18|',
      'responsible.sexo' => 'required_if:age,<,18|',
      'address.street' => 'required|max:255',
      'address.cep' => 'required|max:255',
      'address.neighborhood' => 'required|max:255',
      'address.number' => 'required|max:10',
      'address.city' => 'required|max:255',
      'address.state' => 'required|max:255',
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
     toast()->error('Falha ao atualizar Aluno.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with hasResponsible
    if ($request->get('ask') == 2) {
      $arr['students']['hasResponsible'] = 1;
    } else {
      $arr['students']['hasResponsible'] = 0;
    }

    // Update student
    $result->update($arr['students']);

    // Sync modalities
    $result->modalities()->sync($request->get('modalities'));

    // If has responsible
    if (!is_null($request->get('responsible')['cpf_or_cnpj'])) {

      // Find responsible
      $responsible = $this->responsibles->findOrFail($result->responsible->id);

      // Update responsible
      $responsible->update($arr['responsible']);
    }

    // Sync student placeable
    $result->placeable()->update($request->get('address'));

    // Success message
    toast()->success('Aluno atualizar com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.students.show', ['code' => $result->code]);
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->students->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Aluno removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Aluno não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Aluno.')->error();

    // Redirect to back page
    return redirect()->back();
  }

   /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function subscription(Request $request, $code)
  {
    $validate = validator($request->all(), [
      'type' => 'required',
      'descont' => 'nullable',
    ]);

    if ($validate->fails()):

      toast()->error('Falha ao adicionar Matrícula.', 'Error');

      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    $result = $this->students->whereCode($code)->firstOrFail();

    $amount = $result->modalities->sum('price');

    if ($request->filled('descont')) {
      $amount = $result->modalities->sum('price') - $request->get('amount');
    }

    $subscription = $this->subscriptions->create([
      'amount' => $amount,
      'descont' => $request->get('descont'),
      'type' => $request->get('type'),
      'student_id' => $result->id,
      'user_id' => Auth::user()->id,
    ]);

    if ($request->get('type') == 2) {

      $order = $this->orders->create([
        'description' => "Pagamento da matrícula {$subscription->reference}",
        'amount' => $amount,
        'type' => 2,
        'cpf_or_cnpj' => $result->cpf_or_cnpj,
        'first_name' => $result->first_name,
        'last_name' => $result->last_name,
        'student_id' => $result->id
      ]);

      if (is_null($order)) {
        DB::rollback();

        toast()->warning('Falha na transação da Matrícula.', 'Atenção!');

        return redirect()->back();
      }

      $service = $this->services->sendOrder($order);

      if (is_null($service)) {

        DB::rollback();

        // Force delete
        $result->order()->delete();
        $result->subscription()->delete();

        toast()->warning('Falha na transação da Matrícula.', 'Atenção!');

        return redirect()->back();
      }
    }

    // If successuly
    if ($subscription) {

      // Commit
      DB::commit();

      if (isset($order)) {
        $order->code_gateway = $service['code_gateway'];
        $order->boleto_url = $service['boleto_url'];
        $order->checkout_url = $service['checkout_url'];
        $order->installmentLink = $service['installmentLink'];
        $order->payNumber = $service['payNumber'];
        $order->save();
      }

      // Success message
      toast()->success('Matrícula criada com sucesso.', 'Sucesso');

      // Redirect to list
      return redirect()->route('admin.students.show', ['code' => $result->code]);
    }

    // Rollback
    DB::rollback();

    // Warning message
    toast()->warning('Falha na transação da Matrícula.', 'Atenção!');

    // Redirect to back
    return redirect()->back();
  }

  public function band(Request $request, $code)
  {
    $validate = validator($request->all(), [
      'brandCategory' => 'required',
      'brand' => 'required',
      'degree' => 'required'
    ]);

    if ($validate->fails()):

      toast()->error('Falha ao adicionar Faixa.', 'Error');

      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    $result = $this->students->whereCode($code)->firstOrFail();

    $band = $this->bands->findOrFail($request->get('brand'));

    $result->band_id = $band->id;
    $result->degree = $request->get('degree');
    $result->save();

    // Success message
    toast()->success('Faixa ataualiza com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.students.show', ['code' => $result->code]);
  }

  public function fisic(Request $request, $code)
  {
    $result = $this->students->whereCode($code)->firstOrFail();

    $validate = validator($request->all(), [
      'type' => 'required',
    ]);

    if ($validate->fails()):

      toast()->error('Falha ao adicionar Faixa.', 'Error');

      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    $request->merge(['student_id' => $result->id]);

    $result->aptitude()->update($request->except('_token', 'type'));

    toast()->success('Documento atualizado com sucesso ao.', 'Sucesso');

    return redirect()->route('admin.students.show', ['code' => $result->code]);
  }
}
