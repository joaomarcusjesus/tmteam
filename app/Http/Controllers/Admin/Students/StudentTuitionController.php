<?php

namespace App\Http\Controllers\Admin\Students;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Order\Order;
use App\Models\Student\Student;
use App\Models\Tuition\Tuition;
use App\Services\Orders\OrderService;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class StudentTuitionController extends BaseController
{
  use SEOToolsTrait;

  protected $students;
  protected $tuitions;
  protected $services;
  protected $orders;

  public function __construct(Student $students, Tuition $tuitions, OrderService $services, Order $orders)
  {
    $this->middleware('permission:view_students_submodules', ['only' => ['index']]);
    $this->middleware('permission:add_students_submodules', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_students_submodules', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_students_submodules', ['only' => ['destroy']]);

    $this->students = $students;
    $this->tuitions = $tuitions;
    $this->services = $services;
    $this->orders = $orders;
  }

  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index(Request $request, $code)
  {
    // Student Code
    $student = $this->students->whereCode($code)->firstOrFail();

    // Query
    $query = $student->tuitions()->orderBy('schedule', 'ASC');

    // Filter by name param
    if ($request->filled('nome')):
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    endif;

    // If filled status
    if ($request->filled('status')) {
      $query->whereActive($request->get('status'));
    }

    // Fetch all results
    $results = $query->paginate(15)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Alunos - Mensalidades');

    // Return view
    return view('admin.students_tuitions.index', compact('results', 'student'));
  }

  public function show($code, $id)
  {
    // Student Code
    $student = $this->students->whereCode($code)->firstOrFail();

    // Find result
    $result = $this->tuitions->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Alunos - Mensalidades - Visualizar Mensalidade');

    // Return view
    return view('admin.students_tuitions.show', compact('student', 'result'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create($code)
  {
    // Student Code
    $student = $this->students->whereCode($code)->firstOrFail();

    // Set meta tags
    $this->seo()->setTitle('Alunos - Mensalidades - Nova Mensalidade');

    // Return view
    return view('admin.students_tuitions.create', compact('student'));
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function store(Request $request, $code)
  {
    // Student Code
    $student = $this->students->whereCode($code)->firstOrFail();

    // Validate
    $validate = validator($request->all(),[
      'schedule' => 'required|date_format:d/m/Y',
      'type' => 'required',
      'descont' => 'nullable',
      'amount' => 'required'
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Mensalidade.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    DB::beginTransaction();

    // Merge with user_id
    $request->merge(['student_id' => $student->id]);

    // Create result
    $result = $this->tuitions->create($request->all());

    if ($request->get('type') == 2 && $result->isToday) {
      $amount = $result->amount;
      if ($request->filled('descont')) {
        $amount = $result->amount - $result->descont;
      }

      $order = $this->orders->create([
        'description' => "Pagamento da mensalidade {$result->reference}",
        'amount' => (int) $amount,
        'type' => 1,
        'cpf_or_cnpj' => $result->student->cpf_or_cnpj,
        'first_name' => $result->student->first_name,
        'last_name' => $result->student->last_name,
        'student_id' => $result->student->id
      ]);

      if (is_null($order)) {
        DB::rollback();

        toast()->warning('Falha na transação da Mensalidade.', 'Atenção!');

        return redirect()->back();
      }

      $service = $this->services->sendOrder($order);

      if (is_null($service)) {

        DB::rollback();

        toast()->warning('Falha na transação da Mensalidade.', 'Atenção!');

        return redirect()->back();
      }
    }

    if ($result) {

      DB::commit();

      if (isset($order)) {
        $order->code_gateway = $service['code_gateway'];
        $order->boleto_url = $service['boleto_url'];
        $order->checkout_url = $service['checkout_url'];
        $order->installmentLink = $service['installmentLink'];
        $order->payNumber = $service['payNumber'];
        $order->save();
        $result->orders()->attach($order);
      }

      // Success message
      toast()->success('Mensalidade criada com sucesso.', 'Sucesso');

      // Redirect to list
      return redirect()->route('admin.students_tuitions.index', ['code' => $student->code]);
    }

    DB::rollback();

    toast()->error('Falha na transação da Mensalidade.', 'Error');

    return redirect()->back()->withInput();
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $code, $id)
  {
    // Student Code
    $student = $this->students->whereCode($code)->firstOrFail();

    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->tuitions->find($id);

      // If result exist
      if($student && $result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Mensalidade removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Mensalidade não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Origem.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
