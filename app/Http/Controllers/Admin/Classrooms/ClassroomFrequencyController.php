<?php

namespace App\Http\Controllers\Admin\Classrooms;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Classroom\Classroom;
use App\Models\Classroom\ClassroomFrequency as Frequency;
use App\Models\Student\Student;
use App\Models\Teacher\Teacher;
use App\Models\Modality\Modality;

class ClassroomFrequencyController extends BaseController
{
  use SEOToolsTrait;

  private $classrooms;
  private $frequencies;
  private $students;
  private $modalities;
  private $teachers;

  /**
   * Constructor
   */
  public function __construct(Classroom $classrooms, Frequency $frequencies, Student $students, Modality $modalities, Teacher $teachers)
  {
    // Middlewares
    $this->middleware('permission:view_classrooms_submodules', ['only' => ['index']]);
    $this->middleware('permission:add_classrooms_submodules', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_classrooms_submodules', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_classrooms_submodules', ['only' => ['destroy']]);

    // Dependency Injection
    $this->classrooms = $classrooms;
    $this->frequencies = $frequencies;
    $this->students = $students;
    $this->modalities = $modalities;
    $this->teachers = $teachers;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request, $classroom_id)
  {
    // Find classroom by classroom_id
    $classroom = $this->classrooms->findOrFail($classroom_id);

    // Query filter
    $query = $this->frequencies->select('id', 'classroom_id', 'student_id', 'type', 'released', 'date')->where('classroom_id', $classroom->id)->orderBy('id', 'DESC');

    // Filter by aluno param
    if ($request->filled('aluno')):
      $studentId = $request->get('aluno');
      $query->where('student_id', $studentId);
    endif;

    // If filled status
    if ($request->filled('status')) {
      $query->whereReleased($request->get('status'));
    }

    // Fetch students
    $students = $classroom->students()->asc()->get()->pluck('full_name', 'id');

    // Fetch all results
    $results = $query->paginate(35)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle("Turmas - {$classroom->name} - Frequências");

    // Return view
    return view('admin.classrooms_frequencies.index', compact('results', 'classroom', 'students'));
  }

  /**
   * Show the form for  a new resource.
   * @return Response
   */
  public function show($classroom_id, $id)
  {
    // Find classroom by classroom_id
    $classroom = $this->classrooms->findOrFail($classroom_id);

    // Find result
    $result = $this->frequencies->findOrFail($id);

    // Find student
    $student = $this->students->findOrFail($result->student_id);

    // Set meta tags
    $this->seo()->setTitle("Turmas - {$classroom->name} - Frequências - Visualizar Frequência");

    // Return view
    return view('admin.classrooms_frequencies.show', compact('result', 'classroom', 'student'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create($classroom_id)
  {
    // Find classroom by classroom_id
    $classroom = $this->classrooms->findOrFail($classroom_id);

    // Fetch teachers
    $teachers = $this->teachers->select('id', 'first_name', 'last_name')->asc()->get()->pluck('full_name', 'id');

    // Fetch students
    $students = $classroom->students()->asc()->get()->pluck('full_name', 'id');

    // Set meta tags
    $this->seo()->setTitle("Turmas - {$classroom->name} - Frequências - Nova Frequência");

    // Return view
    return view('admin.classrooms_frequencies.create', compact('teachers', 'students', 'classroom'));
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request, $classroom_id)
  {
    // Find classroom by classroom_id
    $classroom = $this->classrooms->findOrFail($classroom_id);

    // Validate
    $validate = validator($request->all(),[
      'student_id' => 'required',
      'date' => 'required|date_format:d/m/Y H:i',
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Frequência.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with classroom_id
    $request->merge(['classroom_id' => $classroom->id]);

    // Create result
    $result = $this->frequencies->create($request->all());

    // Success message
    toast()->success('Frequência criada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.classrooms_frequencies.index', ['classroom_id' => $classroom->id]);
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($classroom_id, $id)
  {
    // Find classroom by classroom_id
    $classroom = $this->classrooms->findOrFail($classroom_id);

    // Fetch result by id
    $result = $this->frequencies->findOrFail($id);

    // Fetch teachers
    $teachers = $this->teachers->select('id', 'first_name', 'last_name')->asc()->get()->pluck('full_name', 'id');

    // Fetch students
    $students = $classroom->students()->asc()->get()->pluck('full_name', 'id');

    // Set meta tags
    $this->seo()->setTitle("Turma - {$classroom->name} - Frequências - Editar Frequência");

    // Return view
    return view('admin.classrooms_frequencies.edit', compact('result', 'teachers', 'students', 'classroom'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $classroom_id, $id)
  {
    // Find classroom by classroom_id
    $classroom = $this->classrooms->findOrFail($classroom_id);

    // Validate
    $validate = validator($request->all(),[
      'student_id' => 'required',
      'date' => 'required|date_format:d/m/Y H:i',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Frequência.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->frequencies->findOrFail($id);

    // Fill data and save
    $result->fill($request->all())->save();

    // Success message
    toast()->success('Frequência atualizada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.classrooms_frequencies.index', ['classroom_id' => $classroom->id]);
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $classroom_id, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Find classroom by classroom_id
      $classroom = $this->classrooms->findOrFail($classroom_id);

      // Fetch result
      $result = $this->classrooms->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Frequência removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Frequência não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Frequência.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
