<?php

namespace App\Http\Controllers\Admin\Classrooms;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Auth;
use App\Models\Classroom\Classroom;
use App\Models\Classroom\ClassroomFrequency as Frequency;
use App\Models\Student\Student;
use App\Models\Teacher\Teacher;
use App\Models\Modality\Modality;

class ClassroomController extends BaseController
{
  use SEOToolsTrait;

  private $classrooms;
  private $frequencies;
  private $students;
  private $modalities;
  private $teachers;

  /**
   * Constructor
   */
  public function __construct(Classroom $classrooms, Frequency $frequencies, Student $students, Modality $modalities, Teacher $teachers)
  {
    // Middlewares
    $this->middleware('permission:view_classrooms', ['only' => ['index']]);
    $this->middleware('permission:add_classrooms', ['only' => ['create', 'store']]);
    $this->middleware('permission:edit_classrooms', ['only' => ['edit', 'update']]);
    $this->middleware('permission:delete_classrooms', ['only' => ['destroy']]);

    // Dependency Injection
    $this->classrooms = $classrooms;
    $this->frequencies = $frequencies;
    $this->students = $students;
    $this->modalities = $modalities;
    $this->teachers = $teachers;
  }

  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function index(Request $request)
  {
    // Query filter
    $query = $this->classrooms->select('id', 'name', 'teacher_id', 'modality_id', 'active')->orderBy('id', 'DESC');

    // Filter by name param
    if ($request->filled('nome')):
      $name = $request->get('nome');
      $query->where('name', 'like', "%{$name}%");
    endif;

    // If filled status
    if ($request->filled('status')) {
      $query->whereActive($request->get('status'));
    }

    // Fetch all results
    $results = $query->paginate(5)->appends($request->except('page'));

    // Set meta tags
    $this->seo()->setTitle('Turmas');

    // Return view
    return view('admin.classrooms.index', compact('results'));
  }

  /**
   * Show the form for  a new resource.
   * @return Response
   */
  public function show($id)
  {
    // Find result
    $result = $this->classrooms->findOrFail($id);

    // Set meta tags
    $this->seo()->setTitle('Turmas - Visualizar Turma');

    // Return view
    return view('admin.classrooms.show', compact('result'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    // Fetch teachers
    $teachers = $this->teachers->select('id', 'first_name', 'last_name')->asc()->get()->pluck('full_name', 'id');

    // Fetch students
    $students = $this->students->select('id', 'first_name', 'last_name')->asc()->get()->pluck('full_name', 'id');

    // Fetch modalities
    $modalities = $this->modalities->select('id', 'slug', 'name')->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Turmas - Nova Turma');

    // Return view
    return view('admin.classrooms.create', compact('teachers', 'students', 'modalities'));
  }

  /**
  * Store a newly created resource in storage.
  * @param  Request $request
  * @return Response
  */
  public function store(Request $request)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:classrooms',
      'teacher_id' => 'required',
      'students' => 'required',
      'modality_id' => 'required'
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Turma.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Merge with user_id
    $request->merge(['user_id' => Auth::user()->id]);

    // Create result
    $result = $this->classrooms->create($request->except('students'));

    // Attach students
    $result->students()->attach($request->get('students'));

    // Success message
    toast()->success('Turma criada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.classrooms.index');
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($id)
  {
    // Fetch result by id
    $result = $this->classrooms->findOrFail($id);

    // Fetch teachers
    $teachers = $this->teachers->select('id', 'first_name', 'last_name')->asc()->get()->pluck('full_name', 'id');

    // Fetch students
    $students = $this->students->whereHas('modalities', function ($q) use ($result) {
      $q->where('modalities as modality', $result->modality->id);
    })->select('id', 'first_name', 'last_name')->asc()->get()->pluck('full_name', 'id');

    // Fetch modalities
    $modalities = $this->modalities->select('id', 'slug', 'name')->asc()->active()->pluck('name', 'id');

    // Set meta tags
    $this->seo()->setTitle('Turma - Editar Turma');

    // Return view
    return view('admin.classrooms.edit', compact('result', 'teachers', 'students', 'modalities'));
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    // Validate
    $validate = validator($request->all(),[
      'name' => 'required|max:255|unique:classrooms,name,' . $id,
      'teacher_id' => 'required',
      'students' => 'required',
      'modality_id' => 'required',
    ]);

    // If fails validate
    if($validate->fails()):
      // Warning message
     toast()->error('Falha ao atualizar Turma.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    // Fetch result
    $result = $this->classrooms->findOrFail($id);

    // Fill data and save
    $result->fill($request->except('students'))->save();

    // Sync students
    $result->students()->sync($request->get('students'));

    // Success message
    toast()->success('Turma atualizada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('admin.classrooms.index');
  }

  /**
   * Remove the specified resource from storage.
   * @param  Request $request
   * @return Response
   */
  public function destroy(Request $request, $id)
  {
    // If ajax
    if ($request->ajax()):

      // Fetch result
      $result = $this->classrooms->find($id);

      // If result exist
      if($result):

        // Remove result
        $result->delete();

        // Return success response
        return response()->json(['success' => true, 'message' => 'Turma removido com sucesso.'], 200);

      else:

        // Return error response
        return response()->json(['success' => false, 'message' => 'Turma não encontrado.'], 400);

      endif;

    endif;

    // Error message
    flash('Falha ao remover Turma.')->error();

    // Redirect to back page
    return redirect()->back();
  }
}
