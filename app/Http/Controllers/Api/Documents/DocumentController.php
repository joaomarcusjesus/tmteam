<?php

namespace App\Http\Controllers\Api\Documents;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Student\Student;
use App\Models\Student\StudentAptitude;
use Illuminate\Http\Request;

class DocumentController extends BaseController
{
  protected $aptitudes;
  protected $students;

  public function __construct(Student $students, StudentAptitude $aptitudes)
  {
    $this->aptitudes = $aptitudes;
    $this->students = $students;
  }

  public function store(Request $request, $code)
  {
    $student = $this->students->whereCode($code)->firstOrFail();

    // Validate
    $validate = validator($request->all(),[
      'fisic' => 'required|max:6000|image'
    ]);

    // If fails validate
    if($validate->fails()):

      // Warning message
      toast()->error('Falha ao adicionar Aptidão Física.', 'Error');

      // Redirect same page with errors messages
      return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
    endif;

    $request->merge(['status' => 1, 'student_id' => $student->id]);

    // Result
    $result = $this->aptitudes->create($request->except('fisic'));

    $fisic = $request->file('fisic');

    if ($fisic):
      $filename = md5($fisic->getClientOriginalName()) . '.' . $fisic->getClientOriginalExtension();
      $student->addMedia($fisic)
        ->usingName($student->full_name)
        ->usingFileName($filename)
        ->toMediaCollection('fisic');
    endif;

    // Success message
    toast()->success('Aptidão Física enviada com sucesso.', 'Sucesso');

    // Redirect to list
    return redirect()->route('students.dashboard.index');
  }
}
