<?php

namespace App\Http\Controllers\Api\Modalities;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Modality\Modality;
use App\Models\Student\Student;
use Illuminate\Http\Request;

class ModalityController extends BaseController
{
  protected $modalities;
  protected $students;

  public function __construct(Modality $modalities, Student $students)
  {
    // Dependency Injection
    $this->modalities = $modalities;
    $this->students = $students;
  }

  public function filter(Request $request, $id = null)
  {
    $modality = $this->modalities->find($id);

    $students = $this->students->whereHas('modalities', function ($q) use ($modality) {
      $q->whereSlug($modality->slug);
    })->asc()->get();

    // Return response
    return response()->json(['success' => true, 'Requisição feita com sucesso', 'data' => $students], 200);
  }
}
