<?php

namespace App\Http\Controllers\Api\Orders;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Order\Order;
use App\Models\Student\Student;
use App\Models\Subscription\Subscription;
use App\Services\Orders\OrderService;
use Auth;
use DB;

class OrderController extends BaseController
{
  protected $orders;
  protected $students;
  protected $subscriptions;
  protected $services;

  public function __construct(Student $students, Order $orders, Subscription $subscriptions, OrderService $services)
  {
    $this->students = $students;
    $this->orders = $orders;
    $this->subscriptions = $subscriptions;
    $this->services = $services;
  }

  public function subscription($code)
  {
    $result = $this->students->whereCode($code)->firstOrFail();

    $amount = $result->modalities->sum('price');

    $subscription = $this->subscriptions->create([
      'amount' => $amount,
      'type' => 2,
      'student_id' => $result->id,
      'user_id' => Auth::user()->id,
    ]);

    $order = $this->orders->create([
      'description' => "Pagamento da matrícula {$subscription->reference}",
      'amount' => $amount,
      'type' => 1,
      'cpf_or_cnpj' => $result->cpf_or_cnpj,
      'first_name' => $result->first_name,
      'last_name' => $result->last_name,
      'student_id' => $result->id
    ]);

    if (is_null($order)) {
      DB::rollback();

      toast()->warning('Falha na transação da Matrícula.', 'Atenção!');

      return redirect()->back();
    }

    $service = $this->services->sendOrder($order);

    if (is_null($service)) {

      DB::rollback();


      toast()->warning('Falha na transação da Matrícula.', 'Atenção!');

      return redirect()->back();
    }

    // If successuly
    if ($subscription) {

      // Commit
      DB::commit();

      $order->code_gateway = $service['code_gateway'];
      $order->boleto_url = $service['boleto_url'];
      $order->checkout_url = $service['checkout_url'];
      $order->installmentLink = $service['installmentLink'];
      $order->payNumber = $service['payNumber'];
      $order->save();

      // Success message
      toast()->success('Matrícula criada com sucesso.', 'Sucesso');

      // Redirect to list
      return redirect()->route('students.orders.show', ['code' => $order->code]);
    }

    // Rollback
    DB::rollback();

    // Warning message
    toast()->warning('Falha na transação da Matrícula.', 'Atenção!');

    // Redirect to back
    return redirect()->back();
  }
}
