<?php

namespace App\Scopes\Classrooms;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class ClassroomScope implements Scope
{
  /**
   * Apply the scope to a given Eloquent query builder.
   *
   * @param  \Illuminate\Database\Eloquent\Builder  $builder
   * @param  \Illuminate\Database\Eloquent\Model  $model
   * @return void
   */
  public function apply(Builder $builder, Model $model)
  {
    if(Auth::check()):

      $user = Auth::user();

      if($user->hasRole('professor') && $user->teacher) :

        $builder->whereHas('teacher', function($q) use($user) {

          $q->where('teacher_id', $user->teacher->id);
        });
      endif;
    endif;
  }
}
