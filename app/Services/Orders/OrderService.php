<?php

namespace App\Services\Orders;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use App\Models\Order\Order;

class OrderService
{
  const URL = 'https://www.boletobancario.com/boletofacil/integration/api/v1/';

  public function sendOrder(Order $order) : ?array
  {
    $client = new Client();

    $body = 'issue-charge?token=' . config('api.token') .
            '&description=Pedido' . 'Mensalidade CheckMat TMTeam via Plataforma Adminstrativa' .
            '&amount=' . $order['amount'] .
            '&payerName=' .   $order['first_name'] . '+' . $order['last_name'] .
            '&payerCpfCnpj=' . $order['cpf_or_cnpj'];
    try {
      $request = $client->get(self::URL . $body);
      $response = json_decode($request->getBody()->getContents(), true);

      return [
        'code_gateway' => $response['data']['charges'][0]['code'],
        'due_date' => $response['data']['charges'][0]['dueDate'],
        'checkout_url' => $response['data']['charges'][0]['checkoutUrl'],
        'boleto_url' => $response['data']['charges'][0]['link'],
        'installmentLink' => $response['data']['charges'][0]['installmentLink'],
        'payNumber' => $response['data']['charges'][0]['payNumber'],
      ];

    } catch (RequestException $e) {
      app('sentry')->captureException($e);
      dd($e);
      return null;
    } catch (ClientException $e) {
      app('sentry')->captureException($e);
      dd($e);
      return null;
    }
  }
}
