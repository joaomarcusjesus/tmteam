<?php

namespace App\Models\Tuition;

use App\Models\Order\Order;
use App\Models\Student\Student;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tuition extends Model
{
  protected $fillable = [
    'code',
    'schedule',
    'status',
    'descont',
    'signature',
    'amount',
    'type',
    'active',
    'isSchedule',
    'student_id',
  ];

  protected $dates = [
    'schedule',
  ];

  protected $casts = [
    'active' => 'boolean',
    'signature' => 'boolean',
    'isSchedule' => 'boolean',
  ];

  /**
   * The "booting" method of the model.
   *
   * @return void
   */
  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model)
    {
      $model->configs();
    });
  }

  /**
   * Configs
   */
  protected function configs()
  {
    // Set code
    $this->attributes['code'] = sha1(date('dmyhis') . $this->student->cpf_or_cnpj . $this->student->full_name);
    $this->attributes['active'] = true;

    if ($this->type == 2) {
      $this->attributes['status'] = 1;
    } else {
      $this->attributes['status'] = 2;
    }

    if (!$this->isToday) {
      $this->attributes['status'] = 4;
    }
  }

  /**
   * Scopes
   */

  /**
   * Relationships
   */
  public function student()
  {
    return $this->belongsTo(Student::class, 'student_id');
  }

  public function orders()
  {
    return $this->belongsToMany(Order::class, 'tuitions_orders', 'tuitions_id', 'order_id');
  }

  /**
   * Mutators
   */
  public function setScheduleAttribute($input)
  {
    if($input)
      $this->attributes['schedule'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  public function setAmountAttribute($value)
  {
    $this->attributes['amount'] = ($value != null) ? str_replace(['.', ','], ['', '.'], $value) : null;
  }

  public function setDescontAttribute($value)
  {
    $this->attributes['descont'] = ($value != null) ? str_replace(['.', ','], ['', '.'], $value) : null;
  }

  /**
   * Accesors
   */

  public function getReferenceAttribute()
  {
    return "#" . str_pad($this->id, 4, '0', STR_PAD_LEFT);
  }

  public function getTotalAttribute()
  {
    $amount = $this->amount;

    if ($this->descont) {
      $amount = $this->amount - $this->descont;
    }

    return number_format($amount, 2);
  }

  public function getTypeNameAttribute()
  {
    $name = null;

    switch ($this->type) {
      case 1:
        $name = 'Presencial';
        break;
      case 2:
        $name = 'Virtual';
        break;
    }

    return $name;
  }

  public function getStatusNameAttribute()
  {
    $name = null;

    switch ($this->status) {
      case 1:
        $name = 'Aguardando pagamento';
        break;
      case 2:
        $name = 'Pagamento efetuado';
        break;
      case 3:
        $name = 'Em atraso';
        break;
      case 4:
        $name = "Agendado em {$this->schedule->format('d/m/Y')}";
        break;
    }

    return $name;
  }

  public function getSendDateAttribute()
  {
    return $this->schedule->format('d/m/Y');
  }

  public function getStatusBadgeAttribute()
  {
    $status_name = '';

    switch($this->status):
      case 1:
        $status_name = '<span class="badge badge-info -info">Aguardando pagamento</span>';
        break;
      case 2:
        $status_name = '<span class="badge badge-success -status">Pagamento Efetuado</span>';
        break;
      case 3:
        $status_name = '<span class="badge badge-success -status">Em atraso</span>';
        break;
      case 4:
        $status_name = "<span class='badge badge-warning -status'>Agendado em {$this->sendDate}</span>";
        break;
    endswitch;

    return $status_name;
  }

  public function getActiveNameAttribute()
  {
    return $this->active ? 'Sim'  : 'Não';
  }

  public function getScheduleNameAttribute()
  {
    return $this->isSchedule ? 'Sim'  : 'Não';
  }

  public function getIsTodayAttribute()
  {
    return $this->schedule->lte(Carbon::now());
  }
}
