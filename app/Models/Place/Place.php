<?php

namespace App\Models\Place;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
  protected $fillable = [
    'placeable',
    'cep',
    'street',
    'neighborhood',
    'state',
    'complement',
    'city',
    'number'
  ];

  /**
  * Mutators
  */
  public function setCepAttribute($value)
  {
    if ($value)
      $this->attributes['cep'] = trim(preg_replace('#[^0-9]#', '', $value));
  }

  /**
  * Accessors
  */
   public function getFullAddressAttribute()
   {
      return "{$this->street} - {$this->neighborhood} - {$this->city} - {$this->number} - {$this->cep} - {$this->state}";
   }
}
