<?php

namespace App\Models\Order;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Student\Student;

class Order extends Model
{
  protected $fillable = [
    'code',
    'code_gateway',
    'first_name',
    'boleto_url',
    'checkout_url',
    'installmentLink',
    'payNumber',
    'last_name',
    'registration',
    'cpf_or_cnpj',
    'description',
    'type',
    'status',
    'amount',
    'student_id',
  ];

  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model)
    {
      $model->configs();
    });
  }

  /**
  * Configs
  */
  protected function configs()
  {
    // Set code
    $this->attributes['code'] = sha1(date('dmyhis') . $this->student->full_name);
    $this->attributes['status'] = 1;
    $this->attributes['registration'] =  substr(sha1(date('dmyhis') . $this->student->code), 0, 15);
  }

  /**
  * Scopes
  */

  /**
  * Relationships
  */
  public function student()
  {
    return $this->belongsTo(Student::class, 'student_id');
  }

  /**
  * Mutators
  */
  public function setAmountAttribute($value)
  {
    $this->attributes['amount'] = ($value != null) ? str_replace(['.', ','], ['', '.'], $value) : null;
  }

  public function setCpfOrCnpjAttribute($value)
  {
    if ($value)
      $this->attributes['cpf_or_cnpj'] = ($value != null) ? trim(preg_replace('#[^0-9]#', '', $value)) : null;
  }

  public function setFirstNameAttribute($value)
  {
    if ($value)
      $this->attributes['first_name'] = titleCaseBR($value);
  }

  public function setLastNameAttribute($value)
  {
    if ($value)
      $this->attributes['last_name'] = titleCaseBR($value);
  }

  /**
  * Accessors
  */
  public function getReferenceAttribute()
  {
    return "#" . str_pad($this->id, 4, '0', STR_PAD_LEFT);
  }

  public function getTypeNameAttribute()
  {
    $text = '';


    switch ($this->type) {
      case 1:
        $text = 'Mensalidade';
        break;

      case 2:
        $text = 'Matrícula';
        break;
    }

    return $text;
  }

  public function getStatusBadgeAttribute()
  {
    $status_name = '';

    switch($this->status):
      case 1:
        $status_name = '<span class="badge badge-info -info">Aguardando pagamento</span>';
        break;
      case 2:
        $status_name = '<span class="badge badge-success -status">Pagamento Efetuado</span>';
        break;
      case 3:
        $status_name = '<span class="badge badge-success -status">Em atraso</span>';
        break;
    endswitch;

    return $status_name;
  }


  public function getIsVirtualAttribute()
  {
    if ($this->type == 2) {
      return true;
    }

    return false;
  }
}
