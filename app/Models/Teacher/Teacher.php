<?php

namespace App\Models\Teacher;

use App\Models\Classroom\Classroom;
use App\Models\Student\StudentModality;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Auth;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use App\Models\Auth\User;
use App\Models\Modality\Modality;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model implements HasMedia, AuditableInterface
{
  use HasMediaTrait, AuditableTrait, Sluggable;

  protected $fillable = [
    'slug',
    'nickname',
    'first_name',
    'last_name',
    'email',
    'cpf_or_cnpj',
    'phone',
    'active',
    'user_id',
    'birthday',
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  protected $appends = [
    'full_name'
  ];

  protected $dates = [
    'birthday',
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'nickname',
    'first_name',
    'last_name',
    'email',
    'body',
    'cpf_or_cnpj',
    'phone',
    'receive_messages',
    'active',
    'user_id',
    'birthday',
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'full_name'
      ]
    ];
  }

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
    $this->addMediaConversion('photo')
         ->fit(Manipulations::FIT_CROP, 320, 320)
         ->quality(90)
         ->nonQueued();
  }

  /**
  * Scopes
  */
  public function scopeAsc($query)
  {
    return $query->orderBy('first_name', 'ASC');
  }

  public function scopeActive($query)
  {
    return $query->where('active', true);
  }

  /**
   * Relationships
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function classrooms()
  {
    return $this->hasMany(Classroom::class, 'teacher_id');
  }

  public function modalities()
  {
    return $this->belongsToMany(StudentModality::class, 'teacher_modality', 'teacher_id', 'modality_id');
  }

  /**
  * Mutators
  */
  public function setFirstNameAttribute($value)
  {
    if ($value)
      $this->attributes['first_name'] = titleCaseBR($value);
  }

  public function setLastNameAttribute($value)
  {
    if ($value)
      $this->attributes['last_name'] = titleCaseBR($value);
  }

  public function setNicknameAttribute($value)
  {
    if ($value)
      $this->attributes['nickname'] = titleCaseBR($value);
  }

  public function setEmailAttribute($input)
  {
    if ($input)
      $this->attributes['email'] = mb_strtolower($input, 'UTF-8');
  }

  public function setPhoneAttribute($input)
  {
    if ($input)
      $this->attributes['phone'] = trim(preg_replace('#[^0-9]#', '', $input));
  }

  public function setCpfOrCnpjAttribute($value)
  {
    if ($value)
      $this->attributes['cpf_or_cnpj'] = ($value != null) ? trim(preg_replace('#[^0-9]#', '', $value)) : null;
  }

  public function setBirthdayAttribute($input)
  {
    if($input)
      $this->attributes['birthday'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  /**
  * Accesors
  */
  public function getLastLoginAttribute()
  {
    return ($this->latest_login) ? $this->latest_login->diffForHumans() : 'Não efetuou login';
  }

  public function getFullNameAttribute()
  {
    return "{$this->first_name} {$this->last_name}";
  }

  public function getPhoneMaskAttribute()
  {
    return mask((strlen($this->phone) == 10) ? '(##) ####-####' : '(##) #####-####', $this->phone);
  }

  public function getIdentificationMaskAttribute()
  {
    return mask((strlen($this->cpf_or_cnpj) == 11) ? '###.###.###-##' : '##############', $this->cpf_or_cnpj);
  }

  public function getStatusNameAttribute()
  {
    return $this->active ? "<span class='badge badge-primary'>Ativo</span" : "<span class='badge badge-danger'>Desativado</span>";
  }

  public function getPhotoAttribute()
  {
    $image = $this->getMedia('user')->first();
    return isset($image) ? $image->getUrl('photo') : asset('/assets/img/default.png');
  }
}
