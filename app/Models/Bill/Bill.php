<?php

namespace App\Models\Bill;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Carbon\Carbon;

class Bill extends Model implements AuditableInterface
{
  use AuditableTrait;

  protected $fillable = [
    'code',
    'type',
    'date',
    'value',
    'type_id',
    'user_id',
    'active'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  protected $dates = [
    'date'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'code',
    'type',
    'date',
    'value',
    'type_id',
    'user_id',
    'active'
  ];

  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model)
    {
      $model->configs();
    });
  }

   /**
  * Configs
  */
  protected function configs()
  {
    // Set code
    $this->attributes['code'] = sha1(date('dmyhis') . $this->user->full_name);
  }

  /**
  * Scopes
  */
  public function scopeAsc($query)
  {
    return $query->orderBy('name', 'ASC');
  }

  public function scopeActive($query)
  {
    $query->whereActive(true);
  }

  /**
  * Relationships
  */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function typeRelation()
  {
    return $this->belongsTo(BillType::class, 'type_id');
  }

  public function billable()
  {
    return $this->morphTo();
  }

  /**
  * Mutators
  */
  public function setDateAttribute($input)
  {
    if($input)
      $this->attributes['date'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  public function setValueAttribute($value)
  {
    $this->attributes['value'] = ($value != null) ? str_replace(['.', ','], ['', '.'], $value) : null;
  }

  /**
  * Accessors
  */
  public function getReferenceAttribute()
  {
    return "#" . str_pad($this->id, 4, '0', STR_PAD_LEFT);
  }

  public function getFormatNameAttribute()
  {
    $type = null;

    switch ($this->type) {
      case 1:
       $type = 'Mensal';
        break;
      case 2:
       $type = 'Anual';
        break;
      case 3:
        $type = 'Outros';
        break;
    }

    return $type;
  }
}
