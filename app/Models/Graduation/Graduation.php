<?php

namespace App\Models\Graduation;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;

class Graduation extends Model implements AuditableInterface
{
  use AuditableTrait;

  protected $fillable = [
    'code',
    'student_active',
    'is_kids',
    'student_id',
    'teacher_id',
    'classroom_id',
    'band_id',
    'rule_id',
    'user_id'
  ];

  protected $casts = [
    'student_active' => 'boolean',
    'is_kids' => 'boolean'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'code',
    'student_active',
    'is_kids',
    'student_id',
    'teacher_id',
    'classroom_id',
    'band_id',
    'rule_id',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model)
    {
      $model->configs();
    });
  }

   /**
  * Configs
  */
  protected function configs()
  {
    // Set code
    $this->attributes['code'] = sha1(date('dmyhis') . $this->creator->full_name);
  }

  /**
  * Scopes
  */
  public function scopeAsc($query)
  {
    return $query->orderBy('name', 'ASC');
  }

  public function scopeActive($query)
  {
    $query->whereActive(true);
  }

  /**
  * Relationships
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }
}
