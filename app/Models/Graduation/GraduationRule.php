<?php

namespace App\Models\Graduation;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class GraduationRule extends Model implements AuditableInterface
{
  use Sluggable, AuditableTrait;

  protected $table = 'graduations_rules';

  protected $fillable = [
    'slug',
    'name',
    'limit',
    'period',
    'month',
    'classrooms',
    'band_id',
    'no_limit',
    'is_kids',
    'user_id',
    'active'
  ];

  protected $casts = [
    'active' => 'boolean',
    'no_limit' => 'boolean',
    'is_kids' => 'boolean'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'name',
    'limit',
    'period',
    'month',
    'classrooms',
    'band_id',
    'no_limit',
    'is_kids',
    'user_id',
    'active'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();
  }

  /**
  * Scopes
  */
  public function scopeAsc($query)
  {
    return $query->orderBy('name', 'ASC');
  }

  public function scopeActive($query)
  {
    $query->whereActive(true);
  }

  /**
  * Relationships
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
  * Mutators
  */
  public function setNameAttribute($value)
  {
    if ($value)
      $this->attributes['name'] = titleCaseBR($value);
  }

  /**
  * Accessors
  */
}
