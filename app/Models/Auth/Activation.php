<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Activation extends Model
{
  protected $fillable = [
    'token',
    'email',
    'expires_in'
  ];

  protected $dates = [
    'expires_in'
  ];

   /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model)
    {
      $model->configs();
    });
  }

  /**
   * Configs
   */
  protected function configs()
  {
    $this->attributes['token'] = sha1(date('dmyhis') . $this->email);
  }

  /**
  * Relationships
  */

  /**
  * Mutators
  */
  public function setExpiresInAttribute($input)
  {
    if($input)
      $this->attributes['expires_in'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  /**
   * Static
   * @param $token
   * @return Activation
   */
   public static function validateToken($token)
   {
      $activation = Activation::whereToken($token)->first();

      if (is_null($activation)) {
        return view('admin.errors.404');
      }

      if ($activation->expires_in->isPast()) {
        return view('admin.errors.403');
      }

      return $activation;
   }

  /**
  * Static
  * @param User
  * @return Activation
  */
  public static function generateToken(User $user)
  {
    $start = Carbon::now();

    $activation = Activation::create(['email' => $user->email, 'expires_in' => $start->addDays(1)->format('d/m/Y')]);

    return $activation;
  }
}
