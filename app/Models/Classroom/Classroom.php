<?php

namespace App\Models\Classroom;

use App\Models\Teacher\Teacher;
use App\Models\Student\Student;
use App\Models\Student\StudentModality as Modality;
use App\Models\Auth\User;
use App\Scopes\Classrooms\ClassroomScope;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Classroom extends Model implements AuditableInterface
{
  use Sluggable, AuditableTrait;

  protected $fillable = [
    'slug',
    'name',
    'modality_id',
    'teacher_id',
    'user_id',
    'active'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];


  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'name',
    'modality_id',
    'teacher_id',
    'user_id',
    'active'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();
    static::addGlobalScope(new ClassroomScope());
  }

  /**
  * Scopes
  */
  public function scopeAsc($query)
  {
    return $query->orderBy('name', 'ASC');
  }

  public function scopeActive($query)
  {
    $query->whereActive(true);
  }

  /**
  * Relationships
  */
  public function teacher()
  {
    return $this->belongsTo(Teacher::class, 'teacher_id');
  }

  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function modality()
  {
    return $this->belongsTo(Modality::class, 'modality_id');
  }

  public function students()
  {
    return $this->belongsToMany(Student::class, 'classrooms_students', 'classroom_id', 'student_id')->orderBy('students.first_name', 'ASC');
  }

  public function frequencies()
  {
    return $this->belongsToMany(Student::class, 'classrooms_frequencies', 'classroom_id', 'student_id')->withPivot(['body', 'date', 'type', 'released']);
  }

  /**
  * Mutators
  */
  public function setNameAttribute($value)
  {
    if ($value)
      $this->attributes['name'] = titleCaseBR($value);
  }

  /**
  * Accessors
  */
}
