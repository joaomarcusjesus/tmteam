<?php

namespace App\Models\Classroom;

use App\Models\Student\Student;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ClassroomFrequency extends Model
{
  protected $table = 'classrooms_frequencies';

  protected $fillable = [
    'classroom_id',
    'student_id',
    'body',
    'date',
    'type',
    'released'
  ];

  protected $casts = [
    'released' => 'boolean'
  ];

  protected $dates = [
    'date'
  ];

  /**
  * Relationships
  */
  public function student()
  {
    return $this->belongsTo(Student::class, 'student_id');
  }

  public function classroom()
  {
    return $this->belongsTo(Classroom::class, 'classroom_id');
  }

  /**
  * Mutators
  */
  public function setDateAttribute($input)
  {
    if($input)
      $this->attributes['date'] = Carbon::createFromFormat('d/m/Y H:i', $input)->format('Y-m-d H:i:s');
  }

  /**
   * Accesors
   */
  public function getSendDateAttribute()
  {
    $this->date ? $this->date->format('d/m/Y H:i') : $this->created_at->format('d/m/Y H:i');
  }
}
