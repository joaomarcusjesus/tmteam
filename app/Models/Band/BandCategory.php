<?php

namespace App\Models\Band;

use App\Models\Unit\Unit;
use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class BandCategory extends Model implements AuditableInterface
{
  use Sluggable, AuditableTrait;

  protected $table = 'bands_categories';

  protected $fillable = [
    'slug',
    'name',
    'age_finish',
    'age_begin',
    'user_id',
    'active'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'name',
    'age_finish',
    'age_begin',
    'user_id',
    'active'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * Scopes
  */
  public function scopeAsc($query)
  {
    return $query->orderBy('name', 'ASC');
  }

  public function scopeActive($query)
  {
    $query->whereActive(true);
  }

  /**
  * Relationships
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
  * Mutators
  */
  public function setNameAttribute($value)
  {
    if ($value)
      $this->attributes['name'] = titleCaseBR($value);
  }

  /**
  * Accessors
  */
  public function getDegreeNameAttribute()
  {
    $name = null;

    switch ($this->degree) {
      case 1:
        $name = '1 Grau';
        break;
      case 2:
        $name = '2 Grau';
        break;
      case 3:
        $name = '3 Grau';
        break;
      case 4:
        $name = '4 Grau';
        break;
    }

    return $name;
  }

  public function getAgeNameAttribute()
  {
    return "{$this->age_begin} anos até {$this->age_finish}";
  }
}
