<?php

namespace App\Models\Band;

use App\Models\Student\Student;
use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Band extends Model implements AuditableInterface
{
  use Sluggable, AuditableTrait;

  protected $fillable = [
    'slug',
    'name',
    'color',
    'category_id',
    'user_id',
    'active',
    'age_finish',
    'age_begin',
    'degree'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'name',
    'color',
    'category_id',
    'user_id',
    'active',
    'age_finish',
    'age_begin',
    'degree'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * Scopes
  */
  public function scopeAsc($query)
  {
    return $query->orderBy('name', 'ASC');
  }

  public function scopeActive($query)
  {
    $query->whereActive(true);
  }

  /**
  * Relationships
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function category()
  {
    return $this->belongsTo(BandCategory::class, 'category_id');
  }

  public function students()
  {
    return $this->belongsToMany(Student::class, 'bands_students', 'band_id', 'student_id')->withPivot(['body', 'feedback', 'period', 'degree', 'process', 'active', 'approved']);
  }

  /**
  * Mutators
  */
  public function setNameAttribute($value)
  {
    if ($value)
      $this->attributes['name'] = titleCaseBR($value);
  }

  /**
  * Accessors
  */
  public function getColorNameAttribute()
  {
    $color = null;

    switch ($this->color) {
      case 1:
        $color = 'Branca';
      break;
     case 2:
       $color = 'Cinza';
      break;
     case 3:
       $color = 'Preta';
      break;
     case 4:
       $color = 'Amarela';
      break;
     case 5:
        $color = 'Marrom';
      break;
     case 6:
        $color = 'Laranja';
      break;
     case 7:
        $color = 'Verde';
      break;
     case 8:
       $color = 'Azul';
      break;
     case 9:
        $color = 'Roxa';
      break;
    }

    return $color;
  }

  public function getColorClassAttribute()
  {
    $color = null;

    switch ($this->color) {
      case 1:
        $color = 'branca';
      break;
     case 2:
       $color = 'cinza';
      break;
     case 3:
       $color = 'preta';
      break;
     case 4:
       $color = 'amarela';
      break;
     case 5:
        $color = 'marrom';
      break;
     case 6:
        $color = 'laranja';
      break;
     case 7:
        $color = 'verde';
      break;
     case 8:
       $color = 'azul';
      break;
     case 9:
        $color = 'roxa';
      break;
    }

    return $color;
  }
}
