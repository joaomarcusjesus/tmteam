<?php

namespace App\Models\Modality;

use App\Models\Auth\User;
use App\Models\Unit\Unit;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Modality extends Model implements AuditableInterface
{
  use Sluggable, AuditableTrait;

  protected $table = 'students_modalities';

  protected $fillable = [
    'slug',
    'name',
    'price',
    'descont',
    'active'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'name',
    'price',
    'descont',
    'active'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /**
  * Scopes
  */
  public function scopeAsc($query)
  {
    return $query->orderBy('name', 'ASC');
  }

  public function scopeActive($query)
  {
    $query->whereActive(true);
  }

  /**
  * Relationships
  */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
  * Mutators
  */
  public function setNameAttribute($value)
  {
    if ($value)
      $this->attributes['name'] = titleCaseBR($value);
  }

  public function setPriceAttribute($value)
  {
    $this->attributes['price'] = ($value != null) ? str_replace(['.', ','], ['', '.'], $value) : null;
  }

  /**
  * Accessors
  */
  public function getDescontNameAttribute()
  {
    return $this->descont ?  "{$this->descont} %" : '-';
  }
}
