<?php

namespace App\Models\Document;

use App\Models\Auth\User;
use App\Models\Student\Student;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class Document extends Model implements AuditableInterface
{
  use AuditableTrait;

  protected $fillable = [
    'title',
    'user_id',
    'active'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
   * Auditable Config
   */
  protected $auditInclude = [
    'title',
    'user_id',
    'active'
  ];

  /**
   * Scopes
   */
  public function scopeAsc($query)
  {
    return $query->orderBy('title', 'ASC');
  }

  public function scopeActive($query)
  {
    $query->whereActive(true);
  }

  /**
   * Relationships
   */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function students()
  {
    $this->belongsToMany(Student::class, 'student_document', 'document_id', 'student_id');
  }
}
