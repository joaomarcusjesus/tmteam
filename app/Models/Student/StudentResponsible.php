<?php

namespace App\Models\Student;

use Auth;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use App\Models\Auth\User;
use App\Models\Place\Place;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class StudentResponsible extends Model implements HasMedia, AuditableInterface
{
  use HasMediaTrait, AuditableTrait, Sluggable;

  protected $table = 'students_responsibles';

  protected $fillable = [
    'slug',
    'first_name',
    'last_name',
    'email',
    'cpf_or_cnpj',
    'phone',
    'student_id',
    'active',
    'user_id'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  protected $dates = [
    'birthday'
  ];

  protected $appends = [
    'full_name'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'first_name',
    'last_name',
    'email',
    'body',
    'cpf_or_cnpj',
    'phone',
    'birthday',
    'age',
    'sexo',
    'student_id',
    'active',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'full_name'
      ]
    ];
  }

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
    $this->addMediaConversion('photo')
         ->fit(Manipulations::FIT_CROP, 320, 320)
         ->quality(90)
         ->nonQueued();
  }

  /**
   * Scopes
   */
  public function findForPassport($identifier) {
    return $this->orWhere('email', $identifier)->where('active', true)->first();
  }

  public function scopebyAsc($query)
  {
    return $query->orderBy('first_name', 'ASC');
  }

  public function scopebyActive($query)
  {
    return $query->where('active', true);
  }

  /**
   * Relationships
   */
  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function student()
  {
    return $this->belongsTo(Student::class, 'student_id');
  }

  public function placeable()
  {
    return $this->morphOne(Place::class, 'placeable');
  }

  /**
  * Mutators
  */
  public function setFirstNameAttribute($value)
  {
    if ($value)
      $this->attributes['first_name'] = titleCaseBR($value);
  }

  public function setLastNameAttribute($value)
  {
    if ($value)
      $this->attributes['last_name'] = titleCaseBR($value);
  }

  public function setEmailAttribute($input)
  {
    if ($input)
      $this->attributes['email'] = mb_strtolower($input, 'UTF-8');
  }

  public function setPhoneAttribute($input)
  {
    if ($input)
      $this->attributes['phone'] = trim(preg_replace('#[^0-9]#', '', $input));
  }

  public function setCpfOrCnpjAttribute($value)
  {
    if ($value)
      $this->attributes['cpf_or_cnpj'] = ($value != null) ? trim(preg_replace('#[^0-9]#', '', $value)) : null;
  }

  public function setBirthdayAttribute($input)
  {
    if($input)
      $this->attributes['birthday'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  /**
  * Accesors
  */
  public function getFullNameAttribute()
  {
    return "{$this->first_name} {$this->last_name}";
  }

  public function getStatusNameAttribute()
  {
    return $this->active ? "<span class='badge badge-primary'>Ativo</span" : "<span class='badge badge-danger'>Desativado</span>";
  }

  public function getPhotoAttribute()
  {
    $image = $this->getMedia('user')->first();
    return isset($image) ? $image->getUrl('photo') : asset('/assets/img/default.png');
  }

  public function getPhoneMaskAttribute()
  {
    return mask((strlen($this->phone) == 10) ? '(##) ####-####' : '(##) #####-####', $this->phone);
  }

  public function getIdentificationMaskAttribute()
  {
    return mask((strlen($this->cpf_or_cnpj) == 11) ? '###.###.###-##' : '##############', $this->cpf_or_cnpj);
  }
}
