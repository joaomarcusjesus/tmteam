<?php

namespace App\Models\Student;

use Illuminate\Database\Eloquent\Model;

class StudentAptitude extends Model
{
  protected $table = 'students_aptitudes';

  protected $fillable = [
    'status',
    'release',
    'follow',
    'student_id'
  ];

  protected $casts = [
    'release' => 'boolean',
    'follow' => 'boolean'
  ];

  /**
   * Relationships
   */
  public function student()
  {
    return $this->belongsTo(Student::class, 'student_id');
  }

  /**
   * Accessors
   */
  public function getIsReleaseAttribute()
  {
    return $this->release ? true : false;
  }
}
