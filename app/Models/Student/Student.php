<?php

namespace App\Models\Student;

use App\Models\Document\Document;
use App\Models\Order\Order;
use App\Models\Tuition\Tuition;
use App\Notifications\Students\WelcomeStudent;
use Illuminate\Notifications\Notifiable;
use Auth;
use App\Notifications\Students\Welcome;
use App\Notifications\Students\ResetPasswordNotification;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use App\Models\Classroom\Classroom;
use App\Models\Auth\User;
use App\Models\Subscription\Subscription;
use App\Models\Place\Place;
use App\Models\Band\Band;
use App\Models\Classroom\ClassroomFrequency;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Student extends Model implements HasMedia, AuditableInterface
{
  use Notifiable, HasMediaTrait, AuditableTrait, Sluggable;

  protected $fillable = [
    'code',
    'registration',
    'nickname',
    'slug',
    'first_name',
    'last_name',
    'email',
    'cpf_or_cnpj',
    'phone',
    'age',
    'sexo',
    'degree',
    'birthday',
    'experimental',
    'active',
    'band_id',
    'origin_id',
    'user_id'
  ];

  protected $casts = [
    'experimental' => 'boolean',
    'active' => 'boolean'
  ];

  protected $dates = [
    'birthday'
  ];

  protected $appends = [
    'full_name'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'code',
    'registration',
    'nickname',
    'slug',
    'first_name',
    'last_name',
    'email',
    'cpf_or_cnpj',
    'phone',
    'age',
    'sexo',
    'birthday',
    'band_id',
    'experimental',
    'active',
    'origin_id',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'full_name'
      ]
    ];
  }

  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model)
    {
      $model->configs();
    });
  }

  /**
  * Configs
  */
  protected function configs()
  {
    // Set code
    $this->attributes['code'] = sha1(date('dmyhis') . $this->cpf_or_cnpj . $this->full_name);
    $this->attributes['registration'] = sha1(date('dmyhis') . $this->cpf_or_cnpj);
  }

  /**
   * MediaLibrary Config
   */
  public function registerMediaConversions(Media $media = null)
  {
    $this->addMediaConversion('photo')
         ->fit(Manipulations::FIT_CROP, 320, 320)
         ->quality(90)
         ->nonQueued();
  }

  /**
   * Notifications
   */
  public function sendWelcomeNotification($token, $user)
  {
    $this->notify(new WelcomeStudent($token, $user));
  }

  public function sendPasswordResetNotification($token)
  {
    $this->notify(new ResetPasswordNotification($token, $this));
  }

  public function sendContactNotification($message)
  {
    $this->notify(new ContactNotification($message, $this));
  }

  /**
   * Scopes
   */
  public function findForPassport($identifier) {
    return $this->orWhere('email', $identifier)->where('active', true)->first();
  }

  public function scopeAsc($query)
  {
    return $query->orderBy('first_name', 'ASC');
  }

  public function scopeActive($query)
  {
    return $query->where('active', true);
  }

  /**
   * Relationships
   */
  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function orders()
  {
    return $this->hasMany(Order::class, 'student_id');
  }

  public function origin()
  {
    return $this->belongsTo(StudentOrigin::class, 'origin_id');
  }

  public function modalities()
  {
    return $this->belongsToMany(StudentModality::class, 'student_modality', 'student_id', 'modality_id');
  }

  public function classrooms()
  {
    return $this->belongsToMany(Classroom::class, 'classrooms_students', 'student_id', 'classroom_id');
  }

  public function responsible()
  {
    return $this->hasOne(StudentResponsible::class, 'student_id');
  }

  public function frequencies()
  {
    return $this->hasMany(ClassroomFrequency::class, 'student_id');
  }

  public function placeable()
  {
    return $this->morphOne(Place::class, 'placeable');
  }

  public function subscription()
  {
    return $this->hasOne(Subscription::class, 'student_id');
  }

  public function band()
  {
    return $this->belongsTo(Band::class, 'band_id');
  }

  public function tuitions()
  {
    return $this->hasMany(Tuition::class, 'student_id');
  }

  public function aptitude()
  {
    return $this->hasOne(StudentAptitude::class, 'student_id');
  }

  /**
  * Mutators
  */
  public function setFirstNameAttribute($value)
  {
    if ($value)
      $this->attributes['first_name'] = titleCaseBR($value);
  }

  public function setLastNameAttribute($value)
  {
    if ($value)
      $this->attributes['last_name'] = titleCaseBR($value);
  }

  public function setNicknameAttribute($value)
  {
    if ($value)
      $this->attributes['nickname'] = titleCaseBR($value);
  }

  public function setEmailAttribute($input)
  {
    if ($input)
      $this->attributes['email'] = mb_strtolower($input, 'UTF-8');
  }

  public function setPhoneAttribute($input)
  {
    if ($input)
      $this->attributes['phone'] = trim(preg_replace('#[^0-9]#', '', $input));
  }

  public function setCpfOrCnpjAttribute($value)
  {
    if ($value)
      $this->attributes['cpf_or_cnpj'] = ($value != null) ? trim(preg_replace('#[^0-9]#', '', $value)) : null;
  }

  public function setBirthdayAttribute($input)
  {
    if($input)
      $this->attributes['birthday'] = Carbon::createFromFormat('d/m/Y', $input)->format('Y-m-d');
  }

  /**
  * Accesors
  */

  public function getTotalModalitiesAttribute()
  {
    $value = number_format($this->modalities->sum('price'), 2);

    return "<span class='badge badge-info'>". money($value, 'BRL')."</span>";
  }

  public function getTotalModalitiesSumAttribute()
  {

    return number_format($this->modalities->sum('price'), 2);
  }

  public function getFullNameAttribute()
  {
    return "{$this->first_name} {$this->last_name}";
  }

  public function getTotalFrequenciesAttribute()
  {
    return $this->frequencies->filter(function($q) {
      if($q->released)
        return $q;
    })->count();
  }

  public function getTotalFaultsAttribute()
  {
    return $this->frequencies->filter(function($q) {
      if(!$q->released)
        return $q;
    })->count();
  }

  public function getAgeNameAttribute()
  {
    return "{$this->age} anos";
  }

  public function getDegreeNameAttribute()
  {
    return "Grau {$this->degree}";
  }

  public function getSexoNameAttribute()
  {
    $sexo = '';

    switch ($this->sexo) {
      case 1:
        $sexo = 'Masculino';
        break;

      case 2:
        $sexo = 'Feminino';
        break;
    }

    return $sexo;
  }

  public function getSubscriptionNameAttribute()
  {
    return $this->subscription ? 'Sim' : 'Não';
  }

  public function getResponsibleNameAttribute()
  {
    return $this->hasResponsible ? 'Sim' : 'Não';
  }

  public function getExperimentalNameAttribute()
  {
    return $this->experimental ? 'Sim' : 'Não';
  }

  public function getStatusNameAttribute()
  {
    return $this->active ? "<span class='badge badge-primary'>Ativo</span" : "<span class='badge badge-danger'>Desativado</span>";
  }

  public function getPhotoAttribute()
  {
    $image = $this->getMedia('user')->first();
    return isset($image) ? $image->getUrl('photo') : asset('/assets/img/default.png');
  }

  public function getFisicAttribute()
  {
    $image = $this->getMedia('fisic')->first();
    return isset($image) ? $image->getUrl() : null;
  }

  public function getPhoneMaskAttribute()
  {
    return mask((strlen($this->phone) == 10) ? '(##) ####-####' : '(##) #####-####', $this->phone);
  }

  public function getIdentificationMaskAttribute()
  {
    return mask((strlen($this->cpf_or_cnpj) == 11) ? '###.###.###-##' : '##############', $this->cpf_or_cnpj);
  }

  public function getIsJiuJitsuAttribute()
  {
    if ($this->band) {
      if ($this->band->slug == 'jiu-jitsu') {
        return true;
      }
    }

    return false;
  }
}
