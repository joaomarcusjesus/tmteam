<?php

namespace App\Models\Message;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Department extends Model implements AuditableInterface
{
  use AuditableTrait;
  use Sluggable;

  protected $fillable = [
    'slug',
    'title',
    'active',
    'user_id'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'slug',
    'title',
    'active',
    'user_id'
  ];

  /**
  * Return the sluggable configuration array for this model.
  *
  * @return array
  */
  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'title'
      ]
    ];
  }

  /**
  * Scopes
  */
  public function scopeActived($query)
  {
    return $query->where('active', true);
  }

  /**
  * Relations
  */
  public function agent()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function users()
  {
    return $this->belongsToMany(User::class, 'departments_users', 'department_id', 'user_id');
  }

  public function messages()
  {
    return $this->hasMany(Message::class, 'department_id');
  }

  /**
  * Mutators
  */
}
