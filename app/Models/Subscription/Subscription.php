<?php

namespace App\Models\Subscription;

use App\Models\Modality\Modality;
use App\Models\Student\Student;
use App\Models\Student\StudentModality;
use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable as AuditableTrait;

class Subscription extends Model implements AuditableInterface
{
  use AuditableTrait;

  protected $fillable = [
    'code',
    'descont',
    'type',
    'amount',
    'status',
    'student_id',
    'user_id',
    'active'
  ];

  protected $casts = [
    'active' => 'boolean'
  ];

  /**
  * Auditable Config
  */
  protected $auditInclude = [
    'code',
    'type',
    'descont',
    'amount',
    'status',
    'student_id',
    'user_id',
    'active'
  ];

  /**
  * The "booting" method of the model.
  *
  * @return void
  */
  protected static function boot()
  {
    parent::boot();

    static::creating(function ($model)
    {
      $model->configs();
    });
  }

  /**
  * Configs
  */
  protected function configs()
  {
    // Set code
    $this->attributes['code'] = sha1(date('dmyhi') . $this->student->full_name);

    if ($this->type == 2) {
      $this->attributes['status'] = 1;
    } else {
      $this->attributes['status'] = 2;
    }

    $this->student->active = true;
    $this->student->save();
  }

  /**
  * Scopes
  */
  public function scopeAsc($query)
  {
    return $query->orderBy('name', 'ASC');
  }

  public function scopeActive($query)
  {
    $query->whereActive(true);
  }

  /**
  * Relationships
  */
  public function student()
  {
    return $this->belongsTo(Student::class, 'student_id');
  }


  public function creator()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  /**
  * Mutators
  */
  public function setNameAttribute($value)
  {
    if ($value)
      $this->attributes['name'] = titleCaseBR($value);
  }

  public function setAmountAttribute($value)
  {
    $this->attributes['amount'] = ($value != null) ? str_replace(['.', ','], ['', '.'], $value) : null;
  }

  /**
  * Accessors
  */
  public function getReferenceAttribute()
  {
    return "#" . str_pad($this->id, 4, '0', STR_PAD_LEFT);
  }

  public function getPlotsNameAttribute()
  {
    $name = null;

    switch ($this->plots) {
      case 1:
        $name = '1x';
        break;
      case 2:
        $name = '2x';
        break;
      case 3:
        $name = '3x';
        break;
      case 4:
        $name = '4x';
        break;
      case 5:
        $name = '5x';
        break;
    }

    return $name;
  }

  public function getTypeNameAttribute()
  {
    $name = null;

    switch ($this->type) {
      case 1:
        $name = 'Presencial';
        break;
      case 2:
        $name = 'Virtual';
        break;
    }

    return $name;
  }

  public function getStatusNameAttribute()
  {
    $name = null;

    switch ($this->status) {
      case 1:
        $name = 'Aguardando pagamento';
        break;
      case 2:
        $name = 'Pagamento efetuado';
        break;
      case 3:
        $name = 'Em atraso';
        break;
    }

    return $name;
  }

  public function getIsVirtualAttribute()
  {
    if ($this->type == 2) {
      return true;
    }

    return false;
  }

  public function getIsPaidAttribute()
  {
    if ($this->status == 2) {
      return true;
    }

    return false;
  }

  public function getDescontNameAttribute()
  {
    return "{$this->descont} reais";
  }

  public function getTotalAttribute()
  {
    $total = $this->amount;

    if ($this->descont) {
      $total = (int) $total - (int) $this->descont;
    }

    return $total;
  }

  public function getLinkBoletAttribute()
  {
    $bolet = null;

    foreach ($this->student->orders as $order) {
        if ($order->type == 2) {
          $bolet = $order;
        }
    }

    return $bolet;
  }
}
