<?php

namespace App\Interfaces;

interface GatewayInterface
{
  public function getOrder(string $uuid) : array;
  public function getOrders() : array;
}
